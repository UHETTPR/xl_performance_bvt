<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Performance by Chapter</name>
   <tag></tag>
   <elementGuidId>8b05cc15-6c24-451c-b90d-d4ffbac1b6d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.end > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tr/td/ul[2]/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e35c6f36-c5c2-4c47-8314-c13c3798c7ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView('Performance by Chapter');</value>
      <webElementGuid>a85f2068-e330-41df-b231-121c26cb5f42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Performance by Chapter</value>
      <webElementGuid>87526304-e3da-4986-93c9-d2b2ba349f29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[@class=&quot;xlbootstrap3&quot;]/app-root[1]/app-gradebook[1]/app-instructor-links[1]/table[@class=&quot;sub-subnav-instructor-links&quot;]/tr[1]/td[@class=&quot;group&quot;]/ul[@class=&quot;end&quot;]/li[1]/a[1]</value>
      <webElementGuid>bcb6b8ff-a25b-4a2f-8a60-e7b238359bbb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tr/td/ul[2]/li/a</value>
      <webElementGuid>e0f0eb9b-3f5b-452c-b3cc-4233c881f8f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Performance by Chapter')]</value>
      <webElementGuid>a07823af-e705-471c-bfd4-45af364b5510</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/following::a[1]</value>
      <webElementGuid>ffee2c72-fc42-4f2e-a7ba-5d8215d352a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student Overview'])[1]/following::a[2]</value>
      <webElementGuid>e21f7736-2e91-4542-b421-836cde155b65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alerts'])[1]/preceding::a[1]</value>
      <webElementGuid>753f5358-026e-45eb-b118-ba3478439bf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inactivity'])[1]/preceding::a[1]</value>
      <webElementGuid>57e560cf-5239-4a07-b3d5-2762f6339b4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Performance by Chapter']/parent::*</value>
      <webElementGuid>1d8a505e-3e22-475f-bcae-236333e93773</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:goView('Performance by Chapter');&quot;)]</value>
      <webElementGuid>f4d53c17-6f51-4744-b71e-6ff68225c429</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li/a</value>
      <webElementGuid>d0d29d6a-66bd-4fc9-abb4-70fcdb4cf4d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:goView(&quot; , &quot;'&quot; , &quot;Performance by Chapter&quot; , &quot;'&quot; , &quot;);&quot;) and (text() = 'Performance by Chapter' or . = 'Performance by Chapter')]</value>
      <webElementGuid>4b6b65e4-da7c-4cea-8911-4d7eb60f6618</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
