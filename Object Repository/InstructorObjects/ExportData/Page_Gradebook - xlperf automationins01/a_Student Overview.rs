<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Student Overview</name>
   <tag></tag>
   <elementGuidId>4d2f8b48-cf34-43eb-8cae-a20aab1325c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Student Overview')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>4c9e7962-efc1-4f07-ab1e-e87fd06e52b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView('Overview by Student');</value>
      <webElementGuid>2ad5637d-adc5-4c02-95b5-eb3ff013a729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Student Overview</value>
      <webElementGuid>dbdb83ad-415b-40a6-9ca6-725d4a9c2dbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[@class=&quot;xlbootstrap3&quot;]/app-root[1]/app-gradebook[1]/app-instructor-links[1]/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[2]/a[1]</value>
      <webElementGuid>a20091e4-ab20-4d26-bfad-b022d68abc17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tbody/tr/td/ul/li[2]/a</value>
      <webElementGuid>dc0caaec-97f6-4ad9-b2a5-9765a617bdcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Student Overview')]</value>
      <webElementGuid>40221b9d-cec6-4b07-97f3-7b0f3687fcd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[1]</value>
      <webElementGuid>ab8453e1-2094-4187-a1f1-ec5cc85ef7f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Results By'])[1]/following::a[2]</value>
      <webElementGuid>c4ded4b0-3c6f-4b17-bfa5-2ffe31303861</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/preceding::a[1]</value>
      <webElementGuid>84574e48-d83a-4a6b-999d-1e62ce0c863d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/preceding::a[2]</value>
      <webElementGuid>bf767876-2502-43ea-baff-cd10d4c023e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Student Overview']/parent::*</value>
      <webElementGuid>319dc1ed-2ad8-4074-8192-df9afe06fffc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:goView('Overview by Student');&quot;)]</value>
      <webElementGuid>bf55189b-8097-4a51-acfd-5713a775db3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/ul/li[2]/a</value>
      <webElementGuid>7ffb90c0-462f-483b-873d-64729a339224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:goView(&quot; , &quot;'&quot; , &quot;Overview by Student&quot; , &quot;'&quot; , &quot;);&quot;) and (text() = 'Student Overview' or . = 'Student Overview')]</value>
      <webElementGuid>d48e9691-2f5b-4d7b-b622-d75330b6e420</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
