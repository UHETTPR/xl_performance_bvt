<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationins01, xlperf</name>
   <tag></tag>
   <elementGuidId>ba219e65-a1aa-4451-99c4-ba7423a685d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>tr.omit > td</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationins01, xlperf' or . = 'automationins01, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>4fade3b4-e4b7-4b16-86a8-99f977dc865b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationins01, xlperf</value>
      <webElementGuid>c04e5afd-669e-499c-9c39-d817f0be67a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;omit&quot;]/td[1]</value>
      <webElementGuid>0f92cdf5-ab00-4a49-a6e5-6b0eddf5809a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[5]/td</value>
      <webElementGuid>4a17f6cf-5098-48b0-a8d0-75dd242fb541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu06, xlperf'])[1]/following::td[5]</value>
      <webElementGuid>e4d843f4-1b9f-45f2-814e-a57bedce85c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu05, xlperf'])[1]/following::td[10]</value>
      <webElementGuid>4da6ceb8-b3b8-474f-8ed7-ed92f0c2f2df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu03, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>f5ea0599-4c27-4e2c-9991-3b2e6633a4f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu04, xlperf'])[1]/preceding::td[10]</value>
      <webElementGuid>d2edb42d-1220-492c-b995-4ec74f82ea23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationins01, xlperf']/parent::*</value>
      <webElementGuid>d4784c89-3281-482e-a0ae-22773263b0cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td</value>
      <webElementGuid>704fef43-6f70-44b8-9a38-789edd7cb4c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationins01, xlperf' or . = 'automationins01, xlperf')]</value>
      <webElementGuid>ef170cf0-94bf-4c16-b9a5-2e91981f7d12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
