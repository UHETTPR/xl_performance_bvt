<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationins02, xlperf</name>
   <tag></tag>
   <elementGuidId>3965eb8b-dbb5-42e0-b136-3131b2bfdf54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>tr.omit > td</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationins02, xlperf' or . = 'automationins02, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>cb161e3b-4005-4410-b8f1-083498c0df84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationins01, xlperf</value>
      <webElementGuid>987f5e0a-a6a1-4880-8cac-4663493ca55e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;omit&quot;]/td[1]</value>
      <webElementGuid>20ecf4cb-6883-4f27-a739-c470eaede747</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[5]/td</value>
      <webElementGuid>f9078a0d-380e-4ae1-8b63-aea5317653c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu06, xlperf'])[1]/following::td[5]</value>
      <webElementGuid>e1be6d40-83ce-4bda-9343-79afd099b9cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu05, xlperf'])[1]/following::td[10]</value>
      <webElementGuid>3cf0f5dc-86ec-420c-8a12-f6fbc015e075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu03, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>da5c11de-ba39-46f6-bdd2-a89f81ce22be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu04, xlperf'])[1]/preceding::td[10]</value>
      <webElementGuid>b18d6119-e196-421b-8c50-eadf7eb55585</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationins01, xlperf']/parent::*</value>
      <webElementGuid>1de958cd-65aa-4bb2-9a51-b4e1640233fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td</value>
      <webElementGuid>1bc90717-15b4-4312-beaf-e09ebdd0cfa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationins01, xlperf' or . = 'automationins01, xlperf')]</value>
      <webElementGuid>cdf8cb57-2450-4bb6-a2e0-175bc36fa0ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
