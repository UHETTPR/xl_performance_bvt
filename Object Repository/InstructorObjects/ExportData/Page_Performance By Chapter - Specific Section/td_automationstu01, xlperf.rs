<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu01, xlperf</name>
   <tag></tag>
   <elementGuidId>ed077015-84de-4d92-bdf4-f5788f7578f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>f56407b2-85c2-4f4b-87ea-1f6801de6753</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>717e68f6-aeb7-45ef-bd90-4ded7b55f78b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;alt&quot;]/td[1]</value>
      <webElementGuid>e4827ba2-87c5-455e-9b27-82e3f3b7fba5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[2]/td</value>
      <webElementGuid>bce1eaa2-34ef-44a9-8dd1-103a02be2cb9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[1]/following::td[1]</value>
      <webElementGuid>7f5529a3-7608-44c7-bcd8-f8c4ef8e93b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(all attempts)'])[2]/following::td[1]</value>
      <webElementGuid>44e9c8d8-b4f9-4fe3-afc5-f720d78ac195</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu05, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>abb94a07-ece1-4fe9-9ee5-0277b1933104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu06, xlperf'])[1]/preceding::td[10]</value>
      <webElementGuid>a9fd2519-1709-4dbf-89b6-fea1c75028fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>172c7a41-4642-4da3-85ff-c169bd335fa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[2]/td</value>
      <webElementGuid>a8855fc8-6d2c-4869-82bc-3be8c0b7706d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>febfada0-68c4-4a97-a8c9-9b1a2c55e4df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
