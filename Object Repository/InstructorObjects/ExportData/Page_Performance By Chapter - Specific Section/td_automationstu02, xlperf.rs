<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu02, xlperf</name>
   <tag></tag>
   <elementGuidId>90a4c547-7abe-4ee5-9fbc-d48d1fee5b8b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu02, xlperf' or . = 'automationstu02, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>9ce6b40a-3103-46c6-b558-2760c50c07e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>d71b5a5f-8e5c-4f81-916b-1a9a3fad513e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;alt&quot;]/td[1]</value>
      <webElementGuid>64589130-d57c-4f14-9138-a64bcdbe5148</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[2]/td</value>
      <webElementGuid>c40333ba-1635-4b66-8db8-b33a77b1df26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[1]/following::td[1]</value>
      <webElementGuid>6864258e-bfc3-43bb-be84-bc6b28f7b709</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(all attempts)'])[2]/following::td[1]</value>
      <webElementGuid>da0ae258-e227-4cce-9d6a-e3a8a89e627f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu05, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>29fb5697-5453-40c2-b18e-8d0ba0396329</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu06, xlperf'])[1]/preceding::td[10]</value>
      <webElementGuid>076c7c55-3206-42db-acc9-3cd6af3f3d3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>7a35a40c-4f4b-468d-92d8-409d869e5cc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/table/tbody/tr[2]/td</value>
      <webElementGuid>1f15e790-cf40-4102-ac46-c5e0eec47026</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>b83ae0c4-5ad7-4d6a-8d22-cec726a6125d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
