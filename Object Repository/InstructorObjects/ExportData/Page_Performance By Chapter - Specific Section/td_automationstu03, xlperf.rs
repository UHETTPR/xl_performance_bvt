<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu03, xlperf</name>
   <tag></tag>
   <elementGuidId>6aff40f8-cf77-4818-af47-8e74ea444f17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>tr.alt.omit > td</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='RepeaterContainer']/table/tbody/tr[6]/td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>4628418a-26d0-4f28-9eeb-1ccbd57a27c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu03, xlperf</value>
      <webElementGuid>5cb8de6c-6b63-4e36-ad17-e5c765c48986</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;alt omit&quot;]/td[1]</value>
      <webElementGuid>d6e93597-283c-4118-a635-133e34001010</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[6]/td</value>
      <webElementGuid>067d680b-c8d8-4836-8a01-a590ef909d89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationins01, xlperf'])[1]/following::td[5]</value>
      <webElementGuid>65018761-7d32-4f16-9558-fc2912557bd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu06, xlperf'])[1]/following::td[10]</value>
      <webElementGuid>6dab3675-89b2-408b-913c-ebf165e4e89c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu04, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>de31d371-05fb-4a17-9c28-6e8d36d52189</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::td[10]</value>
      <webElementGuid>ca48fcb8-e246-4e83-b9cc-3c372a25e96a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu03, xlperf']/parent::*</value>
      <webElementGuid>1fc31980-be34-4fd1-913b-fa339a592e94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[6]/td</value>
      <webElementGuid>5fbddf8e-8d86-4c44-9710-dc28d7a84224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu03, xlperf' or . = 'automationstu03, xlperf')]</value>
      <webElementGuid>03730a2b-f27a-442e-8302-efdef833c741</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
