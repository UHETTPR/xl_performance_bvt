<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu04, xlperf</name>
   <tag></tag>
   <elementGuidId>fdf8873b-0f86-487d-ba95-db57d419072a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu04, xlperf' or . = 'automationstu04, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>eddf747d-c700-46af-b9cc-1455c369d0e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu04, xlperf</value>
      <webElementGuid>1882b00f-11f4-4f5b-8b8d-f7f971740029</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;omit&quot;]/td[1]</value>
      <webElementGuid>05b8c9b8-0442-41ac-b261-2b1d00c2ba20</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[7]/td</value>
      <webElementGuid>14cfaf5f-5fc9-497f-85ca-66d29878b966</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu03, xlperf'])[1]/following::td[5]</value>
      <webElementGuid>ee21c2cd-4eb3-4a19-9d07-65152995835f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationins01, xlperf'])[1]/following::td[10]</value>
      <webElementGuid>5c16cfaf-fba0-4bbb-ac95-a12eb141ee99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::td[5]</value>
      <webElementGuid>f731118c-3c7f-4973-9fec-e604dce813d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/preceding::td[5]</value>
      <webElementGuid>18bcc649-940d-490a-ab29-e7511b094d1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu04, xlperf']/parent::*</value>
      <webElementGuid>898b94bd-02f8-43d0-bec6-d43c2d937c16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[7]/td</value>
      <webElementGuid>6b704447-a87a-4912-937e-454764b38f3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu04, xlperf' or . = 'automationstu04, xlperf')]</value>
      <webElementGuid>2498f306-46d6-4323-833e-5dad7f67bca8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
