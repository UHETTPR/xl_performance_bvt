<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu07, xlperf</name>
   <tag></tag>
   <elementGuidId>3b82e71b-ddbd-4b66-aba5-904991c28d4d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu07, xlperf' or . = 'automationstu07, xlperf')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>51c8b95c-4563-4ed4-b19c-4328c41d275a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu06, xlperf</value>
      <webElementGuid>b4a3c444-063e-4bdf-942f-b9009b867a81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/table[@class=&quot;grid&quot;]/tbody[1]/tr[@class=&quot;alt&quot;]/td[1]</value>
      <webElementGuid>eb6e3059-e096-4fb9-a61f-2f95135cb50c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='RepeaterContainer']/table/tbody/tr[4]/td</value>
      <webElementGuid>8d1a89f0-79aa-4f1b-a77b-74726cf85813</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu05, xlperf'])[1]/following::td[5]</value>
      <webElementGuid>607644a3-7680-44d8-b11f-5edb4ec3f196</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu01, xlperf'])[1]/following::td[10]</value>
      <webElementGuid>5949b321-c7e2-4ae9-8473-303ec64243d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationins01, xlperf'])[1]/preceding::td[5]</value>
      <webElementGuid>096637f2-eda0-4904-ab8d-06fbbe7d64b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationstu03, xlperf'])[1]/preceding::td[10]</value>
      <webElementGuid>dbc57e3f-3770-408b-98c7-4916d3143d8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu06, xlperf']/parent::*</value>
      <webElementGuid>471ce4ab-2174-482a-bc8e-2ad29b061238</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td</value>
      <webElementGuid>8c630843-1290-4571-91a5-0bb509d1c8e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu06, xlperf' or . = 'automationstu06, xlperf')]</value>
      <webElementGuid>2623cb66-e251-4c86-8c00-a7e232ed213b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
