<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 1.2 Symbols and Sets of Numbers</name>
   <tag></tag>
   <elementGuidId>e6d1310a-dce6-4bdc-a91f-1080262e13e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 1.2: Symbols and Sets of Numbers')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>63846323-d8c6-402c-87e8-574c5b7e9d35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0);</value>
      <webElementGuid>30d310c6-7af4-45ec-9252-70b47995f06e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_treeview.NodeTextClicked('ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_2'); return false;</value>
      <webElementGuid>cea96c68-d1d5-4d5c-b148-824e88294ed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 1.2: Symbols and Sets of Numbers</value>
      <webElementGuid>c51a0878-3dcb-4f7c-bc11-d1939a24c77c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_2&quot;)/td[1]/table[1]/tbody[1]/tr[1]/td[4]/a[1]</value>
      <webElementGuid>9fe08872-966c-4ba7-a53b-9a41ac3f5f40</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;ctl00_ctl00_InsideForm_MasterContent_treeview.NodeTextClicked('ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_2'); return false;&quot;]</value>
      <webElementGuid>355e0acb-39de-43d2-a8a5-c329790bdba4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_2']/td/table/tbody/tr/td[4]/a</value>
      <webElementGuid>1d449414-b84c-4ed1-a395-3b79f02656b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 1.2: Symbols and Sets of Numbers')]</value>
      <webElementGuid>71c7a3df-ec14-4ef6-adae-ca8473aa08ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tips for Success in Mathematics, no exercises available'])[1]/following::a[3]</value>
      <webElementGuid>3f79c6b8-e5e6-475b-a8f0-6d5f350594b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section 1.1: Tips for Success in Mathematics, no exercises available'])[1]/following::a[4]</value>
      <webElementGuid>8c0d8e89-341d-43cd-ae5b-7846301a6b07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vocabulary and Readiness Check'])[1]/preceding::a[2]</value>
      <webElementGuid>5dd6c34c-6a06-4b84-b2d9-1ede9bc0600b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Check'])[1]/preceding::a[3]</value>
      <webElementGuid>4175cb74-c494-4af2-99c2-bb57a18b0e6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 1.2: Symbols and Sets of Numbers']/parent::*</value>
      <webElementGuid>399fef71-3b9b-4333-9ac3-142ec98bdf6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0);')])[13]</value>
      <webElementGuid>b1462672-1dd9-4003-9423-accbaf1e087d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td/table/tbody/tr/td[4]/a</value>
      <webElementGuid>e7ac8dd2-d787-41e6-97f5-d18534615d95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0);' and (text() = 'Section 1.2: Symbols and Sets of Numbers' or . = 'Section 1.2: Symbols and Sets of Numbers')]</value>
      <webElementGuid>29a9b372-c86e-43b1-8d1e-31fb6601b0d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
