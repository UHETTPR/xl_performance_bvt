<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 1.3 Fractions and Mixed Numbers</name>
   <tag></tag>
   <elementGuidId>203abf99-f4b9-42b8-907f-d758d2ab2fe7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick=&quot;ctl00_ctl00_InsideForm_MasterContent_treeview.NodeTextClicked('ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_10'); return false;&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>cd64ec64-05db-44a9-943f-e5df995ffe01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0);</value>
      <webElementGuid>a54c2b92-d396-43e2-ae48-00382050d937</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_treeview.NodeTextClicked('ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_10'); return false;</value>
      <webElementGuid>0af07a32-ab8c-435a-9e41-bb46d6c382ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 1.3: Fractions and Mixed Numbers</value>
      <webElementGuid>5aa3194e-6d5f-49e2-9b29-322fdccfa3dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_10&quot;)/td[1]/table[1]/tbody[1]/tr[1]/td[4]/a[1]</value>
      <webElementGuid>e2aa1992-aa64-4559-95c6-2c09a61ca880</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick=&quot;ctl00_ctl00_InsideForm_MasterContent_treeview.NodeTextClicked('ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_10'); return false;&quot;]</value>
      <webElementGuid>e6f28c93-a750-487e-9dca-90dd0b535216</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_treeview_0_1_10']/td/table/tbody/tr/td[4]/a</value>
      <webElementGuid>b9afc893-4c40-456e-9ff2-772f30d15a7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 1.3: Fractions and Mixed Numbers')]</value>
      <webElementGuid>73698644-8b57-4a41-9443-c88cf0b5c023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Concept Extensions'])[1]/following::a[3]</value>
      <webElementGuid>3726545d-64b8-401c-af0b-9fff2dbffc3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Find the absolute value of a real number.'])[1]/following::a[4]</value>
      <webElementGuid>2889543e-838e-48b5-be32-07994ed83105</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Vocabulary and Readiness Check'])[2]/preceding::a[2]</value>
      <webElementGuid>e83e0341-c5e7-46ac-89c2-44830d668aad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video Check'])[2]/preceding::a[3]</value>
      <webElementGuid>3a7fcb2b-188e-4d39-a474-6b6f65e9c797</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 1.3: Fractions and Mixed Numbers']/parent::*</value>
      <webElementGuid>7c1c4429-2425-4d9e-a877-ab921704c92f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0);')])[15]</value>
      <webElementGuid>cd4536a1-ef7d-43a3-b5df-b9a2af54a96b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[13]/td/table/tbody/tr/td[4]/a</value>
      <webElementGuid>7c801995-5dbd-4eab-9c1e-ec81c97a2f46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0);' and (text() = 'Section 1.3: Fractions and Mixed Numbers' or . = 'Section 1.3: Fractions and Mixed Numbers')]</value>
      <webElementGuid>7522cace-0212-4852-b132-d8e31d9cd54d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
