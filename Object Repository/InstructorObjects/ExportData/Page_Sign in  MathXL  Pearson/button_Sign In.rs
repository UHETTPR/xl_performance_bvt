<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In</name>
   <tag></tag>
   <elementGuidId>190b9f11-a7af-4d6c-a14b-772b8df6444d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.button.button-big-icon.bg-color-match-medium.uppercase.mar-top-x1Half</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4a8932c6-6897-46f2-94b7-4b934f20b87b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>5d34924e-79a4-46b3-bcfb-1ba750cd54b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium uppercase mar-top-x1Half</value>
      <webElementGuid>3686cfda-c644-408a-b34a-8b5e91cf2950</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Sign In
                        </value>
      <webElementGuid>15ca69aa-1572-4920-9a06-8be084786433</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inputfrm&quot;)/button[@class=&quot;button button-big-icon bg-color-match-medium uppercase mar-top-x1Half&quot;]</value>
      <webElementGuid>27d7239c-c092-408b-be8b-813add240b18</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>6db0800d-c83d-4fa4-a423-dc02315bd9b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='inputfrm']/button</value>
      <webElementGuid>4989e674-7054-4656-b0e6-ee2c024f7cfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>079ef262-9d68-4fdb-8503-bdc038a36c1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::button[1]</value>
      <webElementGuid>5bd176ac-e6e4-491f-bb4f-1907c798b3b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/preceding::button[1]</value>
      <webElementGuid>479fe3f3-001d-48da-b778-4464f6bb2fa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit our home page to register!'])[1]/preceding::button[1]</value>
      <webElementGuid>4c1d3ac9-918b-4382-b7eb-e6251cfa6801</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>1da44f22-6f0a-4f46-92f8-76a02fb09985</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>1dd00f5b-e271-4513-bb93-8c6f007c1ce9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                            Sign In
                        ' or . = '
                            Sign In
                        ')]</value>
      <webElementGuid>5f72e6b3-2e2b-4ca5-b6d6-dc66c96a8803</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
