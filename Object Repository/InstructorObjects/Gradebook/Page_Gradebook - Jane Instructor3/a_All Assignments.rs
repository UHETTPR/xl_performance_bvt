<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_All Assignments</name>
   <tag></tag>
   <elementGuidId>86869444-f607-4ad8-9c95-f6cecb74950b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.popover-content > ul > li > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'All Assignments')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ff84e182-62ce-4047-af87-61c1ee1824cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView(AllAssignments);</value>
      <webElementGuid>9ee42407-fc44-4932-9ccf-207cd131b888</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All Assignments</value>
      <webElementGuid>c0f863b8-36fd-473b-a408-1c651769c0f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;instructorlinks&quot;)/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[2]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[1]/a[1]</value>
      <webElementGuid>8d361942-01f6-4903-bbfc-760bd517aa3d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='instructorlinks']/table/tbody/tr/td/ul/li[2]/div/div[2]/ul/li/a</value>
      <webElementGuid>be7b28c2-6be2-490d-b905-099cdb1e7471</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'All Assignments')]</value>
      <webElementGuid>e0342980-be22-48ba-8ea1-b4c83c418b66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[1]</value>
      <webElementGuid>298ea44c-3f6e-4250-a887-0f5acc83ece6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Path'])[1]/following::a[2]</value>
      <webElementGuid>b75ea3df-f56c-484f-a93b-b4f69248d5de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>10c88e32-4eca-47be-a828-d2a9649326f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quizzes'])[1]/preceding::a[2]</value>
      <webElementGuid>1456fd5b-a04f-466e-bfb7-15baea8e58ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='All Assignments']/parent::*</value>
      <webElementGuid>483b0b5a-d80b-4b0d-9938-dba38430aa3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:goView(AllAssignments);')]</value>
      <webElementGuid>6794ae4a-4e03-429d-957f-cc2d8d6ec6d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/a</value>
      <webElementGuid>465b9eb8-ebae-4ba3-9e71-52d16603f535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:goView(AllAssignments);' and (text() = 'All Assignments' or . = 'All Assignments')]</value>
      <webElementGuid>b36626f6-ef10-4245-bdb1-5ab780e0895b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
