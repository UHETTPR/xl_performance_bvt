<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Performance by Chapter</name>
   <tag></tag>
   <elementGuidId>a289c703-0f26-4caf-a67a-6df2aa5172f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.end > li > a.ng-binding</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Performance by Chapter')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>39cdda5f-e0fd-468d-b353-93f4a69d7109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView('Performance by Chapter');</value>
      <webElementGuid>5b5f6aba-fa13-466d-80e5-bdea381dbfb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>7240402f-796b-4f24-9c5d-cffc1ebbe722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Performance by Chapter</value>
      <webElementGuid>738f3f83-4419-47b3-8285-7eabcd339051</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;instructorlinks&quot;)/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[@class=&quot;end&quot;]/li[1]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>6fe13282-221d-4a8c-bc8a-12c18d05f909</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='instructorlinks']/table/tbody/tr/td/ul[2]/li/a</value>
      <webElementGuid>045e9406-4c91-4eb5-83a7-6a113bd1146e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Performance by Chapter')]</value>
      <webElementGuid>b9cd4386-4bca-4af0-a179-a0dcaf860d31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/following::a[1]</value>
      <webElementGuid>8150652e-a241-45b1-86e3-e5fee53507f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student Overview'])[1]/following::a[2]</value>
      <webElementGuid>c22acbdc-c2e7-474a-a108-c90ebbed76f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Knowledge Checks'])[1]/preceding::a[1]</value>
      <webElementGuid>9ebe7907-2bf5-4fd2-8e65-8ca3ec030a43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Standard'])[1]/preceding::a[2]</value>
      <webElementGuid>f12f7dab-0d81-4f87-a982-c39226e63357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Performance by Chapter']/parent::*</value>
      <webElementGuid>8275a523-2896-4bd5-b281-d33804eda874</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:goView('Performance by Chapter');&quot;)]</value>
      <webElementGuid>e63e2ed4-72eb-40e0-ad4b-d5ff0b0df169</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li/a</value>
      <webElementGuid>cbe56021-ff24-40ec-b2d2-14cf882e5c49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:goView(&quot; , &quot;'&quot; , &quot;Performance by Chapter&quot; , &quot;'&quot; , &quot;);&quot;) and (text() = 'Performance by Chapter' or . = 'Performance by Chapter')]</value>
      <webElementGuid>a4f98064-e500-46af-bc51-21550dc39edb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
