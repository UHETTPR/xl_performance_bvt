<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Student Overview</name>
   <tag></tag>
   <elementGuidId>793b644d-538d-4abd-beb6-2019cec682e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Student Overview')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>80c52337-911a-4a0f-b45d-bc8bd57699ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView('Overview by Student');</value>
      <webElementGuid>42a20c4d-1550-4db3-a9de-75e55f178b76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-binding</value>
      <webElementGuid>f27fa591-9223-4462-a381-6ce5981d0bcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Student Overview</value>
      <webElementGuid>92bfeef8-9e7b-4359-9a03-67b665d09a68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;instructorlinks&quot;)/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[3]/a[@class=&quot;ng-binding&quot;]</value>
      <webElementGuid>9052d7e2-20a5-427a-aa30-6835ad5cee59</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='instructorlinks']/table/tbody/tr/td/ul/li[3]/a</value>
      <webElementGuid>d2a16c6d-88a7-4612-bb15-a3f72647f573</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Student Overview')]</value>
      <webElementGuid>e654678b-b7d1-4a2e-a4f9-f216c7bf2bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[1]</value>
      <webElementGuid>e0cee370-99b7-4340-9783-84869cb55256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learning Path'])[1]/following::a[2]</value>
      <webElementGuid>f2f2c8de-308a-4c12-9841-756335d7912c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/preceding::a[1]</value>
      <webElementGuid>9e25b003-4758-4304-a2dd-6a6277fca79f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/preceding::a[2]</value>
      <webElementGuid>fd0f8440-9fee-46d0-a56c-aa968b1d43ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Student Overview']/parent::*</value>
      <webElementGuid>91b13e01-4b24-43ec-8b64-bef013a057e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:goView('Overview by Student');&quot;)]</value>
      <webElementGuid>b4191585-202b-494a-a2ab-798e8b926c2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/ul/li[3]/a</value>
      <webElementGuid>e3e598a1-7d18-4e25-9f68-f33d0779038a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:goView(&quot; , &quot;'&quot; , &quot;Overview by Student&quot; , &quot;'&quot; , &quot;);&quot;) and (text() = 'Student Overview' or . = 'Student Overview')]</value>
      <webElementGuid>5976b2ca-a297-47cc-b20b-4fbadcf01cec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
