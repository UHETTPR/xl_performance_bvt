<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_--_xlicon-ia</name>
   <tag></tag>
   <elementGuidId>7b5121eb-a338-4851-aa1c-4ef4a55f6303</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span/a[2]/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.xlicon-ia</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>e28f5ff5-22c2-48b2-9941-2cd5b0817b3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlicon-ia</value>
      <webElementGuid>116eebdc-b8f8-4dcb-9fcb-145cb76cea90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9b43248a-b79c-45d9-9baf-b2aa016e0691</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/tbody[@class=&quot;class-stats&quot;]/tr[2]/td[8]/span[@class=&quot;vcenter&quot;]/a[2]/i[@class=&quot;xlicon-ia&quot;]</value>
      <webElementGuid>d31a087b-3f14-4b16-b779-68b8b5cda1be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='gradebook']/tbody/tr[2]/td[8]/span/a[2]/i</value>
      <webElementGuid>c194cc8e-e494-4ecc-bb82-ebf382f5c29b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a[2]/i</value>
      <webElementGuid>4376e7c5-6c7e-4062-ac45-b518cf2c9fc3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
