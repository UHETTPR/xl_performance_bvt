<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_--_xlicon-ia_1</name>
   <tag></tag>
   <elementGuidId>20fd7995-95ed-4208-9817-10af7c9a2ac3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[2]/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.xlicon-ia</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>73fb74f9-a932-4d04-8bfb-1284a632dbd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlicon-ia</value>
      <webElementGuid>edec7b50-2a8c-4af6-a186-c5b64081505e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>70c9c18a-da8e-4f72-98c8-6cb6aabc8294</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebook&quot;)/tbody[@class=&quot;class-stats&quot;]/tr[2]/td[4]/span[@class=&quot;vcenter&quot;]/a[2]/i[@class=&quot;xlicon-ia&quot;]</value>
      <webElementGuid>31513aa6-5926-4f5c-aeaa-e31efcd82913</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='gradebook']/tbody/tr[2]/td[4]/span/a[2]/i</value>
      <webElementGuid>e699729d-e89c-4aee-89d6-f5c5fdcfb3e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/i</value>
      <webElementGuid>d4883c87-2346-4889-ae7b-55cb11e6331c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
