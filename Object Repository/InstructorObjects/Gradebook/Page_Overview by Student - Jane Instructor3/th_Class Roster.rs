<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_Class Roster</name>
   <tag></tag>
   <elementGuidId>c000053f-136e-4611-98b0-0dc4f4656323</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>th.ascending.ascendingColumnHeader.sortableColumnHeader</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[@onclick=&quot;this.blur(); return SortTableColumn('gbbody',0,false,0,null,'','alternatingRow',this);&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scope</name>
      <type>Main</type>
      <value>col</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ascending ascendingColumnHeader sortableColumnHeader</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>this.blur(); return SortTableColumn('gbbody',0,false,0,null,'','alternatingRow',this);</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Class Roster</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebookOverviewStudent&quot;)/table[@class=&quot;table table-inner-bordered table-stripes-inverse table-student-overview&quot;]/thead[1]/tr[1]/th[@class=&quot;ascending ascendingColumnHeader sortableColumnHeader&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//th[@onclick=&quot;this.blur(); return SortTableColumn('gbbody',0,false,0,null,'','alternatingRow',this);&quot;]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='gradebookOverviewStudent']/table/thead/tr/th</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/following::th[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/following::th[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Overall Score'])[1]/preceding::th[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = 'Class Roster' or . = 'Class Roster')]</value>
   </webElementXpaths>
</WebElementEntity>
