<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_-- Select --</name>
   <tag></tag>
   <elementGuidId>0aa868f3-5dd3-442a-a907-b0704e01a468</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ActionList</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id = 'ActionList' and @type = 'button' and (text() = '
	        -- Select --
		    
	    ' or . = '
	        -- Select --
		    
	    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d6219964-f637-4ccd-a0c2-1b24eaadb62c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ActionList</value>
      <webElementGuid>8f1be1a8-cf56-46d5-90f8-fd2c8e32ebc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fa67ec58-53a8-4957-872a-08e5b2087efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default dropdown-toggle</value>
      <webElementGuid>b376268b-a2f0-4400-ba88-6a08bf8ea7f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>81dae4be-4503-452e-aba2-fc7606d3bc97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>cb7a851f-af78-4880-9e73-cb4464c04cbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c67c4311-cbaf-44b3-bedf-40b98124fad9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	        -- Select --
		    
	    </value>
      <webElementGuid>6380ff03-d25b-4567-9b87-8d8a4894a52b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row202193526&quot;)/td[@class=&quot;center rightnone action-dropdown-foundations last-field&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller&quot;]/button[@id=&quot;ActionList&quot;]</value>
      <webElementGuid>fb9dd873-b245-415f-a3d3-9bc0427e92bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='ActionList']</value>
      <webElementGuid>71a4d949-fe47-4502-aec0-c2c85c7d3be9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row202193526']/td[11]/div/div/button</value>
      <webElementGuid>5689ad88-ed4e-4e7c-bec5-de49ccfde82b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Supported'])[1]/following::button[1]</value>
      <webElementGuid>6c498350-7b43-41b3-b5c6-a3ec7558c10b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(omitted)'])[1]/following::button[1]</value>
      <webElementGuid>1a2f746f-9a4c-43e5-a4a2-5e38c16b7c1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Student'])[1]/preceding::button[1]</value>
      <webElementGuid>965f0553-b336-499c-adf5-a88beef725ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[11]/div/div/button</value>
      <webElementGuid>1670995a-b837-48c8-ba42-90bcb44bbd52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'ActionList' and @type = 'button' and (text() = '
	        -- Select --
		    
	    ' or . = '
	        -- Select --
		    
	    ')]</value>
      <webElementGuid>a4a65d0e-c58f-4571-a1d4-0dcc1791ae5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Changed by instructor 03/18/22'])[1]/following::button[1]</value>
      <webElementGuid>98785b9a-f430-47c6-9783-e015a9dc59dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
