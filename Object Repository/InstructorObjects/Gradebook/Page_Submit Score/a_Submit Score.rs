<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Submit Score</name>
   <tag></tag>
   <elementGuidId>40ad8cda-4889-411f-949a-a0124602b159</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ImgBtnUpdate</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Submit Score')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fdd9304c-e316-466a-bd47-698de136c79d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))</value>
      <webElementGuid>1e88efa9-864c-4172-8fc1-74330d86fe18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ImgBtnUpdate</value>
      <webElementGuid>ff987e13-1de4-4a23-be64-aa1b12ac3329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>if(!SubCheckSubmit(this)) return false;</value>
      <webElementGuid>ed686089-5f44-4bbf-a015-d15930b8ca67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary </value>
      <webElementGuid>4ec8f365-91f8-40d1-8a98-e6568c6eae99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Score
</value>
      <webElementGuid>603733c9-2455-4ffa-9480-78d60ef36322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ImgBtnUpdate&quot;)</value>
      <webElementGuid>cbd21fc4-ae7d-48c1-ace4-c7c67657f58b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ImgBtnUpdate']</value>
      <webElementGuid>511a521a-b1f3-4117-b517-3e0e7991d7a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wizard-buttons']/div[2]/a</value>
      <webElementGuid>58cedc24-7e2f-4bba-9764-edf6812c02fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Submit Score')]</value>
      <webElementGuid>ac6a4aae-f730-4b3d-be8d-8caf2bd979dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::a[1]</value>
      <webElementGuid>0047d340-1f63-4523-a2ee-314f936c5caf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You must enter a value.'])[2]/following::a[2]</value>
      <webElementGuid>93656f64-e0fe-4eb7-b794-e94b04a01399</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/preceding::a[1]</value>
      <webElementGuid>b9dec127-e71c-4968-8ce7-e540f16db3ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'ImgBtnUpdate', '&quot;', ')')])[1]/preceding::a[1]</value>
      <webElementGuid>9dc9a22c-cfbf-4f10-a65e-b1b1276aa703</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))')]</value>
      <webElementGuid>b20cb4ed-fe1e-452e-b155-4626d9def0cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a</value>
      <webElementGuid>48885fe3-9ea3-47d7-9fbe-c5dadf74fc8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))' and @id = 'ImgBtnUpdate' and (text() = 'Submit Score
' or . = 'Submit Score
')]</value>
      <webElementGuid>8a7643dc-3531-4702-ab40-51b564d1162a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
