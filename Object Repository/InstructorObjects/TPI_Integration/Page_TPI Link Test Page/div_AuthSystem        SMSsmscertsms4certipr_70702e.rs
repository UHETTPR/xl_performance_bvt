<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AuthSystem        SMSsmscertsms4certipr_70702e</name>
   <tag></tag>
   <elementGuidId>5c7d896e-f154-4865-a9a8-eea3fd32675a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='pnlManage']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#pnlManage</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pnlManage</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student's SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pnlManage&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='pnlManage']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='(use tpicertprofalec for the professor account)']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'pnlManage' and (text() = concat(&quot;
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
&quot;) or . = concat(&quot;
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
