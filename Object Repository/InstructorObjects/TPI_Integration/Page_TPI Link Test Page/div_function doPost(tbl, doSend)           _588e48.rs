<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_function doPost(tbl, doSend)           _588e48</name>
   <tag></tag>
   <elementGuidId>ed03f401-21ef-4694-89cc-561e78e05e92</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        function doPost(tbl, doSend) {
            var sForm = document.createElement(&quot;form&quot;);
            document.body.appendChild(sForm);

            var debugStr = '';
            var func = function () {
                var id = this.id;
                var value = this.value;

                id = id.substring(id.indexOf('_') + 1); // remove first prefix &quot;tb_&quot; or &quot;tb2_&quot;

                if (value.length > 0) {
                    var sMarker = document.createElement('input');
                    sMarker.setAttribute('type', 'hidden');
                    sMarker.setAttribute('name', id);
                    sMarker.setAttribute('value', value);
                    sForm.appendChild(sMarker);
                }
                debugStr += id + ' = ' + value + '\n';
            };
            $('#' + tbl).find('input').each(func);
            $('#' + tbl).find(&quot;select&quot;).each(func);

            var sMarker = document.createElement('input');
            sMarker.setAttribute('type', 'hidden');
            sMarker.setAttribute('name', 'bypass');
            sMarker.setAttribute('value', 'yes');
            sForm.appendChild(sMarker);
            
            $('#textDebugList').val(debugStr); // for easy copying and pasting
            if (doSend) 
            {
                sForm.method = &quot;POST&quot;;
                sForm.action = 'https://testxl.pearsoncmg.com/test/../tpilink.aspx';
                sForm.target = &quot;_blank&quot;;
                sForm.submit();
            }
        }
        function loadList(prefix) {
            var lines = $('#textDebugList').val().split('\n');
            for (var i = 0; i &lt; lines.length; i++) {
                var splited = lines[i].split(' = ');
                var id = splited[0];
                var val = '';
                if (splited.length > 1)
                    val = splited[1];
                $(&quot;#&quot; + prefix + splited[0]).val(val);

            }
        }
        function setNextSubmitTargetBlank(checkPageLinkMode)
        {
            if (checkPageLinkMode)
            {
                var drp = document.getElementById(&quot;link_PageLinkMode&quot;);
                var drpTarget = document.getElementById(&quot;link_Target&quot;);

                if (drp.options[drp.selectedIndex].value == &quot;frame&quot; &amp;&amp; 
                    drpTarget.options[drpTarget.selectedIndex].value != &quot;uopdiagnostic&quot;) {
                    return;
                }
            }

            //document.forms[0].target = '_blank';
            $(&quot;#form1&quot;).attr(&quot;target&quot;, &quot;_blank&quot;);

            setTimeout('$(&quot;#form1&quot;).removeAttr(&quot;target&quot;);', 500); // reset
        }
    
    








//&lt;![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>









	
	

        

    
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student's SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        

    


//&lt;![CDATA[
WebForm_AutoFocus('ButtonRedirect');//]]>



        
        
    
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
    
        function doPost(tbl, doSend) {
            var sForm = document.createElement(&quot;form&quot;);
            document.body.appendChild(sForm);

            var debugStr = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var func = function () {
                var id = this.id;
                var value = this.value;

                id = id.substring(id.indexOf(&quot; , &quot;'&quot; , &quot;_&quot; , &quot;'&quot; , &quot;) + 1); // remove first prefix &quot;tb_&quot; or &quot;tb2_&quot;

                if (value.length > 0) {
                    var sMarker = document.createElement(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;type&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;hidden&quot; , &quot;'&quot; , &quot;);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;, id);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;, value);
                    sForm.appendChild(sMarker);
                }
                debugStr += id + &quot; , &quot;'&quot; , &quot; = &quot; , &quot;'&quot; , &quot; + value + &quot; , &quot;'&quot; , &quot;\n&quot; , &quot;'&quot; , &quot;;
            };
            $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + tbl).find(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;).each(func);
            $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + tbl).find(&quot;select&quot;).each(func);

            var sMarker = document.createElement(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;type&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;hidden&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;bypass&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;yes&quot; , &quot;'&quot; , &quot;);
            sForm.appendChild(sMarker);
            
            $(&quot; , &quot;'&quot; , &quot;#textDebugList&quot; , &quot;'&quot; , &quot;).val(debugStr); // for easy copying and pasting
            if (doSend) 
            {
                sForm.method = &quot;POST&quot;;
                sForm.action = &quot; , &quot;'&quot; , &quot;https://testxl.pearsoncmg.com/test/../tpilink.aspx&quot; , &quot;'&quot; , &quot;;
                sForm.target = &quot;_blank&quot;;
                sForm.submit();
            }
        }
        function loadList(prefix) {
            var lines = $(&quot; , &quot;'&quot; , &quot;#textDebugList&quot; , &quot;'&quot; , &quot;).val().split(&quot; , &quot;'&quot; , &quot;\n&quot; , &quot;'&quot; , &quot;);
            for (var i = 0; i &lt; lines.length; i++) {
                var splited = lines[i].split(&quot; , &quot;'&quot; , &quot; = &quot; , &quot;'&quot; , &quot;);
                var id = splited[0];
                var val = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                if (splited.length > 1)
                    val = splited[1];
                $(&quot;#&quot; + prefix + splited[0]).val(val);

            }
        }
        function setNextSubmitTargetBlank(checkPageLinkMode)
        {
            if (checkPageLinkMode)
            {
                var drp = document.getElementById(&quot;link_PageLinkMode&quot;);
                var drpTarget = document.getElementById(&quot;link_Target&quot;);

                if (drp.options[drp.selectedIndex].value == &quot;frame&quot; &amp;&amp; 
                    drpTarget.options[drpTarget.selectedIndex].value != &quot;uopdiagnostic&quot;) {
                    return;
                }
            }

            //document.forms[0].target = &quot; , &quot;'&quot; , &quot;_blank&quot; , &quot;'&quot; , &quot;;
            $(&quot;#form1&quot;).attr(&quot;target&quot;, &quot;_blank&quot;);

            setTimeout(&quot; , &quot;'&quot; , &quot;$(&quot;#form1&quot;).removeAttr(&quot;target&quot;);&quot; , &quot;'&quot; , &quot;, 500); // reset
        }
    
    








//&lt;![CDATA[
var theForm = document.forms[&quot; , &quot;'&quot; , &quot;form1&quot; , &quot;'&quot; , &quot;];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>









	
	

        

    
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        

    


//&lt;![CDATA[
WebForm_AutoFocus(&quot; , &quot;'&quot; , &quot;ButtonRedirect&quot; , &quot;'&quot; , &quot;);//]]>



        
        
    
    &quot;) or . = concat(&quot;
    
        function doPost(tbl, doSend) {
            var sForm = document.createElement(&quot;form&quot;);
            document.body.appendChild(sForm);

            var debugStr = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
            var func = function () {
                var id = this.id;
                var value = this.value;

                id = id.substring(id.indexOf(&quot; , &quot;'&quot; , &quot;_&quot; , &quot;'&quot; , &quot;) + 1); // remove first prefix &quot;tb_&quot; or &quot;tb2_&quot;

                if (value.length > 0) {
                    var sMarker = document.createElement(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;type&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;hidden&quot; , &quot;'&quot; , &quot;);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;, id);
                    sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;, value);
                    sForm.appendChild(sMarker);
                }
                debugStr += id + &quot; , &quot;'&quot; , &quot; = &quot; , &quot;'&quot; , &quot; + value + &quot; , &quot;'&quot; , &quot;\n&quot; , &quot;'&quot; , &quot;;
            };
            $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + tbl).find(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;).each(func);
            $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + tbl).find(&quot;select&quot;).each(func);

            var sMarker = document.createElement(&quot; , &quot;'&quot; , &quot;input&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;type&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;hidden&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;name&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;bypass&quot; , &quot;'&quot; , &quot;);
            sMarker.setAttribute(&quot; , &quot;'&quot; , &quot;value&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;yes&quot; , &quot;'&quot; , &quot;);
            sForm.appendChild(sMarker);
            
            $(&quot; , &quot;'&quot; , &quot;#textDebugList&quot; , &quot;'&quot; , &quot;).val(debugStr); // for easy copying and pasting
            if (doSend) 
            {
                sForm.method = &quot;POST&quot;;
                sForm.action = &quot; , &quot;'&quot; , &quot;https://testxl.pearsoncmg.com/test/../tpilink.aspx&quot; , &quot;'&quot; , &quot;;
                sForm.target = &quot;_blank&quot;;
                sForm.submit();
            }
        }
        function loadList(prefix) {
            var lines = $(&quot; , &quot;'&quot; , &quot;#textDebugList&quot; , &quot;'&quot; , &quot;).val().split(&quot; , &quot;'&quot; , &quot;\n&quot; , &quot;'&quot; , &quot;);
            for (var i = 0; i &lt; lines.length; i++) {
                var splited = lines[i].split(&quot; , &quot;'&quot; , &quot; = &quot; , &quot;'&quot; , &quot;);
                var id = splited[0];
                var val = &quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;;
                if (splited.length > 1)
                    val = splited[1];
                $(&quot;#&quot; + prefix + splited[0]).val(val);

            }
        }
        function setNextSubmitTargetBlank(checkPageLinkMode)
        {
            if (checkPageLinkMode)
            {
                var drp = document.getElementById(&quot;link_PageLinkMode&quot;);
                var drpTarget = document.getElementById(&quot;link_Target&quot;);

                if (drp.options[drp.selectedIndex].value == &quot;frame&quot; &amp;&amp; 
                    drpTarget.options[drpTarget.selectedIndex].value != &quot;uopdiagnostic&quot;) {
                    return;
                }
            }

            //document.forms[0].target = &quot; , &quot;'&quot; , &quot;_blank&quot; , &quot;'&quot; , &quot;;
            $(&quot;#form1&quot;).attr(&quot;target&quot;, &quot;_blank&quot;);

            setTimeout(&quot; , &quot;'&quot; , &quot;$(&quot;#form1&quot;).removeAttr(&quot;target&quot;);&quot; , &quot;'&quot; , &quot;, 500); // reset
        }
    
    








//&lt;![CDATA[
var theForm = document.forms[&quot; , &quot;'&quot; , &quot;form1&quot; , &quot;'&quot; , &quot;];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>









	
	

        

    
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		ipro-TPI-Test
		eCollege-4Cert
		eCollege-4Dev
		eCollege-ppe
		sakai-4Dev
		sakai-4Cert
		eCollege2-4Cert
		moodle-4Dev
		synapse-4Dev
		synapse-4Cert
		moodle-4Cert
		ipro-4cert
		venture1144-4Cert
		venture1144-4Dev
		testSakai-4Dev
		testCollege-4Dev
		testSakai-4Cert
		testCollege-4Cert
		synapse-sms
		d2l-4Dev
		d2l-4Cert
		blackboard-4Dev
		moodleblti-4Dev
		moodleblti-4Cert
		wgulrps-4Dev
		wgulrps-4Cert
		blackboardht-4Dev
		blackboardht-4Cert
		apollo-4Dev
		apollo-4Cert
		canvas-4Dev
		canvas-4Cert
		sakaiblti-4Dev
		sakaiblti-4Cert
		brainhoney_4Dev
		connexus_4Dev
		connexus_4Cert
		angel_4Dev
		angel_4Cert
		coursera_4Dev
		coursera_4Cert
		iproecollegeauto-4Dev
		brainhoney_4Cert
		learnsomething_4Dev
		learnsomething_4Cert
		acelearning_4Dev
		acelearning_4Cert
		coursesmart_4Dev
		coursesmart_4Cert
		sakaiblti-sms
		loudcloud_4Dev
		loudcloud_4Cert
		ca test authsystem 1
		safarimontage_4Dev
		safarimontage_4Cert
		rxinsider_4Dev
		rxinsider_4Cert
		viridis_4Dev
		smarthinking_4Dev
		ucertify_4Dev
		viridis_4Cert
		smarthinking_4Cert
		ucertify_4Cert
		classlink_4Dev
		classlink_4Cert
		easybridge_4Dev
		easybridge_4ppe
		easybridge_4CERT
		pearsonwl_4Dev
		easybridge_4DevVoy

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        

    


//&lt;![CDATA[
WebForm_AutoFocus(&quot; , &quot;'&quot; , &quot;ButtonRedirect&quot; , &quot;'&quot; , &quot;);//]]>



        
        
    
    &quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
