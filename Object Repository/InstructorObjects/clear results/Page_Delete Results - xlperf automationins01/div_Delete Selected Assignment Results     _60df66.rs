<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Delete Selected Assignment Results     _60df66</name>
   <tag></tag>
   <elementGuidId>2607f0d4-7d72-4e85-b2da-75e0d06e532d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='DeleteConfirmModal']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>efa3181d-3dd3-4ee0-82b3-0bd37de3f748</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content</value>
      <webElementGuid>7629ec77-c1e5-455c-b6f7-f59b5b289b2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Delete Selected Assignment Results?
            
            
                Results for these assignments will be permanently deleted for the whole class.
                
                Warning: You cannot undo this action.
            
            
                Permanently Delete Results
                Cancel
            
        </value>
      <webElementGuid>cdc8368e-59c9-478c-ac1d-862d9baf0326</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;DeleteConfirmModal&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]</value>
      <webElementGuid>bb109b9f-9e9d-42c2-bec2-e1c81e6a06e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='DeleteConfirmModal']/div/div</value>
      <webElementGuid>e274d470-744a-44ef-ad90-2911c3b95cb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete Selected Results'])[1]/following::div[6]</value>
      <webElementGuid>3b9c4383-31ec-45aa-884d-e5bbdad73e8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Done'])[1]/following::div[6]</value>
      <webElementGuid>dd3df696-52e4-475b-b0da-216544814a18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div</value>
      <webElementGuid>bb97bf42-1554-4557-a29c-9b1f452fcf54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Delete Selected Assignment Results?
            
            
                Results for these assignments will be permanently deleted for the whole class.
                
                Warning: You cannot undo this action.
            
            
                Permanently Delete Results
                Cancel
            
        ' or . = '
            
                Delete Selected Assignment Results?
            
            
                Results for these assignments will be permanently deleted for the whole class.
                
                Warning: You cannot undo this action.
            
            
                Permanently Delete Results
                Cancel
            
        ')]</value>
      <webElementGuid>4f4292fb-87aa-47a5-b5be-1e63550d058a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
