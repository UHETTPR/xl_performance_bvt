<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Your updates have been saved. Some studen_f25684</name>
   <tag></tag>
   <elementGuidId>c6a4a81d-a2c7-47d0-a244-417e1d9700e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalBody0']/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modalBody0 > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>bd74b0dc-ea3b-4af6-bfe1-34729610a328</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your updates have been saved. Some students did not have results that matched your deletion selection.</value>
      <webElementGuid>e60f1c80-6cba-411e-b66d-462a79d8f026</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalBody0&quot;)/p[1]</value>
      <webElementGuid>dec58861-1c8c-404b-a340-d514bc3d158d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalBody0']/p</value>
      <webElementGuid>3b6e19ff-04cb-47cc-b31c-6ac851695d63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/following::p[1]</value>
      <webElementGuid>9ed6ff2a-af6c-4407-a5b2-ed066f23f80c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Policy'])[1]/following::p[1]</value>
      <webElementGuid>b331224d-0ed1-48f7-9e60-7af1a0a4dfaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::p[1]</value>
      <webElementGuid>b0cfa436-64c1-4af4-b222-070d90a73c18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'modalBody0', '&quot;', ')/p[1]')])[1]/preceding::p[1]</value>
      <webElementGuid>6b56968d-0602-4211-bec0-d4a3923d0182</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your updates have been saved. Some students did not have results that matched your deletion selection.']/parent::*</value>
      <webElementGuid>eba4d1bf-d37e-4694-9b16-1225a7ad063f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/p</value>
      <webElementGuid>6b5bbb5b-b174-49d0-8cd0-28f6611a7de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Your updates have been saved. Some students did not have results that matched your deletion selection.' or . = 'Your updates have been saved. Some students did not have results that matched your deletion selection.')]</value>
      <webElementGuid>21634912-4430-4e4c-8a1d-bc2db981e9a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
