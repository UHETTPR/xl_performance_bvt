<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Delete Results</name>
   <tag></tag>
   <elementGuidId>7165dab6-0415-445e-900e-2719b55c7680</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Delete Results')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>751e485f-87a5-48f8-a143-6c3f8cafa6d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:MoreToolsClick('Delete Results')</value>
      <webElementGuid>7f409300-2c58-4ae7-a1cf-160c1e82f5d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete Results</value>
      <webElementGuid>f702a9dc-08ca-4c49-9dae-fa0b2766954c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-dropdown&quot;]/div[@class=&quot;popover popover-menu fade bottom in&quot;]/div[@class=&quot;popover-content&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>0f22dc6f-15c0-4800-bb6d-0061c75d53f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[5]/div/div[2]/ul/li[3]/a</value>
      <webElementGuid>8095f81b-619b-4878-838d-0abd4c391ed3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Delete Results')]</value>
      <webElementGuid>c07ad27b-190a-4e3d-9985-15550d2dae90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear Study Plan'])[1]/following::a[1]</value>
      <webElementGuid>da06fa1b-f7ef-4319-80b7-0aee535bccb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add/Edit Student IDs'])[1]/following::a[2]</value>
      <webElementGuid>fc3a80c2-46c9-46db-9589-674e9c65367a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Lowest Scores'])[1]/preceding::a[1]</value>
      <webElementGuid>588c3ac7-5f14-417b-a2f4-139000c48139</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Roster'])[1]/preceding::a[2]</value>
      <webElementGuid>d25ce0c0-aadc-43df-874a-3ed13059c00c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delete Results']/parent::*</value>
      <webElementGuid>d202f32a-cb3d-4880-b960-2f5de18066f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:MoreToolsClick('Delete Results')&quot;)]</value>
      <webElementGuid>b9ce7450-98c7-497b-8060-1a939c3efb41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[3]/a</value>
      <webElementGuid>ebfd7e27-6bec-4fda-a046-0b7d75f15c9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:MoreToolsClick(&quot; , &quot;'&quot; , &quot;Delete Results&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Delete Results' or . = 'Delete Results')]</value>
      <webElementGuid>231477ee-c5dc-4bf1-8df7-426ff6be5938</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
