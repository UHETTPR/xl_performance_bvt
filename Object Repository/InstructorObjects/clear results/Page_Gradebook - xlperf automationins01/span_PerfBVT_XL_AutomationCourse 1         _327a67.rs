<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PerfBVT_XL_AutomationCourse 1         _327a67</name>
   <tag></tag>
   <elementGuidId>b02d90b5-6647-4626-b30e-6eca12aa701f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_BreadCrumb1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e2067fd7-7aca-49bc-86c7-9cbe622a88f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_BreadCrumb1</value>
      <webElementGuid>34c2a368-29ad-4025-818a-b5de03fa8089</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>breadcrumb</value>
      <webElementGuid>080b3ca9-e9d3-4ddf-9c6d-1d5ad71de73c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


    
        
            PerfBVT_XL_AutomationCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          PerfBVT_XL_AutomationCourse [1]
        
    




     
    

</value>
      <webElementGuid>3f09f45e-c1e5-4232-b216-562ffc25f128</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_BreadCrumb1&quot;)</value>
      <webElementGuid>be2f2471-4706-4207-a0d5-9a037f38bd11</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_BreadCrumb1']</value>
      <webElementGuid>4b4d6a3c-7678-4df7-94a0-dd44bb4bc462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_PnlBreadCrumb']/span[2]</value>
      <webElementGuid>8cbd7a73-6465-46b2-a2b0-36b9b1b57fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gradebook'])[1]/following::span[2]</value>
      <webElementGuid>1daa9255-e80d-421c-be04-bfa3beb3ebef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/following::span[2]</value>
      <webElementGuid>96404ea1-3f76-41bd-800d-bd48cd1ad4b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>51cc5e1e-e19f-497e-a53e-cac9ccc9fc43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_BreadCrumb1' and (text() = '


    
        
            PerfBVT_XL_AutomationCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          PerfBVT_XL_AutomationCourse [1]
        
    




     
    

' or . = '


    
        
            PerfBVT_XL_AutomationCourse [1]
            
        
        
          
              
              Manage Course List
          
          
          PerfBVT_XL_AutomationCourse [1]
        
    




     
    

')]</value>
      <webElementGuid>366ad068-25eb-4aea-9111-ffe5f1b435bf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
