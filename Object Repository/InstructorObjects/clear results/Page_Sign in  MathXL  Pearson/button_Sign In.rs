<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In</name>
   <tag></tag>
   <elementGuidId>d8653eed-37de-4591-af48-bb9dfc8a3dff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.button.button-big-icon.bg-color-match-medium.uppercase.mar-top-x1Half</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7004e32d-9446-4a68-a196-56e02839ed27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>a6c30a1f-f44d-4684-ae99-618c4aa57ee4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium uppercase mar-top-x1Half</value>
      <webElementGuid>44cf427f-d45a-4781-89e8-305a9335ffea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Sign In
                        </value>
      <webElementGuid>4f44e740-e74a-490c-898e-d375ef7d457c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inputfrm&quot;)/button[@class=&quot;button button-big-icon bg-color-match-medium uppercase mar-top-x1Half&quot;]</value>
      <webElementGuid>fb87b4b3-cc05-4d22-9e29-c87229a2222b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>ba648a3f-e1eb-40f8-8dc1-cede63281d4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='inputfrm']/button</value>
      <webElementGuid>ad9ffb61-d1f6-4d9e-b468-7664c060dce8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>b8874396-1c18-4eb1-9312-faf45480cc59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::button[1]</value>
      <webElementGuid>bfa03973-47d3-48e3-9aec-19e9e9026d59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/preceding::button[1]</value>
      <webElementGuid>67ea3235-5e84-4fb0-a3a8-ddefc992529c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit our home page to register!'])[1]/preceding::button[1]</value>
      <webElementGuid>e31dba58-bb06-48b6-99f1-07671d7d567d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>15bad6f1-60a3-4946-96db-414e1c31a110</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>d2f27c4e-3a66-4bb0-9c82-9aa9e73e310d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                            Sign In
                        ' or . = '
                            Sign In
                        ')]</value>
      <webElementGuid>e136c0fb-1787-46b3-aa49-a7452e5d7ac4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
