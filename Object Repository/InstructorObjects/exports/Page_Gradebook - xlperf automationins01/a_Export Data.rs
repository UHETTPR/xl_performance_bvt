<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Export Data</name>
   <tag></tag>
   <elementGuidId>4ae26f20-831b-429c-be45-9f3457da96fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Export Data')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>67917dd9-5be5-43fc-ba6e-1491b7e70935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/ExportData.aspx?sender=Gradebook.aspx</value>
      <webElementGuid>708c3232-b5fa-4ca6-8d79-e01fcef8c6ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Export Data</value>
      <webElementGuid>385c19e3-aa14-4275-9b07-6061385a67e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>c36287c5-80fa-433d-85c5-90f72e9e8b93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Export Data</value>
      <webElementGuid>bd09c666-2627-4bfa-a333-06c92d6818cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Toolbar_collapse_toolbar1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;toolbar-menu-item toolbar-menu-item-link&quot;]/a[1]</value>
      <webElementGuid>b13fd550-1595-4512-adb3-b48c58d61578</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Toolbar_collapse_toolbar1']/ul/li[4]/a</value>
      <webElementGuid>1868260a-dc97-4914-8fa3-54327024baa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Export Data')]</value>
      <webElementGuid>c456b547-b238-4022-80f1-e4318e8fe60a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Offline Items'])[1]/following::a[1]</value>
      <webElementGuid>66fec860-a800-4928-80ba-3ddf982b17f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Weights'])[1]/following::a[2]</value>
      <webElementGuid>fa20aa5b-2cea-4786-b498-7e508858780e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More Tools'])[1]/preceding::a[1]</value>
      <webElementGuid>2cd5c2d5-b400-4996-9806-691d304aa9f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View Results By'])[1]/preceding::a[2]</value>
      <webElementGuid>e452965c-4adc-4179-8279-b47e1edef9aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Export Data']/parent::*</value>
      <webElementGuid>9bd7482f-b405-4d8b-b409-46f4cbf00d15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/ExportData.aspx?sender=Gradebook.aspx')]</value>
      <webElementGuid>10e1fb21-4886-4064-b437-92e5440264c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div/ul/li[4]/a</value>
      <webElementGuid>a2afb4cb-c079-486d-837c-76cf49d52a48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/ExportData.aspx?sender=Gradebook.aspx' and (text() = 'Export Data' or . = 'Export Data')]</value>
      <webElementGuid>e3c99efc-6d57-493f-9923-8581daa0696a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
