<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Study Plan</name>
   <tag></tag>
   <elementGuidId>9c548077-d6ad-49bb-a078-5ea2de50fcb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Study Plan')])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f6336c6f-8f6d-4b7a-bdfa-af8b8ed8c900</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:goView('Study Plan');</value>
      <webElementGuid>d866c6ec-a0c5-4d1f-938c-b4c54035187f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Study Plan</value>
      <webElementGuid>90895da2-394d-4053-a304-2d12ea43588d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/div[@class=&quot;xlbootstrap3&quot;]/app-root[1]/app-gradebook[1]/app-instructor-links[1]/table[@class=&quot;sub-subnav-instructor-links&quot;]/tbody[1]/tr[1]/td[@class=&quot;group&quot;]/ul[1]/li[3]/a[1]</value>
      <webElementGuid>811b0d45-a640-455d-896c-59e4bedeaeff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[2]/app-root/app-gradebook/app-instructor-links/table/tbody/tr/td/ul/li[3]/a</value>
      <webElementGuid>229e76db-44ef-4c31-98f2-38546f431d81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Study Plan')])[3]</value>
      <webElementGuid>1c0fbb5b-7c94-43fa-a852-ed911031fb3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student Overview'])[1]/following::a[1]</value>
      <webElementGuid>d7712072-173b-45eb-bbf8-a9927f8b16d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments'])[1]/following::a[2]</value>
      <webElementGuid>eca3a099-3533-42a9-afe2-f97f631c92e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/preceding::a[1]</value>
      <webElementGuid>fccdcf93-0dbe-457a-9408-aec45c970616</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alerts'])[1]/preceding::a[2]</value>
      <webElementGuid>6ccaf765-36f1-4de0-a688-78af09b68e47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:goView('Study Plan');&quot;)]</value>
      <webElementGuid>6e85111e-97e9-422a-8d54-e7c8baf2a5eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/ul/li[3]/a</value>
      <webElementGuid>659311c6-45a1-4a60-ab83-1316b01d8e6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:goView(&quot; , &quot;'&quot; , &quot;Study Plan&quot; , &quot;'&quot; , &quot;);&quot;) and (text() = 'Study Plan' or . = 'Study Plan')]</value>
      <webElementGuid>de04acb5-e321-45c5-b1f7-523f8fa3fe0f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
