<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Gradebook</name>
   <tag></tag>
   <elementGuidId>b2c32060-91c9-4cdc-b490-c73cc8729f3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Gradebook')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>437a6625-5466-4ed7-b2c7-b4210220003f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Instructor/Gradebook.aspx</value>
      <webElementGuid>f60d6113-1616-4f7e-b85c-92668de936ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>2ff9f701-756d-43cc-ba8c-2c8999239323</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>603b7754-5970-4607-a082-733c6222b351</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;35&quot;)</value>
      <webElementGuid>19b0ce00-0b89-4b2f-9a51-4f5ae0c8c427</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>8e166689-d10d-4e55-a1f6-812e46207e7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gradebook</value>
      <webElementGuid>47cefa6a-8d50-4ecb-ac7c-18da346d0286</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>dab1033b-52a3-40ac-92f1-ee9ce09366d6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;35&quot;)']</value>
      <webElementGuid>8b8dc833-1540-4db0-971a-440d3f32c60b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[14]/a</value>
      <webElementGuid>079bf90b-2fc6-41d5-a9d5-19b458ba642b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Gradebook')]</value>
      <webElementGuid>1f3d878b-caa7-414b-9de7-3b9c6e5fe3b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan Manager'])[1]/following::a[1]</value>
      <webElementGuid>b598f448-2af0-4e2f-bf00-e7b7a401f9d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignment Manager'])[1]/following::a[2]</value>
      <webElementGuid>7f2b70a0-0e46-47f2-b68c-401c27542a1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PerfBVT_XL_AutomationCourse [1]'])[1]/preceding::a[2]</value>
      <webElementGuid>bd90c8f3-44a8-42ec-a174-9ec055106e8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage Course List'])[1]/preceding::a[3]</value>
      <webElementGuid>7bc29d22-33ac-4bdd-ac4a-3c464a965da0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Gradebook']/parent::*</value>
      <webElementGuid>3e7596f6-c90f-45e6-a844-3c659b6baa62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Instructor/Gradebook.aspx')]</value>
      <webElementGuid>683c8fe0-9242-43ae-a791-8c279a88b08c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/a</value>
      <webElementGuid>561af160-7fa6-4972-a9f4-0ed32beb0324</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Instructor/Gradebook.aspx' and @title = 'Gradebook' and (text() = 'Gradebook' or . = 'Gradebook')]</value>
      <webElementGuid>8ae79626-722b-4551-87f6-2b0ced898bbd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
