<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Study Plan</name>
   <tag></tag>
   <elementGuidId>ce7a105f-cc30-49eb-9832-7d15ad793aca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Study Plan')])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ad50a99f-684a-4912-8cba-09744ecf9174</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl19&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookStudyPlan.aspx&quot;, false, true))</value>
      <webElementGuid>aae1a8ff-4c18-4aa8-adb3-4cae8f39c1b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Study Plan</value>
      <webElementGuid>e806c362-ffb7-4f8b-93d5-9d94b36b1173</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TabControlGB_TabUl&quot;)/li[@id=&quot;li&quot;]/a[1]</value>
      <webElementGuid>e90a3b06-0719-447d-85c3-f7dec39c48c1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='li']/a)[2]</value>
      <webElementGuid>eb2d904d-b2a1-4784-9ac5-a00b399d8e92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Study Plan')])[3]</value>
      <webElementGuid>d1e3d905-3715-45d5-a5a7-d25d5887117e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student Overview'])[1]/following::a[1]</value>
      <webElementGuid>87b7d910-6203-4aea-b325-ebc166a0d4f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other'])[1]/following::a[2]</value>
      <webElementGuid>d9da7895-0a0d-4d7b-9547-38b2231a6579</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/preceding::a[1]</value>
      <webElementGuid>66fcf745-1c52-4f24-9e09-a6b5c1b6867e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Student Tags'])[1]/preceding::a[2]</value>
      <webElementGuid>c9889077-e5c2-4c88-9c1b-2da64c8d9f7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl19&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookStudyPlan.aspx&quot;, false, true))')]</value>
      <webElementGuid>dd0c20aa-cd4b-4a37-bc15-16a70bf84dad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/ul/li[3]/a</value>
      <webElementGuid>9328f630-0a3e-4df3-905e-6df4cf6be831</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl19&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookStudyPlan.aspx&quot;, false, true))' and (text() = 'Study Plan' or . = 'Study Plan')]</value>
      <webElementGuid>64ac9247-ab65-4191-83bd-55bb621ae947</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
