<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_All Student Tags</name>
   <tag></tag>
   <elementGuidId>79e95fb3-cd04-4455-918b-9e7898950f76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#All_Student_Tags</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='All_Student_Tags']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e44fa374-3edb-459f-86a7-ca20a725a542</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>All_Student_Tags</value>
      <webElementGuid>158ec287-25cd-43d8-9e25-1b552fd41eef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn btn-default dropdown-toggle dropdown-toggle btn-xl-toggle</value>
      <webElementGuid>de231271-849c-417d-b378-3edd4829848f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>713fea88-ed46-46b6-9cca-15490668a299</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>2e1188b0-fe75-4d84-b7e1-d37b7751645c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-active-css</name>
      <type>Main</type>
      <value>btn-default</value>
      <webElementGuid>12ee3f9a-df6c-441d-b1d7-aa405ee556c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d140ffe5-8723-4fdd-9e8d-c3fd6b31eb3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e323009a-07f0-4bf8-adef-2bc9041302a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>ul_All Student Tags</value>
      <webElementGuid>2070ecda-3c6c-410d-ba17-05547b7aea38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> All Student Tags </value>
      <webElementGuid>6c1f6cf9-585b-4333-8b32-053b437897ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;All_Student_Tags&quot;)</value>
      <webElementGuid>18bd8017-895e-4d0a-92d9-4861eccc1728</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='All_Student_Tags']</value>
      <webElementGuid>f38e36f3-974b-45f5-9929-413a732d9e6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FilterButtons']/div/div/button</value>
      <webElementGuid>3359ab5f-ed2c-491c-9bb5-309b93c27697</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Performance by Chapter'])[1]/following::button[1]</value>
      <webElementGuid>6505d66b-2739-40a2-ae64-875718196997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/following::button[1]</value>
      <webElementGuid>fa23877b-29f3-4658-a63f-99222ce046c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Student Tags'])[2]/preceding::button[1]</value>
      <webElementGuid>c22a5c61-cebf-4779-a650-7d07c9559e33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>10def5c0-8aa1-40ee-8104-fe8e9aa7ade8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'All_Student_Tags' and @type = 'button' and (text() = ' All Student Tags ' or . = ' All Student Tags ')]</value>
      <webElementGuid>ec37404c-e5a4-4b92-b853-d94887483995</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
