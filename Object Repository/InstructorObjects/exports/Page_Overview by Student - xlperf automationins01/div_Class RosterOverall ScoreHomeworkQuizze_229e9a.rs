<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Class RosterOverall ScoreHomeworkQuizze_229e9a</name>
   <tag></tag>
   <elementGuidId>4a05e9b8-2d0e-45f7-b3f8-c21fd4f805e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#gradebookOverviewStudent</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='gradebookOverviewStudent']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ecdb383c-1c71-42bd-9487-3978f384dce9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>gradebookOverviewStudent</value>
      <webElementGuid>b5096fc5-a197-4922-a8e2-a842ed285e1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlbootstrap3</value>
      <webElementGuid>11c95f09-b59f-448f-8ba4-74d08238e455</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        


Class RosterOverall ScoreHomeworkQuizzesTestsOtherStudy Plan

Category Weight
-
30 pt
20 pt
50 pt
0 pt
0 pt



Class Average
26.65%
29.41%

25%

0%



automationstu01, xlperf
26.65%
29.41%
--
25%
--
0%

automationins01, xlperf
--
--
--
--
--
--


 </value>
      <webElementGuid>0de08f6b-d9ad-47a5-9eac-7e08bfa85567</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gradebookOverviewStudent&quot;)</value>
      <webElementGuid>5bb8ab50-7889-4a71-9b12-ca65e58ce13c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='gradebookOverviewStudent']</value>
      <webElementGuid>54112880-9315-4dd8-ab43-cfc2330b73b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/div[5]</value>
      <webElementGuid>d8988fe1-1180-4c05-bc35-43edf935557e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stag1'])[1]/following::div[1]</value>
      <webElementGuid>e0860daa-6e1f-4f7e-9871-8c87481e7d30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Student Tags'])[2]/following::div[1]</value>
      <webElementGuid>bb090d67-13b2-4b10-8daf-97006bf9a48a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[5]</value>
      <webElementGuid>6e2f73cd-6536-48bc-9f80-6f505cc510db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'gradebookOverviewStudent' and (text() = '
        


Class RosterOverall ScoreHomeworkQuizzesTestsOtherStudy Plan

Category Weight
-
30 pt
20 pt
50 pt
0 pt
0 pt



Class Average
26.65%
29.41%

25%

0%



automationstu01, xlperf
26.65%
29.41%
--
25%
--
0%

automationins01, xlperf
--
--
--
--
--
--


 ' or . = '
        


Class RosterOverall ScoreHomeworkQuizzesTestsOtherStudy Plan

Category Weight
-
30 pt
20 pt
50 pt
0 pt
0 pt



Class Average
26.65%
29.41%

25%

0%



automationstu01, xlperf
26.65%
29.41%
--
25%
--
0%

automationins01, xlperf
--
--
--
--
--
--


 ')]</value>
      <webElementGuid>bd20acf9-8ed2-45d7-a292-9b0247802594</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
