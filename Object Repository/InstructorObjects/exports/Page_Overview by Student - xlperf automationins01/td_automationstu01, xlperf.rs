<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu01, xlperf</name>
   <tag></tag>
   <elementGuidId>6d3c10de-b33e-45a7-ad3e-030434388bf1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#gbbody > tr > td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>53ec570a-d430-42ad-9d9e-0a2e9fcaf9d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>c523281c-0931-4733-bbe1-2c2f5e057650</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gbbody&quot;)/tr[1]/td[1]</value>
      <webElementGuid>2d21f69c-5deb-413f-b4bc-0492ec1763e7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='gbbody']/tr/td</value>
      <webElementGuid>10250499-ccc2-4762-aebe-16ae082a6f7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Class Average'])[1]/following::td[7]</value>
      <webElementGuid>a826a821-b28e-4e72-ab5f-da73d34044f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[1]/following::td[13]</value>
      <webElementGuid>07c74f88-7062-4324-90cb-c3a7bf137b21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='--'])[1]/preceding::td[3]</value>
      <webElementGuid>f886ad87-6c6f-45f9-9e79-1ab43e7d8aab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tbody[2]/tr/td</value>
      <webElementGuid>7f7b9cb3-8998-4771-b7d5-afded68dc812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>817309a1-7582-49cc-b17c-2d3d7680f5c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
