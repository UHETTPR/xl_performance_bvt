<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu02, xlperf</name>
   <tag></tag>
   <elementGuidId>ef2a3322-9d0b-4228-87d3-ec474aba79e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = 'automationstu02, xlperf' or . = 'automationstu02, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#gbbody > tr > td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>4e1ebc8c-c1bb-440a-80f5-5bebc4abcfd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>62265265-473f-4fd9-bb77-67534e74492b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gbbody&quot;)/tr[1]/td[1]</value>
      <webElementGuid>b45ba5e1-0bd6-4f27-b3cd-137459549e36</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tbody[@id='gbbody']/tr/td</value>
      <webElementGuid>5158850b-3d2c-42a4-9556-73a1231743cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Class Average'])[1]/following::td[7]</value>
      <webElementGuid>36b871f0-cb88-426f-ac83-4d62c29fdf55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[1]/following::td[13]</value>
      <webElementGuid>8fedce0f-c21d-4a9e-89ef-b55e1c7f840f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='--'])[1]/preceding::td[3]</value>
      <webElementGuid>4420961f-ef40-4612-aee7-0bc055f72b9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tbody[2]/tr/td</value>
      <webElementGuid>ab3649aa-4b8a-42f5-94e4-6d040f58a641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>389adc62-2b4c-4808-ab78-7c408cdfa73b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
