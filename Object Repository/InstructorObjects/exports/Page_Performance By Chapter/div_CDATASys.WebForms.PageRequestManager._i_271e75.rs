<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_CDATASys.WebForms.PageRequestManager._i_271e75</name>
   <tag></tag>
   <elementGuidId>e81626ee-a7b0-4ec9-801e-78c5b8904522</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.xlbootstrap</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9ee77479-b1e0-4e42-86a1-2518bfad0cb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlbootstrap</value>
      <webElementGuid>5d928710-35e2-4f27-9eb9-2b472a5999ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                    

//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$InsideForm$MasterContent$ScriptManager1', 'aspnetForm', [], [], [], 90, 'ctl00$ctl00');
//]]>





    function getHeaderText(){
        return $('#PageHeaderTitle').get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $('#PageHeaderTitle').get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    
 
        
        
      
    
    


          
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools   
    
    

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
      
  
    Note: The test, quiz, and homework scores below are based on the number of questions on which students received full credit from each content area for all attempts on all assignments.  
        
	
		
			Book ContentsHomework Quizzes(all attempts)Tests(all attempts)Study Plan 
		
	
		
			
				
					Ch. O. Orientation Questions for Students
				
			    
		
			
				
					Ch. 1. Review of Real Numbers
				
			29.4% 25%0%
		
			
				
					Ch. 2. Equations, Inequalities, and Problem Solving
				
			    
		
			
				
					Ch. 3. Graphing
				
			    
		
			
				
					Ch. 4. Solving Systems of Linear Equations
				
			    
		
			
				
					Ch. 5. Exponents and Polynomials
				
			    
		
			
				
					Ch. 6. Factoring Polynomials
				
			    
		
			
				
					Ch. 7. Rational Expressions
				
			    
		
			
				
					Ch. 8. More on Functions and Graphs
				
			    
		
			
				
					Ch. 9. Inequalities and Absolute Value
				
			    
		
			
				
					Ch. 10. Rational Exponents, Radicals, and Complex Numbers
				
			    
		
			
				
					Ch. 11. Quadratic Equations and Functions
				
			    
		
			
				
					Ch. 12. Exponential and Logarithmic Functions
				
			    
		
			
				
					Ch. 13. Conic Sections
				
			    
		
			
				
					Ch. 14. Sequences, Series, and the Binomial Theorem
				
			    
		
			
				
					Ch. Appendix. Appendices
				
			    
		
			
				
					Ch. GA. Online Appendix: Additional Geometry Questions
				
			    
		
			
				
					Ch. Mma. Mathematica test questions
				
			    
		
			
		
	

    
    
                                                                </value>
      <webElementGuid>97791fc6-021f-43de-be83-5119b83bfd57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]</value>
      <webElementGuid>d11cce1e-970c-4281-b99e-92a3f4519ba1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]</value>
      <webElementGuid>19a53860-5931-40e4-b4c8-eb9af5bd2c8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL Load Testbook'])[1]/following::div[2]</value>
      <webElementGuid>5446dd2b-3308-41a5-ab22-3468e4a98a3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='xlperf automationins01'])[3]/following::div[2]</value>
      <webElementGuid>d60dac1d-9a9d-4eb8-915b-65763a179144</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[4]</value>
      <webElementGuid>99eeb755-2e3e-495d-9f1b-eea3fb331dee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                                                                    

//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>





    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    


          
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools   
    
    

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
      
  
    Note: The test, quiz, and homework scores below are based on the number of questions on which students received full credit from each content area for all attempts on all assignments.  
        
	
		
			Book ContentsHomework Quizzes(all attempts)Tests(all attempts)Study Plan 
		
	
		
			
				
					Ch. O. Orientation Questions for Students
				
			    
		
			
				
					Ch. 1. Review of Real Numbers
				
			29.4% 25%0%
		
			
				
					Ch. 2. Equations, Inequalities, and Problem Solving
				
			    
		
			
				
					Ch. 3. Graphing
				
			    
		
			
				
					Ch. 4. Solving Systems of Linear Equations
				
			    
		
			
				
					Ch. 5. Exponents and Polynomials
				
			    
		
			
				
					Ch. 6. Factoring Polynomials
				
			    
		
			
				
					Ch. 7. Rational Expressions
				
			    
		
			
				
					Ch. 8. More on Functions and Graphs
				
			    
		
			
				
					Ch. 9. Inequalities and Absolute Value
				
			    
		
			
				
					Ch. 10. Rational Exponents, Radicals, and Complex Numbers
				
			    
		
			
				
					Ch. 11. Quadratic Equations and Functions
				
			    
		
			
				
					Ch. 12. Exponential and Logarithmic Functions
				
			    
		
			
				
					Ch. 13. Conic Sections
				
			    
		
			
				
					Ch. 14. Sequences, Series, and the Binomial Theorem
				
			    
		
			
				
					Ch. Appendix. Appendices
				
			    
		
			
				
					Ch. GA. Online Appendix: Additional Geometry Questions
				
			    
		
			
				
					Ch. Mma. Mathematica test questions
				
			    
		
			
		
	

    
    
                                                                &quot;) or . = concat(&quot;
                                                                    

//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>





    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    


          
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools   
    
    

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
      
  
    Note: The test, quiz, and homework scores below are based on the number of questions on which students received full credit from each content area for all attempts on all assignments.  
        
	
		
			Book ContentsHomework Quizzes(all attempts)Tests(all attempts)Study Plan 
		
	
		
			
				
					Ch. O. Orientation Questions for Students
				
			    
		
			
				
					Ch. 1. Review of Real Numbers
				
			29.4% 25%0%
		
			
				
					Ch. 2. Equations, Inequalities, and Problem Solving
				
			    
		
			
				
					Ch. 3. Graphing
				
			    
		
			
				
					Ch. 4. Solving Systems of Linear Equations
				
			    
		
			
				
					Ch. 5. Exponents and Polynomials
				
			    
		
			
				
					Ch. 6. Factoring Polynomials
				
			    
		
			
				
					Ch. 7. Rational Expressions
				
			    
		
			
				
					Ch. 8. More on Functions and Graphs
				
			    
		
			
				
					Ch. 9. Inequalities and Absolute Value
				
			    
		
			
				
					Ch. 10. Rational Exponents, Radicals, and Complex Numbers
				
			    
		
			
				
					Ch. 11. Quadratic Equations and Functions
				
			    
		
			
				
					Ch. 12. Exponential and Logarithmic Functions
				
			    
		
			
				
					Ch. 13. Conic Sections
				
			    
		
			
				
					Ch. 14. Sequences, Series, and the Binomial Theorem
				
			    
		
			
				
					Ch. Appendix. Appendices
				
			    
		
			
				
					Ch. GA. Online Appendix: Additional Geometry Questions
				
			    
		
			
				
					Ch. Mma. Mathematica test questions
				
			    
		
			
		
	

    
    
                                                                &quot;))]</value>
      <webElementGuid>f04e87ce-e01e-48e7-b53b-14e80fbb440d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
