<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Ch. O. Orientation Questions for Studen_531166</name>
   <tag></tag>
   <elementGuidId>e1d3ca9f-3e13-4546-8a41-0ea16403eada</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_treeview_0_1']/td/table/tbody/tr/td[2]/a/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_treeview_0_1img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>249345e1-d64b-4897-8624-0c98aaeff24e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>../App_Themes/Math/images/xlicon-disclosure-open.gif</value>
      <webElementGuid>c995c717-e2a7-45d0-8ceb-c684062add39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>border</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>26041b3b-d265-48d8-bf04-a43d86829d5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_treeview_0_1img</value>
      <webElementGuid>f60d1378-8c62-46cb-8d12-5d62a97348ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_treeview_0_1img&quot;)</value>
      <webElementGuid>bb60fee1-6893-4a7a-a4ca-ac55169bae94</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//img[@id='ctl00_ctl00_InsideForm_MasterContent_treeview_0_1img']</value>
      <webElementGuid>8060789a-4f82-4b5e-b368-1c07e34cec8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_treeview_0_1']/td/table/tbody/tr/td[2]/a/img</value>
      <webElementGuid>47517465-e9ab-4788-b9f2-7767726d1ba2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td/table/tbody/tr/td[2]/a/img</value>
      <webElementGuid>3b8df9b7-225c-4f53-b7d8-ea105ea81981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '../App_Themes/Math/images/xlicon-disclosure-open.gif' and @id = 'ctl00_ctl00_InsideForm_MasterContent_treeview_0_1img']</value>
      <webElementGuid>737e593c-dd10-426b-ba0a-614f14931596</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
