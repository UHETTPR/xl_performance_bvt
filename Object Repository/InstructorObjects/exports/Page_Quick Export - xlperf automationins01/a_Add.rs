<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Add</name>
   <tag></tag>
   <elementGuidId>4d3291ac-5d22-4b1a-9c75-737376b1cce2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id = 'assignmentTagAddEditLink' and (text() = 'Add...' or . = 'Add...')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#assignmentTagAddEditLink</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fd489f31-819d-41be-9b84-b9f7702c247d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>assignmentTagAddEditLink</value>
      <webElementGuid>0f267818-b4be-4b8c-b583-82d3ca0ad449</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>ac37ded9-862c-416d-aedd-75bffeb3ec8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#tagModal</value>
      <webElementGuid>00bbcabb-f921-4762-a0db-d8e505bb1830</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>checkAssignmentTagOption();</value>
      <webElementGuid>d1b20b3e-42ba-4486-bf04-ce17c6b8584d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add...</value>
      <webElementGuid>f489a36e-44f9-4da0-a3b6-3fd449527a4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assignmentTagAddEditLink&quot;)</value>
      <webElementGuid>2c0ab17f-810e-4a17-a9e6-aa2190a776b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='assignmentTagAddEditLink']</value>
      <webElementGuid>50f95937-57e8-4a45-87d2-04f652283318</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assignmentTagSectionDiv']/a</value>
      <webElementGuid>011132fd-97b3-4d43-ac10-35cb369eec7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Add...')]</value>
      <webElementGuid>1c94569c-932e-4b43-bf11-facfa826e579</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='None'])[1]/following::a[1]</value>
      <webElementGuid>9f285ea3-1cf6-405f-b757-bb9b82bdcce7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments with tag(s):'])[1]/following::a[1]</value>
      <webElementGuid>efb71944-0d03-412a-9902-0f46a309bba4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Include Omitted Results'])[1]/preceding::a[1]</value>
      <webElementGuid>9209d33e-253c-472d-a4fc-17deffd689d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapters'])[1]/preceding::a[1]</value>
      <webElementGuid>bc50da95-b628-4049-a415-69035ccb9c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add...']/parent::*</value>
      <webElementGuid>fbe27e92-eda0-496c-ab7c-c4702debf1b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div/div/div[2]/a</value>
      <webElementGuid>324f8d12-1301-4576-855e-ae7da1669de8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'assignmentTagAddEditLink' and (text() = 'Add...' or . = 'Add...')]</value>
      <webElementGuid>2a987570-1844-4ae6-a470-37e7f0af28c9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
