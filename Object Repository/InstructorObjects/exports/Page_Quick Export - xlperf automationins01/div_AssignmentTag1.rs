<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AssignmentTag1</name>
   <tag></tag>
   <elementGuidId>91f30c29-f0c6-4fc5-8e2f-2d81beffd889</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@title = 'AssignmentTag1' and (text() = 'AssignmentTag1' or . = 'AssignmentTag1')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.coreq-tag-Box</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>29effc61-7024-4ce6-a143-fd0386796b18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>coreq-tag-Box</value>
      <webElementGuid>a2a9fe1e-20c3-4712-988b-dfef638c729a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>AssignmentTag1</value>
      <webElementGuid>a2e4bcfc-c0ca-4635-8442-0d93466bb2bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AssignmentTag1</value>
      <webElementGuid>58364d84-987a-4ce3-90c4-3100df6e4a29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;tagContainer&quot;)/div[@class=&quot;coreq-tag-Box&quot;]</value>
      <webElementGuid>4af962ca-2268-463e-820e-a65c6e45d0a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='tagContainer']/div</value>
      <webElementGuid>98ccb590-f078-4351-a28e-29cc99edf259</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assignments with tag(s):'])[1]/following::div[3]</value>
      <webElementGuid>f42c2845-2a9c-4f4b-b2d7-a56901987ef7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='( Assignment Tag(s) selected )'])[1]/following::div[6]</value>
      <webElementGuid>cbeda5b7-be8d-4995-afc5-c70fef0f5362</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AssignmentTag2'])[1]/preceding::div[1]</value>
      <webElementGuid>110a1732-bd0a-4c7c-95d9-079d47848a89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AssignmentTag3'])[1]/preceding::div[2]</value>
      <webElementGuid>4c6cc086-4101-4520-b11c-4269f7d42575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='AssignmentTag1']/parent::*</value>
      <webElementGuid>104a10ee-c491-44a4-9f6f-678619ab88b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div/div/div[2]/div/div</value>
      <webElementGuid>65d07ea5-037b-4f1b-95ab-963c06a32e83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'AssignmentTag1' and (text() = 'AssignmentTag1' or . = 'AssignmentTag1')]</value>
      <webElementGuid>9eaafda2-3828-45c3-9ce4-6ee4ddb0f0d4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
