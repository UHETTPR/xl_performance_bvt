<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f</name>
   <tag></tag>
   <elementGuidId>f619a063-ea21-4fe1-b074-39f72dc1b385</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_drpExportOptions</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpExportOptions']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>0e77e921-dc3f-44ca-915d-e2dd2356b492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctl00$InsideForm$MasterContent$drpExportOptions</value>
      <webElementGuid>a2365682-87a2-4e06-a21c-c4e8b309a384</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_drpExportOptions</value>
      <webElementGuid>ba2e5340-9b33-435f-b67c-05dc2a6e442c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control export-options-width</value>
      <webElementGuid>48db3ef5-dedd-48a9-b79f-36bff357f9c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>showOptions();</value>
      <webElementGuid>ba817a4f-308f-4a3d-9b8e-0a0a67fc55f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

</value>
      <webElementGuid>c3465000-f144-4674-979c-27e39104d707</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_drpExportOptions&quot;)</value>
      <webElementGuid>27b15c48-5b8b-4bd9-aaf7-9b00bf23c259</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ctl00_ctl00_InsideForm_MasterContent_drpExportOptions']</value>
      <webElementGuid>f086f6b6-05a7-4332-9c41-e7cefbe6c005</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/table/tbody/tr/td[2]/select</value>
      <webElementGuid>8a4c682f-825f-47ea-95a4-507ec6d317a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Set'])[1]/following::select[1]</value>
      <webElementGuid>48c5a40a-5dab-49f4-9de6-3ce7755aa87a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change delimiter...'])[1]/following::select[1]</value>
      <webElementGuid>3133870d-b9fd-4684-9306-2a71361f58cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Content Areas'])[1]/preceding::select[1]</value>
      <webElementGuid>409365f0-f24e-471b-af5c-8c4a3781e115</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Spreadsheet Layout'])[1]/preceding::select[2]</value>
      <webElementGuid>207516d0-f748-4146-8d76-31523362d90b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>846a6fff-5d44-4b27-95d0-e14768c6fba7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'ctl00$ctl00$InsideForm$MasterContent$drpExportOptions' and @id = 'ctl00_ctl00_InsideForm_MasterContent_drpExportOptions' and (text() = '
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

' or . = '
	--Choose--
	Homework
	Quiz
	Quiz Me
	Test
	Sample Tests
	Other
	Study Plan
	Overview of student averages
	Item Analysis

')]</value>
      <webElementGuid>a302dac9-961f-40ea-99f4-7c028a72312b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
