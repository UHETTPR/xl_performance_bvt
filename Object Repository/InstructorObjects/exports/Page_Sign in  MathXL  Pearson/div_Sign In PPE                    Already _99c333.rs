<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sign In PPE                    Already _99c333</name>
   <tag></tag>
   <elementGuidId>6c3e7341-2c9d-4cfd-8eec-994ffe054611</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.wrapper</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='®'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>52d15e28-e50f-4075-a9ba-dcb9b3489b7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wrapper</value>
      <webElementGuid>ee76e0c3-7edd-4c29-9744-45863ec730b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            

                
                    
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    

                    Sign In PPE
                    Already registered? Sign in with your Pearson account.
                    
                        Username
                        
                            
                        
                        Password
                        
                            
                        
                        
                            Sign In
                        
                        
                            Forgot your username or password?
                        
                        
                        New to MathXL? Visit our home page to register!
                    
                
            

        </value>
      <webElementGuid>159db91b-9083-4559-9281-a7b1be3fbfce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths cssfilters&quot;]/body[@class=&quot;green extra-light-grey-bg has-two-or-less-featured-link-categories&quot;]/section[@class=&quot;short-content extra-light-grey-bg&quot;]/div[@class=&quot;wrapper&quot;]</value>
      <webElementGuid>c0633235-5fd4-439c-b73b-29d3d3d1ee09</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='®'])[1]/following::div[1]</value>
      <webElementGuid>dd8afe70-2c71-4257-8f62-1883534213f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div</value>
      <webElementGuid>14b527ab-0aa6-4a69-9399-78a568685517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

            

                
                    
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    

                    Sign In PPE
                    Already registered? Sign in with your Pearson account.
                    
                        Username
                        
                            
                        
                        Password
                        
                            
                        
                        
                            Sign In
                        
                        
                            Forgot your username or password?
                        
                        
                        New to MathXL? Visit our home page to register!
                    
                
            

        ' or . = '

            

                
                    
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    

                    Sign In PPE
                    Already registered? Sign in with your Pearson account.
                    
                        Username
                        
                            
                        
                        Password
                        
                            
                        
                        
                            Sign In
                        
                        
                            Forgot your username or password?
                        
                        
                        New to MathXL? Visit our home page to register!
                    
                
            

        ')]</value>
      <webElementGuid>aaa0230c-b5d2-4dee-8f76-b774f90a649f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
