<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Performance by Chapter</name>
   <tag></tag>
   <elementGuidId>ca89cd2f-d9d1-4c78-b447-dc9e7b9333de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Performance by Chapter')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fa62c33d-eb83-4686-a7bc-1f9ea6e62c61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl21&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookPerformanceByChapter.aspx&quot;, false, true))</value>
      <webElementGuid>1145467c-b2bd-43f9-9ca9-0ce90fae62d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Performance by Chapter</value>
      <webElementGuid>c0a33e7f-cb58-4ada-86aa-59c6f49acc14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_TabControlGB_TabUl&quot;)/li[@id=&quot;li&quot;]/a[1]</value>
      <webElementGuid>d29f79c7-084a-4218-836a-ccea1abbdaa9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='li']/a)[3]</value>
      <webElementGuid>1adeca8c-9e17-4f34-b7da-f4184a2ad1dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Performance by Chapter')]</value>
      <webElementGuid>8858f59e-9d84-4792-bfa4-ede50404f6a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[2]/following::a[1]</value>
      <webElementGuid>fbf2bfac-fe75-461b-af27-d72e8a6c7f4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Student Overview'])[1]/following::a[2]</value>
      <webElementGuid>e0fd9a08-428a-4cf4-b003-2b427ca93938</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[1]/preceding::a[1]</value>
      <webElementGuid>839f8beb-b227-4ecc-8cce-757e24f04597</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[2]/preceding::a[1]</value>
      <webElementGuid>9347752d-6351-464d-82db-46776ba6248d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Performance by Chapter']/parent::*</value>
      <webElementGuid>ece1d863-113a-4674-a3c6-9c5b0fa4a612</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl21&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookPerformanceByChapter.aspx&quot;, false, true))')]</value>
      <webElementGuid>165d8331-3ce5-48b9-b0b8-1c953d21f472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[3]/div/div/ul/li[4]/a</value>
      <webElementGuid>8b54383b-1f1b-451b-9e6e-997a557885f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$TabControlGB$ctl21&quot;, &quot;&quot;, false, &quot;&quot;, &quot;/Instructor/GradebookPerformanceByChapter.aspx&quot;, false, true))' and (text() = 'Performance by Chapter' or . = 'Performance by Chapter')]</value>
      <webElementGuid>77da9ca6-8f20-48bb-8cb9-fd332743c09f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
