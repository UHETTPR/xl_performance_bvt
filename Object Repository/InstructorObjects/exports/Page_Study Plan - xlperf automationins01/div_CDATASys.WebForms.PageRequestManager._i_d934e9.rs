<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_CDATASys.WebForms.PageRequestManager._i_d934e9</name>
   <tag></tag>
   <elementGuidId>55e4c78a-7902-4d22-8fac-7bd4cf0f9fc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.xlbootstrap</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3cd442eb-0c27-478e-a790-634250c92aa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlbootstrap</value>
      <webElementGuid>95de8136-7e43-404d-b240-6e6930b53f24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$InsideForm$MasterContent$ScriptManager1', 'aspnetForm', [], [], [], 90, 'ctl00$ctl00');
//]]>


    


    function getHeaderText(){
        return $('#PageHeaderTitle').get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $('#PageHeaderTitle').get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener('mousedown', function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener('keydown', function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById('ButtonHelp').setAttribute('aria-expanded', 'false');
            }
        });

        }
    
 
        
        
      
    
    



    
    

    
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools 
    
    
        

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
    
    
        
            





    
	 All Chapters All ChaptersO. Orientation Questions for Students1. Review of Real Numbers2. Equations, Inequalities, and Problem Solving3. Graphing4. Solving Systems of Linear Equations5. Exponents and Polynomials6. Factoring Polynomials7. Rational Expressions8. More on Functions and Graphs9. Inequalities and Absolute Value10. Rational Exponents, Radicals, and Complex Numbers11. Quadratic Equations and Functions12. Exponential and Logarithmic Functions13. Conic Sections14. Sequences, Series, and the Binomial TheoremAppendix. AppendicesGA. Online Appendix: Additional Geometry QuestionsMma. Mathematica test questions 




    //client side change event
        function resultsFilterCommand(elem, command) {

            var continueFn = true;
            //use this function to call page level event
            if (typeof resultsFilterCommand_OnPage == 'function')
                continueFn = resultsFilterCommand_OnPage(elem, command);

            if (!continueFn) return;
            var downArrow = '&lt;span class=&quot;fa fa-angle-down fa-caret&quot;>&lt;/span>';
            var menuId = $(elem).data(&quot;menu-id&quot;);
            var menu = $('#' + menuId);

            menu.parent().find(&quot;li&quot;).removeClass(&quot;active&quot;);
            // title setting
            menu.html(&quot;&lt;span>&quot; + $(elem).text() + &quot;&lt;/span>&quot; + downArrow);

            // add selected state for the menu
            if ($(elem.parentNode))
                $(elem.parentNode).addClass(&quot;active&quot;);

        }
   

        
        
    
    

    
	
        
            View:
            Companion Study Plan(s)
            Entire Course
        
    


    
    
            
                
                     
                    Questions
                    
                        Objectives
                    
                        Time Spent
                
                
                    Class Roster

                    
                        Correct
                    
                        Worked
                    
                        Total

                    
                        Mastered
                    
                        Total
                    
                        Score

                    
                        Practice
                    
                        Quiz Me
                
        
            
                
                    automationstu01, xlperf
                
                    3
                
                    4
                
                    5304
                
                    0
                
                    737
                
                    0.00%
                5m 53s
                
            
        
            
        

    


    

    

                                                                </value>
      <webElementGuid>578d0c56-dca2-47b4-9d3a-961d3ce948dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]</value>
      <webElementGuid>3bcaaa6d-b2cb-41d3-989c-423df621b925</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]</value>
      <webElementGuid>dbf7e1ee-5cb3-4d6a-8771-71d1a31b7194</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL Load Testbook'])[1]/following::div[2]</value>
      <webElementGuid>8b85757c-2118-4b3b-a581-145ee66c5826</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='xlperf automationins01'])[3]/following::div[2]</value>
      <webElementGuid>9614574d-7e60-4afb-b7b3-934bcb372618</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div[4]</value>
      <webElementGuid>ca3f7857-66f1-471a-aded-e2dfa56afa0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
                                                                    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>


    


    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    



    
    

    
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools 
    
    
        

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
    
    
        
            





    
	 All Chapters All ChaptersO. Orientation Questions for Students1. Review of Real Numbers2. Equations, Inequalities, and Problem Solving3. Graphing4. Solving Systems of Linear Equations5. Exponents and Polynomials6. Factoring Polynomials7. Rational Expressions8. More on Functions and Graphs9. Inequalities and Absolute Value10. Rational Exponents, Radicals, and Complex Numbers11. Quadratic Equations and Functions12. Exponential and Logarithmic Functions13. Conic Sections14. Sequences, Series, and the Binomial TheoremAppendix. AppendicesGA. Online Appendix: Additional Geometry QuestionsMma. Mathematica test questions 




    //client side change event
        function resultsFilterCommand(elem, command) {

            var continueFn = true;
            //use this function to call page level event
            if (typeof resultsFilterCommand_OnPage == &quot; , &quot;'&quot; , &quot;function&quot; , &quot;'&quot; , &quot;)
                continueFn = resultsFilterCommand_OnPage(elem, command);

            if (!continueFn) return;
            var downArrow = &quot; , &quot;'&quot; , &quot;&lt;span class=&quot;fa fa-angle-down fa-caret&quot;>&lt;/span>&quot; , &quot;'&quot; , &quot;;
            var menuId = $(elem).data(&quot;menu-id&quot;);
            var menu = $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + menuId);

            menu.parent().find(&quot;li&quot;).removeClass(&quot;active&quot;);
            // title setting
            menu.html(&quot;&lt;span>&quot; + $(elem).text() + &quot;&lt;/span>&quot; + downArrow);

            // add selected state for the menu
            if ($(elem.parentNode))
                $(elem.parentNode).addClass(&quot;active&quot;);

        }
   

        
        
    
    

    
	
        
            View:
            Companion Study Plan(s)
            Entire Course
        
    


    
    
            
                
                     
                    Questions
                    
                        Objectives
                    
                        Time Spent
                
                
                    Class Roster

                    
                        Correct
                    
                        Worked
                    
                        Total

                    
                        Mastered
                    
                        Total
                    
                        Score

                    
                        Practice
                    
                        Quiz Me
                
        
            
                
                    automationstu01, xlperf
                
                    3
                
                    4
                
                    5304
                
                    0
                
                    737
                
                    0.00%
                5m 53s
                
            
        
            
        

    


    

    

                                                                &quot;) or . = concat(&quot;
                                                                    
    
//&lt;![CDATA[
Sys.WebForms.PageRequestManager._initialize(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$ScriptManager1&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;aspnetForm&quot; , &quot;'&quot; , &quot;, [], [], [], 90, &quot; , &quot;'&quot; , &quot;ctl00$ctl00&quot; , &quot;'&quot; , &quot;);
//]]>


    


    function getHeaderText(){
        return $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0).innerText;
    }
    function alterHeaderTitle(newTitle) {
        var pageTitle = $(&quot; , &quot;'&quot; , &quot;#PageHeaderTitle&quot; , &quot;'&quot; , &quot;).get(0);
        pageTitle.innerText = newTitle;
    }


       
           
        
            
        
                
           
            
            Gradebook  
        
        
        
              
        
         
         


    
        
        
            
            
            Print this page
        
        
            
                
                Help
            

            
                

                    Help with this page
                
                
                    
                        Learn about Gradebook
                    
                
                
                
                    
                        Icons &amp; Conventions
                    
                
            
        
    
    
    
    var HelpButton = document.getElementById(&quot;ButtonHelp&quot;);
    if (HelpButton) {
        HelpButton.addEventListener(&quot; , &quot;'&quot; , &quot;mousedown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.buttons != 0) {
                HelpButton.removeAttribute(&quot;href&quot;);

            }
        });

        document.body.addEventListener(&quot; , &quot;'&quot; , &quot;keydown&quot; , &quot;'&quot; , &quot;, function (event) {
            if (event.keyCode === 9) {
                HelpButton.href = &quot;javascript:void(0);&quot;  
            }

            if (event.keyCode === 27) {
                document.getElementById(&quot; , &quot;'&quot; , &quot;ButtonHelp&quot; , &quot;'&quot; , &quot;).setAttribute(&quot; , &quot;'&quot; , &quot;aria-expanded&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;false&quot; , &quot;'&quot; , &quot;);
            }
        });

        }
    
 
        
        
      
    
    



    
    

    
        Manage IncompletesChange WeightsOffline Items Export DataMore Tools 
    
    
        

    
        
        
        View: All AssignmentsAll AssignmentsHomeworkQuizzesTestsOtherStudent OverviewStudy PlanPerformance by Chapter
    
    




    
    
    
        
            





    
	 All Chapters All ChaptersO. Orientation Questions for Students1. Review of Real Numbers2. Equations, Inequalities, and Problem Solving3. Graphing4. Solving Systems of Linear Equations5. Exponents and Polynomials6. Factoring Polynomials7. Rational Expressions8. More on Functions and Graphs9. Inequalities and Absolute Value10. Rational Exponents, Radicals, and Complex Numbers11. Quadratic Equations and Functions12. Exponential and Logarithmic Functions13. Conic Sections14. Sequences, Series, and the Binomial TheoremAppendix. AppendicesGA. Online Appendix: Additional Geometry QuestionsMma. Mathematica test questions 




    //client side change event
        function resultsFilterCommand(elem, command) {

            var continueFn = true;
            //use this function to call page level event
            if (typeof resultsFilterCommand_OnPage == &quot; , &quot;'&quot; , &quot;function&quot; , &quot;'&quot; , &quot;)
                continueFn = resultsFilterCommand_OnPage(elem, command);

            if (!continueFn) return;
            var downArrow = &quot; , &quot;'&quot; , &quot;&lt;span class=&quot;fa fa-angle-down fa-caret&quot;>&lt;/span>&quot; , &quot;'&quot; , &quot;;
            var menuId = $(elem).data(&quot;menu-id&quot;);
            var menu = $(&quot; , &quot;'&quot; , &quot;#&quot; , &quot;'&quot; , &quot; + menuId);

            menu.parent().find(&quot;li&quot;).removeClass(&quot;active&quot;);
            // title setting
            menu.html(&quot;&lt;span>&quot; + $(elem).text() + &quot;&lt;/span>&quot; + downArrow);

            // add selected state for the menu
            if ($(elem.parentNode))
                $(elem.parentNode).addClass(&quot;active&quot;);

        }
   

        
        
    
    

    
	
        
            View:
            Companion Study Plan(s)
            Entire Course
        
    


    
    
            
                
                     
                    Questions
                    
                        Objectives
                    
                        Time Spent
                
                
                    Class Roster

                    
                        Correct
                    
                        Worked
                    
                        Total

                    
                        Mastered
                    
                        Total
                    
                        Score

                    
                        Practice
                    
                        Quiz Me
                
        
            
                
                    automationstu01, xlperf
                
                    3
                
                    4
                
                    5304
                
                    0
                
                    737
                
                    0.00%
                5m 53s
                
            
        
            
        

    


    

    

                                                                &quot;))]</value>
      <webElementGuid>2d358b59-181b-435d-9a3f-44ff2e8d7d79</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
