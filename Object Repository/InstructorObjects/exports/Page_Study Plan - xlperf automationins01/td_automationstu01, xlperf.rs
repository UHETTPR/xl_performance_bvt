<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu01, xlperf</name>
   <tag></tag>
   <elementGuidId>3b559c1a-4871-4fb7-bf25-3929184cb8f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                    automationstu01, xlperf' or . = '
                    automationstu01, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.rightdark</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>03d7768f-3b41-4814-b9e2-95a3a396ca44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rightdark</value>
      <webElementGuid>a701d1b2-8fc6-45de-9521-c0d49a266ebd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    automationstu01, xlperf</value>
      <webElementGuid>9b6e1b95-810f-4494-bafc-bc19d928b125</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/table[@class=&quot;grid xlbootstrap3&quot;]/tbody[1]/tr[@class=&quot;alternatingRow&quot;]/td[@class=&quot;rightdark&quot;]</value>
      <webElementGuid>c99fe0f1-d99f-43e3-a3fe-b0e325545c17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/table/tbody/tr[3]/td</value>
      <webElementGuid>4ec07e94-7ee3-4c66-9da9-0c21f9e6ed85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz Me'])[1]/following::td[1]</value>
      <webElementGuid>49917644-7257-4c61-aa2a-db7cc5255cfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Practice'])[1]/following::td[1]</value>
      <webElementGuid>432bc057-2ba1-45c1-822f-b44f110b6cbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::td[9]</value>
      <webElementGuid>698dedf5-c1e1-4f1a-b96d-e27f64ee89a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/table/tbody/tr[3]/td</value>
      <webElementGuid>41675a34-d1d0-4ea7-a6c4-42174773bf88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    automationstu01, xlperf' or . = '
                    automationstu01, xlperf')]</value>
      <webElementGuid>17086ede-7fc7-4b7c-8937-8b2d2f40500f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
