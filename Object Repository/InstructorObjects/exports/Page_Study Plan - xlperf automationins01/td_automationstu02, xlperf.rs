<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_automationstu02, xlperf</name>
   <tag></tag>
   <elementGuidId>fa1d5e1c-506f-4f9c-97d1-d77a9879c411</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                    automationstu02, xlperf' or . = '
                    automationstu02, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.rightdark</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>9bac2b3e-78b4-4a51-b70c-ae5a48f909d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rightdark</value>
      <webElementGuid>4876b656-fe5f-49ab-9290-3903766c7d00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    automationstu01, xlperf</value>
      <webElementGuid>431c18d2-6bd7-4ff5-80d6-142e002c25cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftnavtable&quot;)/tbody[1]/tr[2]/td[1]/div[@class=&quot;xlbootstrap&quot;]/table[@class=&quot;grid xlbootstrap3&quot;]/tbody[1]/tr[@class=&quot;alternatingRow&quot;]/td[@class=&quot;rightdark&quot;]</value>
      <webElementGuid>beb33068-a9c2-483e-8922-50a091ed5ae0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='leftnavtable']/tbody/tr[2]/td/div[4]/table/tbody/tr[3]/td</value>
      <webElementGuid>fa633715-8d07-498b-a6c2-44a0a819ab62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz Me'])[1]/following::td[1]</value>
      <webElementGuid>70796334-6d44-487d-aa70-21d790cf800a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Practice'])[1]/following::td[1]</value>
      <webElementGuid>d15770f2-347f-43ed-bccf-5cd4c75f8bfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::td[9]</value>
      <webElementGuid>43df8cf5-83d6-41ff-97c6-8265874f67f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/table/tbody/tr[3]/td</value>
      <webElementGuid>9f28c020-166d-4f3a-833f-efcf5926d7d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                    automationstu01, xlperf' or . = '
                    automationstu01, xlperf')]</value>
      <webElementGuid>a0cd37db-e479-442d-9bae-4ae031f18e65</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
