<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_automationstu01, xlperf</name>
   <tag></tag>
   <elementGuidId>2c62a91f-c4ee-4671-84bd-bcc872d6e17e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'automationstu01, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.roster-li > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fd0d06af-9b77-425c-a4ef-fb591a6c1982</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>ba944b49-9755-43c0-8755-b817bebfe328</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/Results.aspx?userId=29726355</value>
      <webElementGuid>6585ed5f-45b2-45dd-b316-3ef9f23ad8fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>6bae69be-d17d-4904-8ff0-50369f4d7c25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;classRoster&quot;)/tbody[1]/tr[@class=&quot;no-hover&quot;]/td[1]/div[@class=&quot;row classrosterbody&quot;]/ul[@class=&quot;list-group col-xs-4 roster-ul&quot;]/li[@class=&quot;roster-li&quot;]/a[1]</value>
      <webElementGuid>c12619f4-5f34-4ce9-9ea8-884154c5477b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='classRoster']/tbody/tr/td/div/ul/li/a</value>
      <webElementGuid>f6c8b805-2a84-4801-93b5-cb3e5da20334</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'automationstu01, xlperf')]</value>
      <webElementGuid>d6f6d52f-b56f-4b2a-9251-b7812270c2f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Class Roster'])[1]/following::a[1]</value>
      <webElementGuid>75cefe34-e3d8-4286-8996-4edbe1356194</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Additional Details'])[1]/following::a[1]</value>
      <webElementGuid>399a9535-2d58-4f3d-81aa-79db4891e278</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationins01, xlperf'])[1]/preceding::a[1]</value>
      <webElementGuid>829b62ae-4063-46eb-b90d-f43d9c31e903</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::a[2]</value>
      <webElementGuid>31f55ec8-6e75-4136-b4b7-7f6787307d28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>d906b141-7816-4ebc-898d-542fee051951</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Student/Results.aspx?userId=29726355')]</value>
      <webElementGuid>69bcf7fc-6342-4ae5-b2d6-37c711e1b74b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/ul/li/a</value>
      <webElementGuid>675decfc-63d0-42cd-bbf3-a4f13996c93b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/Results.aspx?userId=29726355' and (text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>c5421ab8-db59-48c5-8719-5ec1fe804210</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
