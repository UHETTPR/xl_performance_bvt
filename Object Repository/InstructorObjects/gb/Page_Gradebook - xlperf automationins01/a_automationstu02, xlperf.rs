<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_automationstu02, xlperf</name>
   <tag></tag>
   <elementGuidId>7cdf7b2a-c555-46b7-93ba-5af6ee0f5f51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'automationstu02, xlperf')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.roster-li > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ee841a34-d803-4ed6-a5ef-768ffa871002</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>1dd373b5-50e5-420b-bf5a-298a2078d493</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/Results.aspx?userId=29726355</value>
      <webElementGuid>962d925c-43d6-465c-9121-d8788bce373c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>automationstu01, xlperf</value>
      <webElementGuid>e8bc523d-69fa-4ca9-a733-b90435b3535e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;classRoster&quot;)/tbody[1]/tr[@class=&quot;no-hover&quot;]/td[1]/div[@class=&quot;row classrosterbody&quot;]/ul[@class=&quot;list-group col-xs-4 roster-ul&quot;]/li[@class=&quot;roster-li&quot;]/a[1]</value>
      <webElementGuid>58149cdc-7997-4bdb-9541-ef7a7071f5d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='classRoster']/tbody/tr/td/div/ul/li/a</value>
      <webElementGuid>9b6c1c99-1c21-4de6-b59a-8fddf17de1a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'automationstu01, xlperf')]</value>
      <webElementGuid>d6111b67-3f11-41b3-8c09-a9195dd4c6a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Class Roster'])[1]/following::a[1]</value>
      <webElementGuid>7249917f-3541-4b2f-ab61-96743fd385ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Additional Details'])[1]/following::a[1]</value>
      <webElementGuid>d386fbe6-d53b-46e0-9c84-8d759fe50cb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='automationins01, xlperf'])[1]/preceding::a[1]</value>
      <webElementGuid>ed9c34db-778d-44da-9132-5b90fbdaed47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::a[2]</value>
      <webElementGuid>5da20e06-e791-4ed3-9edc-89119df420d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>1ed9126a-1184-4f8e-b871-cfd7a15bc6aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Student/Results.aspx?userId=29726355')]</value>
      <webElementGuid>4404b2de-46b9-4516-ad74-11a67cc862de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/ul/li/a</value>
      <webElementGuid>14586240-6c02-4c21-9069-e9b4b127a164</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/Results.aspx?userId=29726355' and (text() = 'automationstu01, xlperf' or . = 'automationstu01, xlperf')]</value>
      <webElementGuid>6a5a3d53-2b2c-4d68-a828-31c74bd5b077</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
