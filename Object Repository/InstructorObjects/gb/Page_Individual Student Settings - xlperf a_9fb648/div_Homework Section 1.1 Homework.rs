<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Homework Section 1.1 Homework</name>
   <tag></tag>
   <elementGuidId>39ae8421-e3fc-42dd-b93c-7ff5f23e82b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Homework: Section 1.1 Homework' or . = 'Homework: Section 1.1 Homework')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#wizard > div > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ffcda32a-d337-4ac6-9c6b-a4fc4240ce8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework: Section 1.1 Homework</value>
      <webElementGuid>cd48ed26-76e5-4829-bed1-10cf3b14d6db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wizard&quot;)/div[1]/div[1]</value>
      <webElementGuid>10c9e842-82a8-410a-bfbc-25c4a3cbd878</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wizard']/div/div</value>
      <webElementGuid>5284850b-2043-403e-b92a-164e1de977bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Individual Student Settings'])[1]/following::div[4]</value>
      <webElementGuid>a0371848-6a73-40ea-a676-0e0e2b970c4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help with this page'])[1]/following::div[4]</value>
      <webElementGuid>3a7937ee-3af7-4a61-ae6e-4519526f46cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 1.1 Homework']/parent::*</value>
      <webElementGuid>1d54afee-0c7c-4523-88e2-f099d267e1b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div</value>
      <webElementGuid>65b2f1d9-4d5a-437c-8ba3-632c9cab2b90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Homework: Section 1.1 Homework' or . = 'Homework: Section 1.1 Homework')]</value>
      <webElementGuid>822016c9-fb59-46c4-92b1-43589c6ff76b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
