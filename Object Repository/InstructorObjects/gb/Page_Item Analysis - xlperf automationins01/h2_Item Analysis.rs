<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Item Analysis</name>
   <tag></tag>
   <elementGuidId>1754f285-f3e4-462d-bc9f-38fcfaf98737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Item Analysis' or . = '
            Item Analysis')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>046d29e0-0476-41de-804c-f076d77339ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>9ce3c638-5f06-4ff3-b827-440e4fd49b2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>3360d160-7df4-440a-a71b-ee44e4824301</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Item Analysis</value>
      <webElementGuid>a107ca3b-dd37-4722-8c5d-262f5539b45c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>fe594ad2-dcaf-4af0-95cb-6f0a84fd7ceb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='PageHeaderTitle']</value>
      <webElementGuid>8a253af3-8145-4429-8f79-01e934f5ff94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PageHeader1_HeaderTitleH2Div']/h2</value>
      <webElementGuid>673e7a60-c146-4a02-8b4b-86503409e776</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL Load Testbook'])[1]/following::h2[1]</value>
      <webElementGuid>8ba43ac6-9100-4957-9d3d-85a28d3d017d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='xlperf automationins01'])[2]/following::h2[1]</value>
      <webElementGuid>85c0212b-54ce-4d13-a204-2bba9c980174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h2[1]</value>
      <webElementGuid>33849b87-fd4c-43fa-9582-cf1ce10327b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h2[1]</value>
      <webElementGuid>18f9d706-dc3e-445e-98b2-be42dedf19ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Item Analysis']/parent::*</value>
      <webElementGuid>7e4422cf-44ec-4fea-9ea4-07cce5f0e273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>d7cb8d6b-7a3d-4e95-8638-88cf47ba2498</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Item Analysis' or . = '
            Item Analysis')]</value>
      <webElementGuid>9b820baa-b966-47ef-9000-67dfafe3a998</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
