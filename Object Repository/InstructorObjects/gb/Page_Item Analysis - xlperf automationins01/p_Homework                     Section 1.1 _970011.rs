<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Homework                     Section 1.1 _970011</name>
   <tag></tag>
   <elementGuidId>f551294f-a912-42be-a8fd-e203bdfebdeb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = '
		            Homework: 
                    Section 1.1 Homework
		        ' or . = '
		            Homework: 
                    Section 1.1 Homework
		        ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>2aff7034-e725-48e7-9f4a-aa0d75aa8f21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		            Homework: 
                    Section 1.1 Homework
		        </value>
      <webElementGuid>adb8fbf5-fa08-4ec4-80d3-e5def9e44a0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;xlbootstrap3&quot;]/nav[@class=&quot;subnavbar-item-analysis&quot;]/div[@class=&quot;row-container&quot;]/div[1]/p[1]</value>
      <webElementGuid>04cfc47b-5587-4833-9737-569822a399f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[4]/nav/div/div/p</value>
      <webElementGuid>cb2b18d2-b18d-4dc3-8acb-de0da57889a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous Assignment'])[1]/following::p[1]</value>
      <webElementGuid>3ec98bc0-fca8-4feb-8e54-da77d94e5785</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::p[1]</value>
      <webElementGuid>a0a869f8-b89f-4620-b79c-7c9ebddb4ef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Assignment'])[1]/preceding::p[3]</value>
      <webElementGuid>02daf4df-afa6-413b-a13a-a1c46fd01f6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>f912bdf8-cb07-42d7-a264-892893e9326c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
		            Homework: 
                    Section 1.1 Homework
		        ' or . = '
		            Homework: 
                    Section 1.1 Homework
		        ')]</value>
      <webElementGuid>b2d89dcc-44db-402e-8ac0-e80853f54792</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
