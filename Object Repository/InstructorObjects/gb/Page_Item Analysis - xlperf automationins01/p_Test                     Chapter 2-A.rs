<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Test                     Chapter 2-A</name>
   <tag></tag>
   <elementGuidId>a7ed9891-237c-47cc-890d-2f2c459337b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = '
		            Test: 
                    Chapter 2-A
		        ' or . = '
		            Test: 
                    Chapter 2-A
		        ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>7c41736d-187a-4dbf-87e1-1a505fa223e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		            Test: 
                    Chapter 2-A
		        </value>
      <webElementGuid>e5172eb5-5a88-4cf6-bb46-d343a4acae61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;xlbootstrap3&quot;]/nav[@class=&quot;subnavbar-item-analysis&quot;]/div[@class=&quot;row-container&quot;]/div[1]/p[1]</value>
      <webElementGuid>320030cf-1233-4d46-9a98-f25fa963ca97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[4]/nav/div/div/p</value>
      <webElementGuid>119f5a19-06e3-4cf0-a241-11d1066a6d7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous Assignment'])[1]/following::p[1]</value>
      <webElementGuid>e3e15656-a101-42da-b52e-6979f5920360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::p[1]</value>
      <webElementGuid>c1295d52-dfb9-4c9c-be6b-d9a1e874f716</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Assignment'])[1]/preceding::p[4]</value>
      <webElementGuid>dd6d1593-8150-484b-8729-4b276e1cec2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Best Score'])[1]/preceding::p[4]</value>
      <webElementGuid>155da58c-6230-4daa-a311-d3ca46aa3d08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>929fc8a6-a180-4744-9c56-38da2c6a5a09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
		            Test: 
                    Chapter 2-A
		        ' or . = '
		            Test: 
                    Chapter 2-A
		        ')]</value>
      <webElementGuid>f3411e31-d22f-4daf-9e42-a13cc713324a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
