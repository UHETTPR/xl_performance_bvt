<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Change Score</name>
   <tag></tag>
   <elementGuidId>c55849ae-7791-4876-8091-305178d02bc7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Change Score')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>589a5e6a-089c-4c57-962f-efbea82e5659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0);</value>
      <webElementGuid>5390d64d-e25a-416e-82f8-4f32f1c39201</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-value</name>
      <type>Main</type>
      <value>Change Score</value>
      <webElementGuid>f8559b04-5536-44d3-b1da-40e6277fed09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>da^T^412737835^77066894^29726355^0^3^^Chapter 2-A</value>
      <webElementGuid>f8c92351-55f2-43ea-a4c9-04431c600b2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>javascript: DoSubmitAction(this);</value>
      <webElementGuid>99d9dcc4-5fa2-41d4-becd-7075461446c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>559e5da4-52cc-4d6e-8f0e-bcedb6679251</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Change Score</value>
      <webElementGuid>76fc9b25-68d4-4090-a164-b9255bee5c2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row77066894&quot;)/td[@class=&quot;center rightnone action-dropdown-foundations last-field&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller open&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[4]/a[1]</value>
      <webElementGuid>1280838a-80c9-4c27-b836-5b1e5a8dccca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@onclick='javascript: DoSubmitAction(this);'])[4]</value>
      <webElementGuid>55a068ee-033d-4096-9840-d3ff78998d0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row77066894']/td[11]/div/div/ul/li[4]/a</value>
      <webElementGuid>bf881c75-f021-47f9-8f5d-cbd4ca39f4d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Change Score')]</value>
      <webElementGuid>4c1a458b-e704-49b1-8cdc-10b039e78244</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Analysis'])[1]/following::a[1]</value>
      <webElementGuid>0d5d07e7-43ef-46d4-bfad-ba0b03aba60a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Review'])[1]/following::a[2]</value>
      <webElementGuid>565f3825-c583-483e-8915-fbfec149ae4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print'])[1]/preceding::a[1]</value>
      <webElementGuid>799dd8e5-60ae-4918-a821-b9d632ad6c4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings Per Student'])[1]/preceding::a[2]</value>
      <webElementGuid>ce9f6cc6-c67a-4c81-b8a8-ddf5766cc840</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Change Score']/parent::*</value>
      <webElementGuid>8aa03883-b120-42f1-abcc-87a5292ee40d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0);')])[8]</value>
      <webElementGuid>e904cc6a-be7b-41c8-a436-6de9fe8566ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[11]/div/div/ul/li[4]/a</value>
      <webElementGuid>2e4dfb14-12f2-4d4a-87fa-d10bcda8a241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0);' and (text() = 'Change Score' or . = 'Change Score')]</value>
      <webElementGuid>02b6bc4e-417e-4c58-b4e0-e3a0080a1813</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
