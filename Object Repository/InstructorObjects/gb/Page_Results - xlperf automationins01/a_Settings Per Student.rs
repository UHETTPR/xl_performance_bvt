<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Settings Per Student</name>
   <tag></tag>
   <elementGuidId>c74d504c-a2ea-4081-96d3-2dfb3b82fd44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Settings Per Student')])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>224a96d0-cb1f-4b16-ac2c-6481216fdd81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:void(0);</value>
      <webElementGuid>7c5f7c12-1538-439f-927f-0a2398eb940d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-value</name>
      <type>Main</type>
      <value>Settings per Student</value>
      <webElementGuid>0634dd41-bc25-48e9-8d06-0df058421290</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-id</name>
      <type>Main</type>
      <value>da^H^683634418^205399931^29726355^0^3^^Section 1.1 Homework</value>
      <webElementGuid>3e1a25ce-17bc-44d0-98cf-2251e20affd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>javascript: DoSubmitAction(this);</value>
      <webElementGuid>1646910a-2d7e-4b32-9478-dceb0ee4b87c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
      <webElementGuid>cd1e14bd-8bae-44df-b8f9-f816f902bd00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Settings Per Student</value>
      <webElementGuid>56a2c83b-f92a-4c40-8a5b-a63e2f0dd5b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row205399931&quot;)/td[@class=&quot;center rightnone action-dropdown-foundations last-field&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller open&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[6]/a[1]</value>
      <webElementGuid>f4fb8353-8bdb-49fc-b78c-ba550f661bf0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@onclick='javascript: DoSubmitAction(this);'])[14]</value>
      <webElementGuid>d8234d25-23b4-4ece-9715-8dae36acd0ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row205399931']/td[11]/div/div/ul/li[6]/a</value>
      <webElementGuid>f081a746-34db-414d-a568-4bb6af9b16d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Settings Per Student')])[2]</value>
      <webElementGuid>ed7b444f-8a7b-4027-a2d7-204c06bd21e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print'])[2]/following::a[1]</value>
      <webElementGuid>bbda8da0-1d2d-4c14-b282-9e1567cac0d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Score'])[2]/following::a[2]</value>
      <webElementGuid>d88472e5-4c43-4d75-b269-27de455f3f95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Omit'])[2]/preceding::a[1]</value>
      <webElementGuid>58ba1621-9a7a-4fb3-941f-551c9a470916</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[2]/preceding::a[2]</value>
      <webElementGuid>d212bea2-8a8c-4318-89ec-f53dc9c6636c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, 'javascript:void(0);')])[18]</value>
      <webElementGuid>67b2a89c-fd19-4227-81e1-d1c3bb0bd883</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[11]/div/div/ul/li[6]/a</value>
      <webElementGuid>5f4f840a-1d86-4186-976e-b1d8c861661c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:void(0);' and (text() = 'Settings Per Student' or . = 'Settings Per Student')]</value>
      <webElementGuid>4ae3d0bd-9afe-4b5c-a355-da714a87ab09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
