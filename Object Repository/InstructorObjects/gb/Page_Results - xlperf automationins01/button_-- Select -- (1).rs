<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_-- Select -- (1)</name>
   <tag></tag>
   <elementGuidId>5a853ac6-959f-4f55-b24a-6706573ee3c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Section 1.1 Homework'])[2]/following::button[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ActionList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0a590a97-213f-42e2-90cf-94bbec3bcff6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ActionList</value>
      <webElementGuid>ed350395-4860-4ae4-8c2c-99aa95926c35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a51cce47-4f2d-4a32-8805-815a09f21ec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default dropdown-toggle</value>
      <webElementGuid>5dfe5b36-00b6-473b-b563-bf3672916f57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>619c5a4e-9fc1-4e35-9302-3c2094e64b4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>cd73e8a0-459c-4454-b9e7-2b4306f7f910</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5d9c002c-ce07-4e82-988b-977cad7d939a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	        -- Select --
		    
	    </value>
      <webElementGuid>a340221f-979b-4f5b-8537-f0f6287db943</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row77066894&quot;)/td[@class=&quot;center rightnone action-dropdown-foundations last-field&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller&quot;]/button[@id=&quot;ActionList&quot;]</value>
      <webElementGuid>bb6d3859-0035-47a4-a5bd-de83c7d79344</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='ActionList']</value>
      <webElementGuid>ce0c87b8-6a6e-4546-b2a3-086ca0e21204</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row77066894']/td[11]/div/div/button</value>
      <webElementGuid>87e7ba0d-a1a4-4be5-ba21-3be8da450fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter 2-A'])[2]/following::button[1]</value>
      <webElementGuid>67446d05-3edc-4b04-8b5c-36cc4333e3ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Student'])[1]/preceding::button[1]</value>
      <webElementGuid>167a4e9d-b5e7-4ca0-881e-05873ec2c74f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[11]/div/div/button</value>
      <webElementGuid>353bd0f0-90a2-49f7-b7bd-37d6cb5aa777</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'ActionList' and @type = 'button' and (text() = '
	        -- Select --
		    
	    ' or . = '
	        -- Select --
		    
	    ')]</value>
      <webElementGuid>e64434d3-f472-45f0-962c-c454c62438a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
