<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_-- Select --</name>
   <tag></tag>
   <elementGuidId>fed9ecfd-c91b-43a9-86c9-727358d7624d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter 2-A'])[2]/following::button[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ActionList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>27838a3a-ffa6-468b-a922-e1c6235a8f0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ActionList</value>
      <webElementGuid>d3197427-234d-433e-8906-56d2f16b2192</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9aaa5db9-8f5e-4d8c-9bf4-3f436543f595</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default dropdown-toggle</value>
      <webElementGuid>b5909e3d-be7d-4d49-8b3b-2ea34be1b6bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>bc7bef3e-34c3-484d-bb67-1cb5c306cb14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>c3e5ac6b-c15e-4d38-a1b9-6df6c18bc41e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>bef91be1-5eb1-474a-9315-3346775c3dd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	        -- Select --
		    
	    </value>
      <webElementGuid>ba243b63-5bec-4da1-9e3f-e39a370e7024</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row77066894&quot;)/td[@class=&quot;center rightnone action-dropdown-foundations last-field&quot;]/div[@class=&quot;xlbootstrap3&quot;]/div[@class=&quot;btn-group dropdown-behavior action-dropdown-controller&quot;]/button[@id=&quot;ActionList&quot;]</value>
      <webElementGuid>d0a41f39-209f-440d-9559-6ac3eedeaf5f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='ActionList']</value>
      <webElementGuid>6e5d0a08-37fa-4dc0-89b1-ebb0784e037b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row77066894']/td[11]/div/div/button</value>
      <webElementGuid>cf6e4391-7ab2-4b6d-b0a8-c0600a603622</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Chapter 2-A'])[2]/following::button[1]</value>
      <webElementGuid>f16f20f5-8d5e-4aa6-b759-e14d234624ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Student'])[1]/preceding::button[1]</value>
      <webElementGuid>8968c957-af47-4a7d-89de-29d5dc95960d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[11]/div/div/button</value>
      <webElementGuid>e9993c97-a1e8-48f1-9ff3-b5a4305e923e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'ActionList' and @type = 'button' and (text() = '
	        -- Select --
		    
	    ' or . = '
	        -- Select --
		    
	    ')]</value>
      <webElementGuid>9d1ce4cc-0c68-4ad4-be7e-a30990b809eb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
