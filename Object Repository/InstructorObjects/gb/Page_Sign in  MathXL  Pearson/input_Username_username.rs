<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username_username</name>
   <tag></tag>
   <elementGuidId>3bb528dd-452e-4447-8823-e79e1e825530</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#userName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='userName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6a913c66-e495-43c4-b226-37d76700515f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>245447b3-bc63-409f-83d7-10e1f96a52e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>7584a293-e63f-469e-931c-354481b3fad3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userName</value>
      <webElementGuid>e80050b5-3e25-4cf0-9a18-20512af3b69e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>medium-width required-field</value>
      <webElementGuid>d79a71e3-d419-4521-9772-578e971c30a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;userName&quot;)</value>
      <webElementGuid>813d3212-03d1-4b77-a658-7870749a75a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='userName']</value>
      <webElementGuid>6bb13678-c4e8-425a-8a21-25086691b42e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='inputfrm']/div/input</value>
      <webElementGuid>1121e9fb-e39b-48d8-b2aa-ce01e1e125ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>0e969388-8d49-4682-ae80-145a71ac5c26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'username' and @id = 'userName']</value>
      <webElementGuid>ef99f8f5-cc01-4f45-82d1-76c0dc0cc4cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
