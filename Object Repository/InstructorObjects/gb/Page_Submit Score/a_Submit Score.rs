<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Submit Score</name>
   <tag></tag>
   <elementGuidId>fba12de6-ff68-4ca7-9322-681907c81eac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Submit Score')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ImgBtnUpdate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>805ceee8-00ac-444b-9d6d-6a5334138d96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))</value>
      <webElementGuid>1a52b9d8-d84f-4548-8f21-bb516a949d74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ImgBtnUpdate</value>
      <webElementGuid>4efd3892-819e-4248-9ecf-eb1e75bc5c1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>if(!SubCheckSubmit(this)) return false;</value>
      <webElementGuid>63dd1012-b7bf-4765-9d6e-b2d65a4a7819</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary </value>
      <webElementGuid>253ea0ef-9f73-4104-b17e-38af8fe9e7a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Score
</value>
      <webElementGuid>b89d873b-9d95-40c5-b732-1874b109f228</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ImgBtnUpdate&quot;)</value>
      <webElementGuid>5503f8c2-8302-4f70-b36e-bf8f3b2298d0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='ImgBtnUpdate']</value>
      <webElementGuid>a2b2a2ad-4da4-457a-be97-a88c801c4f45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wizard-buttons']/div[2]/a</value>
      <webElementGuid>0ad5619f-63eb-4bf2-a484-a2406fdeb1a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Submit Score')]</value>
      <webElementGuid>27ee6f18-86a3-4fd9-abe0-463f2af6502e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::a[1]</value>
      <webElementGuid>c44d6835-6722-4822-b258-ff24b4dfa75f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You must enter a value.'])[2]/following::a[2]</value>
      <webElementGuid>8e076e13-2bfd-49ba-b7c9-a913bb2ee8d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='[+]'])[1]/preceding::a[1]</value>
      <webElementGuid>c64411de-06f0-4043-96f6-b3ebb2f4f80a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'ImgBtnUpdate', '&quot;', ')')])[1]/preceding::a[1]</value>
      <webElementGuid>e30cdffe-ca55-46c0-a3b9-b62524da894f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))')]</value>
      <webElementGuid>4162c448-616b-4810-8334-3c6ea7b7c8fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a</value>
      <webElementGuid>b00c381c-3382-4dc6-9a58-bb45151fce54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$InsideForm$MasterContent$ImgBtnUpdate&quot;, &quot;&quot;, true, &quot;valgroup&quot;, &quot;&quot;, false, true))' and @id = 'ImgBtnUpdate' and (text() = 'Submit Score
' or . = 'Submit Score
')]</value>
      <webElementGuid>be923850-ed64-4bfa-9922-bdb897266893</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
