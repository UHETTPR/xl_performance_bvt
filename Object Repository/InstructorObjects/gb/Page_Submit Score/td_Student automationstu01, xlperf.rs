<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Student automationstu01, xlperf</name>
   <tag></tag>
   <elementGuidId>351d6a08-2be4-4f84-bd1a-07d1e5ff0acf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                   Student: automationstu01, xlperf
                ' or . = '
                   Student: automationstu01, xlperf
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.pad.padbottom-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c7272354-2c75-4e8a-a39b-c07890d5c32c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>bac96bae-2eee-407c-bad6-58461ac4fa40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pad padbottom-sm</value>
      <webElementGuid>9d31276c-20e3-48aa-9369-4b8d24050600</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                   Student: automationstu01, xlperf
                </value>
      <webElementGuid>525c1cbe-ac58-48c4-9323-00ff7dc599fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;change-score-popup-table&quot;)/tbody[1]/tr[2]/td[@class=&quot;pad padbottom-sm&quot;]</value>
      <webElementGuid>126b3233-9364-4cfe-875c-f6c13c05c6ef</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='change-score-popup-table']/tbody/tr[2]/td</value>
      <webElementGuid>38fff5aa-944e-4400-ace6-8a897d891b98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/following::td[1]</value>
      <webElementGuid>b4c44bc9-b23b-4883-9b97-39a360cc2220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points Correct:'])[1]/preceding::td[1]</value>
      <webElementGuid>e717305f-9670-4098-8faf-21dc21b55c0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>611bb5f8-0bb8-45d1-a16b-cb115ef1a9e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/table/tbody/tr[2]/td</value>
      <webElementGuid>2c7c8d86-6bc3-4d2f-a577-940ff55edfb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                   Student: automationstu01, xlperf
                ' or . = '
                   Student: automationstu01, xlperf
                ')]</value>
      <webElementGuid>5f558320-e122-4cf9-a602-9dd9cafaee57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
