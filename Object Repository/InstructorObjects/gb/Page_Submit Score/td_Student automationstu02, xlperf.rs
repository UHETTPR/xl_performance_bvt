<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Student automationstu02, xlperf</name>
   <tag></tag>
   <elementGuidId>4979785b-6960-4577-8849-52cdf2739179</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                   Student: automationstu02, xlperf
                ' or . = '
                   Student: automationstu02, xlperf
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.pad.padbottom-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>a5acc6fc-a20b-4c1b-b609-a3e8be66e32b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>d93eeb23-9551-4a4c-85ce-f48dfce69c27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pad padbottom-sm</value>
      <webElementGuid>bb8944be-4e2e-49d6-a9d5-74ffbdb641ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                   Student: automationstu01, xlperf
                </value>
      <webElementGuid>6772aaa0-f6d8-4472-9728-b6fb493b2f5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;change-score-popup-table&quot;)/tbody[1]/tr[2]/td[@class=&quot;pad padbottom-sm&quot;]</value>
      <webElementGuid>5c02de62-2684-41a0-9ad9-8e63ab1802f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='change-score-popup-table']/tbody/tr[2]/td</value>
      <webElementGuid>783468ba-8fc7-47d1-a7c0-2d8b7c96d882</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/following::td[1]</value>
      <webElementGuid>defe503a-dddc-470b-8653-79690d637147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points Correct:'])[1]/preceding::td[1]</value>
      <webElementGuid>801d1c6d-fc6d-43b7-a619-e283abf5edc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='automationstu01, xlperf']/parent::*</value>
      <webElementGuid>441ac26e-d986-4812-b45c-2f71b56519f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/table/tbody/tr[2]/td</value>
      <webElementGuid>59a34e32-eb5a-4d91-a055-bde44c907019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                   Student: automationstu01, xlperf
                ' or . = '
                   Student: automationstu01, xlperf
                ')]</value>
      <webElementGuid>a026e2a6-a4d3-4e21-957f-8354ae2da3e0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
