<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Test                     Chapter 2-A</name>
   <tag></tag>
   <elementGuidId>231e38bd-b945-4f88-a231-d41634ed7568</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[(text() = '
                   Test: 
                    

Chapter 2-A



                ' or . = '
                   Test: 
                    

Chapter 2-A



                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.pad.padtop-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>a7b2d739-fb34-4176-b580-cd43c104978b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>e8bde2a8-ed16-445b-83e3-776bbb5d1f24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pad padtop-sm</value>
      <webElementGuid>6beab11b-9f77-4e7c-ba5d-86872a3270f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                   Test: 
                    

Chapter 2-A



                </value>
      <webElementGuid>683ff0fa-2e75-4600-9508-91b5e3aea451</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;change-score-popup-table&quot;)/tbody[1]/tr[@class=&quot;horizontal-rule&quot;]/td[@class=&quot;pad padtop-sm&quot;]</value>
      <webElementGuid>65943606-adf8-44da-833d-882dc5a822df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='change-score-popup-table']/tbody/tr/td</value>
      <webElementGuid>ad8aaab8-ddd9-48f5-a1bc-db379663254a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Submit Score'])[1]/following::td[1]</value>
      <webElementGuid>4f719ac0-7f99-4e5b-9efc-3cef01c73a3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help with this page'])[1]/following::td[1]</value>
      <webElementGuid>52ea9b49-b657-40e6-a3f8-0608ed30d3ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chapter 2-A']/parent::*</value>
      <webElementGuid>c0749127-eb8e-4550-8584-122f14f09bd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/table/tbody/tr/td</value>
      <webElementGuid>2333fd9a-2791-4f4a-b25d-869899e561f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                   Test: 
                    

Chapter 2-A



                ' or . = '
                   Test: 
                    

Chapter 2-A



                ')]</value>
      <webElementGuid>033326b5-4c9e-4a8b-ae55-5557b00074e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
