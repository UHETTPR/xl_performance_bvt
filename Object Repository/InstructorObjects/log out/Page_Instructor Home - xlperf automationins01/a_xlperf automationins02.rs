<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_xlperf automationins02</name>
   <tag></tag>
   <elementGuidId>73880f1b-89ee-49f1-a9d4-37d6758e09e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'xlperf automationins02')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.dropdown-toggle.my_account_name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6afef0ab-f8c1-4846-983e-23d88b34cb6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>716f307d-7728-4135-893a-8749362b3412</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>99eddf90-5479-463a-96b6-1412e819e29c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-toggle my_account_name</value>
      <webElementGuid>f669de95-b32d-43b4-9d74-37a9562855f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>9f6626f5-74b1-4aba-8b19-62024513c07a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>918dca71-0f49-4ba5-98a5-31745e2b2a35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>xlperf automationins01</value>
      <webElementGuid>97cd1d8e-e68a-41f2-8b46-155b7155da8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;NavigationLinkKey_49&quot;)/a[@class=&quot;dropdown-toggle my_account_name&quot;]</value>
      <webElementGuid>2a1acf3a-0dc5-40ff-bead-6c3520d079e0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='NavigationLinkKey_49']/a</value>
      <webElementGuid>008b0344-3ee8-4690-9104-f9227014b781</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'xlperf automationins01')]</value>
      <webElementGuid>cfa3c185-f0a2-48ba-bec2-aa4a7f669a14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::a[1]</value>
      <webElementGuid>98410266-c960-47c3-8235-ff44898f0a0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PerfBVT_XL_AutomationCourse'])[1]/following::a[2]</value>
      <webElementGuid>3b6f899a-f460-4f05-a04f-b2cf5e0ed6eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Courses'])[1]/preceding::a[1]</value>
      <webElementGuid>6e353743-2aa7-450a-a3a4-cef6a5c148b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enroll in Course'])[1]/preceding::a[2]</value>
      <webElementGuid>b59e3f50-4596-4315-b43e-a023bf9958f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='xlperf automationins01']/parent::*</value>
      <webElementGuid>dc48d23d-de81-4f94-aba8-25ea39f23c57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[2]</value>
      <webElementGuid>7ef314ab-f3ba-4f26-bad0-614873c3334f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/a</value>
      <webElementGuid>2135beae-ca90-4223-b3e2-aa059c827d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'xlperf automationins01' or . = 'xlperf automationins01')]</value>
      <webElementGuid>d12f94e9-9843-40af-9d03-df89c4c42212</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
