<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Next_1_2_3</name>
   <tag></tag>
   <elementGuidId>6b9be164-e017-4602-b058-738b34ea6eb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id,'xl_dijit-bootstrap_Button_') and @type = 'button' and @title = 'Next' and (text() = 'Next' or . = 'Next')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_53</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2a0e8953-b414-4543-b297-9cf5da64d677</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>084af5aa-1432-408c-bf4a-6a26cab8abf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default navButton btn-primary</value>
      <webElementGuid>36ddd9bf-abb6-4ae0-9c22-f24567040dc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>1b23308f-e594-432a-9576-ac78707e1e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_53</value>
      <webElementGuid>62c67760-fce2-49ec-a6c6-00d0bd26dad9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_53</value>
      <webElementGuid>a023ada6-f03b-44d9-9d43-95cb65523b42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>cd9a35a1-8088-482f-8c8a-06dbf9d2ac82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>4c9e8885-20c5-4715-aa51-11dabc0baf54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_53&quot;)</value>
      <webElementGuid>b0fe497c-1eb3-4d29-a8be-328e164ce9ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/AssignmentTaking/Page_Chapter 8-A/iframe_Return to Homework_activityFrame (1)</value>
      <webElementGuid>a3cf6aa5-470d-4fa7-b40e-81294dff4dd1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_53']</value>
      <webElementGuid>6c3ceab1-c8f0-4d67-874a-6da8fd0bf20d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_4']/div[2]/div[2]/div[2]/button[2]</value>
      <webElementGuid>e0c10ee9-6a2c-4629-ad7f-64cc89664209</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skill builder'])[1]/following::button[2]</value>
      <webElementGuid>cdbf572e-d836-4487-bdd3-2ffb6daaecfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/following::button[3]</value>
      <webElementGuid>2cdd45fb-3d9b-4dd5-9819-ae7e51209931</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'xl_dijit-bootstrap_Button_53', '&quot;', ')')])[1]/preceding::button[1]</value>
      <webElementGuid>2f0222d9-ba35-4a25-bc63-fe7f810fa3c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please wait...'])[1]/preceding::button[1]</value>
      <webElementGuid>9a034935-7886-4cf5-a4a4-2263ba879035</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>4486c850-5c7a-420c-86e0-eb13bf357e95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/button[2]</value>
      <webElementGuid>8abaa73e-79e8-498d-bcd1-40ccf94aa150</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_53' and @title = 'Next' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>f8f6f9d8-d3fd-45c1-875d-6c66b13d16a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
