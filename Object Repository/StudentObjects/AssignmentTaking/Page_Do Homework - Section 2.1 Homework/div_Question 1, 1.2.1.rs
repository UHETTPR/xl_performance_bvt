<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 1, 1.2.1</name>
   <tag></tag>
   <elementGuidId>fe76e0c8-bfc5-46c3-b8b4-4e1c3cb2959a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                        
                        
                            
                                Question 1, 1.2.1
                            
                            
                        
                        
                    ' or . = '
                        
                        
                            
                                Question 1, 1.2.1
                            
                            
                        
                        
                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6e8d6df0-c445-4e1c-bff4-5abc69a85ba0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>ec4ecbd7-8eb1-46c7-b97d-3f620a8c7a8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>2c5ef221-3f68-41fb-ab3f-4e5c5c358455</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        
                            
                                Question 1, 1.2.1
                            
                            
                        
                        
                    </value>
      <webElementGuid>d92179a5-fe6f-4363-9006-e39c583775c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_0&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>09fdac94-4544-4267-9f7e-ade1f4564982</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/AssignmentTaking/Page_Do Homework - Section 2.1 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>65ef0f77-5254-418f-b0a2-67543787c899</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_0']/div/div[2]/div</value>
      <webElementGuid>baafadda-9fff-4634-bbd7-6d1c5c2497b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework:'])[1]/following::div[4]</value>
      <webElementGuid>01bb5f50-7840-42de-956a-8c064a50a0a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>601f5f13-290a-4932-8770-24a8f2b19a8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div</value>
      <webElementGuid>54f5834e-c350-4e3c-870c-d106fc737ecb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        
                            
                                Question 1, 1.2.1
                            
                            
                        
                        
                    ' or . = '
                        
                        
                            
                                Question 1, 1.2.1
                            
                            
                        
                        
                    ')]</value>
      <webElementGuid>e7be0877-920a-4cf6-adc2-8f3e2481ba7d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
