<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 1, 2.1.1</name>
   <tag></tag>
   <elementGuidId>76314856-ea68-411e-8958-46e7cc0a6b08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                        
                        
                            
                                Question 1, 2.1.1
                            
                            
                        
                        
                    ' or . = '
                        
                        
                            
                                Question 1, 2.1.1
                            
                            
                        
                        
                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1f6c10e9-795d-4739-a261-da3ce3546c52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>e93a2972-6c69-499c-8736-863d85630aa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>f5b58787-0ef6-4edf-80a2-a2dcd39e5ac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        
                            
                                Question 1, 2.1.1
                            
                            
                        
                        
                    </value>
      <webElementGuid>1e1116aa-8fe4-45b4-9afb-715d8b3e3360</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_0&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>7c00c956-9196-4e56-b530-e651eae19b59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/AssignmentTaking/Page_Do Homework - Section 6.1 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>d7fb321e-6242-4ba5-a5b5-16fe0c532c05</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_0']/div/div[2]/div</value>
      <webElementGuid>4d110000-5132-489c-9eab-bc437ae5124c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework:'])[1]/following::div[4]</value>
      <webElementGuid>302bc231-3887-4064-b6d1-3c19547e5260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>31ed745b-600b-4032-a165-4b4622df0c7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div</value>
      <webElementGuid>e090ca6c-2e3c-4af2-8dcb-dd9a5b471322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        
                            
                                Question 1, 2.1.1
                            
                            
                        
                        
                    ' or . = '
                        
                        
                            
                                Question 1, 2.1.1
                            
                            
                        
                        
                    ')]</value>
      <webElementGuid>f10ba392-3fff-4ca3-be93-2771feaef72a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
