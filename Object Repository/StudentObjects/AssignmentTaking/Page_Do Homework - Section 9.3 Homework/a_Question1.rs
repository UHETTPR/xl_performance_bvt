<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Question1</name>
   <tag></tag>
   <elementGuidId>04c7d6fd-6248-4512-9d49-8dfa6a75c7ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Question 1')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8089d5ad-ca1c-4e6e-b3ec-a9e635b2b160</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>2b7bd88d-59d9-4811-9190-222266ec5424</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>h1 img1</value>
      <webElementGuid>1cd0d008-fac1-4a73-8029-f117baa14ff6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doExercise(1);</value>
      <webElementGuid>bd3a9bb1-8cdb-4aa8-b4b5-1164cb96a0b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>(3.1.1)</value>
      <webElementGuid>38c8dde9-808c-4ce8-8705-3dcef4e9e161</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Question 1</value>
      <webElementGuid>17e9ce33-15a0-46a4-aac3-56e8d47e8e1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;h1&quot;)</value>
      <webElementGuid>49c0b8a0-e2d7-4cc8-823c-d00a4ecc11a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='h1']</value>
      <webElementGuid>7e1271ec-692f-441e-ad62-b11c4be677a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='exPanel']/section[2]/ul/li/span[2]/a</value>
      <webElementGuid>35726f2b-1f80-4e8f-8438-e83529c8ccef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Question 1')]</value>
      <webElementGuid>c65cdccb-bebe-4d11-bb6e-771b1a67afe2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Incorrect: 0'])[1]/following::a[1]</value>
      <webElementGuid>272fd1d6-dc16-45dc-a309-2a03051a2592</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partial Credit: 1'])[1]/following::a[1]</value>
      <webElementGuid>1dee1bd5-bd7c-48f4-b468-06f39a952914</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 2'])[1]/preceding::a[1]</value>
      <webElementGuid>76396db0-d93f-40a4-a19c-9fa1fb2d6e7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Question 1']/parent::*</value>
      <webElementGuid>2c8a012d-eea2-438c-b556-19a84e5d02ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doExercise(1);')]</value>
      <webElementGuid>83125dcb-574a-4d63-b0ed-6d5662b730e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/a</value>
      <webElementGuid>052b99f8-123f-4fbb-b00d-39220b2d99af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'h1' and @href = 'javascript:doExercise(1);' and @title = '(3.1.1)' and (text() = 'Question 1' or . = 'Question 1')]</value>
      <webElementGuid>f9d862ba-7daf-4d41-a761-47a19e822a24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
