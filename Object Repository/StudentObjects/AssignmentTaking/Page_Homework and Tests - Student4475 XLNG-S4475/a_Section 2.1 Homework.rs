<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 2.1 Homework</name>
   <tag></tag>
   <elementGuidId>c53009c0-11af-48dd-9616-8133f29eb5e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 2.1 Homework')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>cff6f03f-ecf6-4ae8-aab7-a76aeeff8055</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(204232586, false, true);</value>
      <webElementGuid>c8572e30-8249-43dd-91f5-5044bdfcd5fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 2.1 Homework</value>
      <webElementGuid>0600c639-29cc-4acb-bbab-25c320f5e613</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[5]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>6ef48a7f-e290-4477-b4a7-3c429f2dbcea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[5]/th/a</value>
      <webElementGuid>1dd39321-c3f9-4713-95fa-0a64a92780e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 2.1 Homework')]</value>
      <webElementGuid>4be10d10-8b34-4f53-9a2e-db07c6659403</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[6]/following::a[1]</value>
      <webElementGuid>34af0e6f-e1f4-47ec-84b6-e0cfe9fedd96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 1.4 Homework'])[1]/following::a[1]</value>
      <webElementGuid>7644aa79-783c-43f1-acd7-0d7c44b8c135</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 2.1 Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>0bad45a4-1f22-4691-ab3c-6e37ece78b84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 2.1 Homework']/parent::*</value>
      <webElementGuid>5a02362c-40b6-4afd-bf64-62b8c4ffa61d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(204232586, false, true);')]</value>
      <webElementGuid>0d0afba6-e000-4c36-a157-ca7f2f1bbb43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/th/a</value>
      <webElementGuid>98a8f74b-08f0-4169-8ed8-ca342d9fe845</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(204232586, false, true);' and (text() = 'Section 2.1 Homework' or . = 'Section 2.1 Homework')]</value>
      <webElementGuid>00cb002f-2a6f-4fad-a975-f7728e7437c1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
