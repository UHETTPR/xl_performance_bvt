<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 6.1 Homework</name>
   <tag></tag>
   <elementGuidId>f8a47501-091b-4e3d-bdf9-74b5200d8192</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 6.1 Homework')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6aaaadb8-3ba4-41a9-8e45-ecdc51890144</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(204232594, false, true);</value>
      <webElementGuid>69a47b71-9443-443d-9b31-435930d9daf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 6.1 Homework</value>
      <webElementGuid>10428838-602b-46c2-9720-cbf954afd838</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[13]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>47deb244-7074-4199-95f2-0d2b989673a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[13]/th/a</value>
      <webElementGuid>4a069400-c247-4845-8c0c-c201884fda28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 6.1 Homework')]</value>
      <webElementGuid>ad0ffb39-6970-423e-83f5-8e335733be68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[14]/following::a[1]</value>
      <webElementGuid>29448f8c-80c9-41c3-a12d-8957b633a8d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 5.3 Homework'])[1]/following::a[1]</value>
      <webElementGuid>a5453c76-bb6c-42b2-ad4e-90140cf6ae71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 6.1 Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>64bd386e-3cb0-4746-997c-8540f9b5512b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 6.1 Homework']/parent::*</value>
      <webElementGuid>a4bfbd4a-95d0-4a11-a7fb-cc3986d5007d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(204232594, false, true);')]</value>
      <webElementGuid>3bd46900-82b1-4e5f-86f6-f4e33b4cc6c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[13]/th/a</value>
      <webElementGuid>7e06d9d3-4011-433a-b524-82523e5560d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(204232594, false, true);' and (text() = 'Section 6.1 Homework' or . = 'Section 6.1 Homework')]</value>
      <webElementGuid>601a0457-f8a7-43c7-bf31-a4047c12a12d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
