<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 9.3 Homework</name>
   <tag></tag>
   <elementGuidId>b0c65f55-37cc-4aff-90bf-24ea4b2b2259</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 9.3 Homework')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>2ce40a78-0500-4906-b9d4-e41b54aa0659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(204232601, false, true);</value>
      <webElementGuid>0666d2db-1729-434e-8ce7-503c03e23b81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 9.3 Homework</value>
      <webElementGuid>2e12b83e-835c-473f-be1b-2da998d78129</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[@class=&quot;alt&quot;]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>355b1468-ea51-4ddd-b8b4-ad76d61b75d4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[20]/th/a</value>
      <webElementGuid>2892d66d-81d9-4c80-afb9-033062023982</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 9.3 Homework')]</value>
      <webElementGuid>b2b784e0-8015-4187-9263-9e4f9fd7dfaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[21]/following::a[1]</value>
      <webElementGuid>5a7c6ebb-00e4-4b9e-9882-e7cf814264c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 9.1 Homework'])[1]/following::a[1]</value>
      <webElementGuid>fa3794f2-91c2-47f6-9c7b-c9b13b0320d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='for Section 9.3 Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>67e01643-084f-475e-a069-257eafb595c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 9.3 Homework']/parent::*</value>
      <webElementGuid>c563865a-b065-496e-a710-4a493d192501</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(204232601, false, true);')]</value>
      <webElementGuid>cf868da9-127d-4431-a941-0ff669f7c55a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[20]/th/a</value>
      <webElementGuid>9ecbf1b1-03da-48b7-b637-37689cd51a4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(204232601, false, true);' and (text() = 'Section 9.3 Homework' or . = 'Section 9.3 Homework')]</value>
      <webElementGuid>841a3e37-c91b-4896-85e9-4f47ee105f14</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
