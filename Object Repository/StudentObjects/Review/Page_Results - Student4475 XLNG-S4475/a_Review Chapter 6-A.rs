<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Review Chapter 6-A</name>
   <tag></tag>
   <elementGuidId>692395d2-f3d0-40d4-8ba7-b26930757a74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='row76518582']/td/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#row76518582 > td.nowrap > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:WebForm_DoCallback('__Page',411871046,reviewCallBack,reviewCallBackPreCall(),reviewCallBackError,true);</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Chapter 6-A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Review Chapter 6-A</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;RepeaterContainer&quot;)/div[@class=&quot;responsive-table&quot;]/table[@class=&quot;table table-inner-bordered&quot;]/tbody[1]/tr[@id=&quot;row76518582&quot;]/td[@class=&quot;nowrap&quot;]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row76518582']/td/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:WebForm_DoCallback('__Page',411871046,reviewCallBack,reviewCallBackPreCall(),reviewCallBackError,true);&quot;)]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:WebForm_DoCallback(&quot; , &quot;'&quot; , &quot;__Page&quot; , &quot;'&quot; , &quot;,411871046,reviewCallBack,reviewCallBackPreCall(),reviewCallBackError,true);&quot;) and (text() = 'Review Chapter 6-A' or . = 'Review Chapter 6-A')]</value>
   </webElementXpaths>
</WebElementEntity>
