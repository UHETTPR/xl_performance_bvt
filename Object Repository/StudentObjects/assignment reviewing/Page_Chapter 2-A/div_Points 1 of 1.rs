<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Points 1 of 1</name>
   <tag></tag>
   <elementGuidId>2a1c08a8-32fe-4783-86da-f7bb31857437</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                        
                        Points: 1 of 1
                    ' or . = '
                        
                        Points: 1 of 1
                    ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>07da4882-1c7d-4af2-8847-73c3b1e76ac8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>itemScoreGroup</value>
      <webElementGuid>df3a2723-fd78-44c6-905d-73c23ba6139a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>itemScore</value>
      <webElementGuid>a8a23b2d-c25b-46b8-8d6f-4d287ed1515d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Points: 1 of 1
                    </value>
      <webElementGuid>20fd6c94-2c98-4616-b002-6f25381760bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutRight&quot;]/div[2]/div[@class=&quot;itemScore&quot;]</value>
      <webElementGuid>c432a18e-c9a2-41f5-9aa8-6dfb2afda435</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>8d928d7e-b51d-46da-96c0-abd0e6f4cc2a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div[2]</value>
      <webElementGuid>0e120abf-c46e-4227-bf67-dbb8ec70cc2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Score:'])[1]/following::div[1]</value>
      <webElementGuid>5c16bc57-1630-48aa-aef0-ff36eca81b3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 5,'])[1]/following::div[6]</value>
      <webElementGuid>d2d99baf-df8a-4b66-8cd8-51cd62a83938</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[1]</value>
      <webElementGuid>fb14e36b-62a3-4445-9761-f1333662af85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div[2]/div[2]</value>
      <webElementGuid>35c84c9e-284b-4610-aaf9-a892d33cf754</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        Points: 1 of 1
                    ' or . = '
                        
                        Points: 1 of 1
                    ')]</value>
      <webElementGuid>ccf7727d-4e11-4cfc-8813-c2f3bfd73b8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
