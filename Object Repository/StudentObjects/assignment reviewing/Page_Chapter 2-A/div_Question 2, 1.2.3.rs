<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 2, 1.2.3</name>
   <tag></tag>
   <elementGuidId>54a1a3c3-61b8-4b8d-935a-4ade9effe4a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                    
                    
                        
                            Question 2, 1.2.3
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 2, 1.2.3
                        
                        
                    
                    
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>94f8bc20-0129-4264-a59d-5a0461deb76b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>89d58cbb-9739-494a-850d-248266c376c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>dab18cc6-e3ba-4d4c-b29c-b6d819fab349</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            Question 2, 1.2.3
                        
                        
                    
                    
                </value>
      <webElementGuid>b69c4bc2-4b35-4357-b465-54ebb7beb7eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>d7bdc0ab-a253-4061-b6e6-115dbedf2a87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>78f0ca0f-1ea3-4547-becc-bfc674641a7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div</value>
      <webElementGuid>177bfb98-8fb9-4acb-aff7-442ee423a8c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/following::div[4]</value>
      <webElementGuid>47bb054e-52c9-4e18-b8c7-e5f783944a72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>fdefbe9e-68cf-40ce-bb86-73f1138011bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div</value>
      <webElementGuid>0e84d69e-1ec4-49a5-a00f-e8bdfc2a3058</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                            Question 2, 1.2.3
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 2, 1.2.3
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>2cd37a25-5de0-4d9b-bc9b-db89f85e74cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
