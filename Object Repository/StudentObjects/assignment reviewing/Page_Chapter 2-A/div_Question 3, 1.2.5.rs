<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 3, 1.2.5</name>
   <tag></tag>
   <elementGuidId>22182d57-c61e-411c-9b61-5cbc5e75d779</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                    
                    
                        
                            Question 3, 1.2.5
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 3, 1.2.5
                        
                        
                    
                    
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>70e5b019-8f24-4c4b-884f-a925ad0aa8f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>4de13fa4-dadc-47df-8930-b6ed1e102b4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>71eab80a-e573-4bb8-93a1-b2555a3b2c46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            Question 3, 1.2.5
                        
                        
                    
                    
                </value>
      <webElementGuid>81c93588-1b13-4711-8c86-f93b9bba6373</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>739d0ca5-ee0d-48a6-b0b6-ab3fdc4f7315</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>9f372e74-5dac-49d2-af3e-ddac6f59f6a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div</value>
      <webElementGuid>dca8fd40-aceb-475d-807b-65906540265a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/following::div[4]</value>
      <webElementGuid>0fd19879-19ed-448e-81b7-a8fb09c93007</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>f3933f73-4d98-4de4-a365-d9d3f3d9daaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div</value>
      <webElementGuid>00af2dd1-f9d8-4c97-8955-8ccd91a2d08b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                            Question 3, 1.2.5
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 3, 1.2.5
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>e8f55b00-6866-4f62-a13b-e7840e91dd18</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
