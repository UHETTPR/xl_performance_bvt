<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Return to Homework_activityFrame</name>
   <tag></tag>
   <elementGuidId>3166250e-a106-473d-862a-bed7528d25d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#activityFrame</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='activityFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>61ce8534-45aa-47cc-bb23-66c321a5f574</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>8d45bf5f-59db-4ceb-a84d-2da580a53dcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>activityFrame</value>
      <webElementGuid>5eaa1441-f295-42f7-924d-24984947a625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true</value>
      <webElementGuid>4b0e5c77-fab3-4e84-9f4d-6b01a47e50b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>auto</value>
      <webElementGuid>44d8e0bd-7cb6-4745-9bf9-0d0bad2ddb16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Test Overview : Review  Test: Chapter 2-A</value>
      <webElementGuid>35802ce1-c842-4f23-a067-064a6530791f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>webkitallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ef15ca82-3503-4cc4-bba7-6da577f5d388</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mozallowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>1d2d8a16-c1a4-4f57-ad77-8b9977079520</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>86dba67e-2288-483c-a887-b621965b45ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;activityFrame&quot;)</value>
      <webElementGuid>52bb5141-1787-4d1b-92d1-adaedaa7cdb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40</value>
      <webElementGuid>a1b0d7dc-5c51-49a0-a613-962336886fcf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='activityFrame']</value>
      <webElementGuid>76a90863-5e95-4445-bcfa-26784c4b15c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assignmentViewer']/div/div[3]/table/tbody/tr/td/div/iframe</value>
      <webElementGuid>e7c8a95e-a214-4f37-8fee-07871e7fb60f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>a4d801aa-3a26-44d2-8d79-81765a0265a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@id = 'activityFrame' and @src = 'https://xlitemppe.pearsoncmg.com/Player/Player.aspx?cultureId=&amp;theme=math&amp;style=highered&amp;disableStandbyIndicator=true&amp;assignmentHandlesLocale=true' and @title = 'Test Overview : Review  Test: Chapter 2-A']</value>
      <webElementGuid>2714127b-a696-4257-86e9-2fa15e4befdf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
