<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Entire course to date</name>
   <tag></tag>
   <elementGuidId>526fc1d0-0b8a-455e-aed5-a65426464969</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Entire course to date')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bcba29a7-d6d1-48c6-baf5-25a2faddb083</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-menu-id</name>
      <type>Main</type>
      <value>Past_month</value>
      <webElementGuid>543259a7-9777-4aaa-bf01-a8c00d856007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')</value>
      <webElementGuid>39f401a3-5fcc-4026-8189-8f93ad1a25b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Entire course to date</value>
      <webElementGuid>57ac9f78-cc1c-40b1-88be-46f8f90c2a75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ul_Past month&quot;)/li[3]/a[1]</value>
      <webElementGuid>d8099e3b-c7c9-4d77-9a1c-cfe834f91796</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ul_Past month']/li[3]/a</value>
      <webElementGuid>ae387046-c48e-480d-8924-bb7db1f1ac66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Entire course to date')]</value>
      <webElementGuid>86aacb67-c3d5-48d8-b6ce-fbb4bcacc991</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Past month'])[1]/following::a[1]</value>
      <webElementGuid>24735154-52e4-46f1-b5ee-760bb4f3f443</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Past 2 Weeks'])[2]/following::a[2]</value>
      <webElementGuid>0362038f-f696-464d-b72e-0446afe4e36c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[1]/preceding::a[1]</value>
      <webElementGuid>5c9848b9-8ff8-474d-aa7c-047898666132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/preceding::a[1]</value>
      <webElementGuid>2308aa64-971b-4941-98a7-407905c058ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Entire course to date']/parent::*</value>
      <webElementGuid>e88a88d2-3484-4f19-bbb0-389f7353e7ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')&quot;)]</value>
      <webElementGuid>03d50bca-b094-459e-86c6-d5589e2b3959</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div/div/div/div/ul/li[3]/a</value>
      <webElementGuid>1b2fb096-6f2f-414e-b118-607cf78cebe7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Entire course to date' or . = 'Entire course to date')]</value>
      <webElementGuid>6f1823e6-e38a-4b79-808d-5a7f081a7c79</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
