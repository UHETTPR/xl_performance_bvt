<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Warning            You are reviewing yo_14d351</name>
   <tag></tag>
   <elementGuidId>e30dfc98-e12f-4887-a262-2da2afb49d61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dvAlertWarning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dvAlertWarning']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>85c158b6-5acb-41eb-bff1-bbd1b308077b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dvAlertWarning</value>
      <webElementGuid>23eb79db-e138-4b7b-a0e6-bbdec4f61787</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-warning</value>
      <webElementGuid>b57b9578-bcfe-49df-84d1-b3ae921257c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.</value>
      <webElementGuid>ccd32436-28de-4007-ad0c-92e6b27bb7fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dvAlertWarning&quot;)</value>
      <webElementGuid>e6961468-1118-4067-aaac-6d849dbdcd06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='dvAlertWarning']</value>
      <webElementGuid>2fad83e0-32d8-4a60-8ac4-7b67e5511506</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]/div/div[3]</value>
      <webElementGuid>61ee4158-48ee-46d9-9073-442e1002e360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::div[1]</value>
      <webElementGuid>dfbf8fe4-d2f5-4897-9273-4fea2e21761f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Review Homework'])[1]/following::div[1]</value>
      <webElementGuid>fcfcf4d4-b08f-4ccb-9424-0cfc02a1744d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]</value>
      <webElementGuid>7cd35d16-c4df-4478-b281-1591e1c08278</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'dvAlertWarning' and (text() = '
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.' or . = '
        Warning:
   
        
	 You are reviewing your homework. Any work you do here will NOT change your score.')]</value>
      <webElementGuid>23aab483-a47e-41f8-98ed-30d7e853c580</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
