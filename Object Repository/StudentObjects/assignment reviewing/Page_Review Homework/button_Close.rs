<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close</name>
   <tag></tag>
   <elementGuidId>9171e2b1-e4e9-4361-8031-6b32a25caa20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_dijit-bootstrap_Button') and (text() = 'Close' or . = 'Close')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>793b932a-104d-44ce-af6c-8ca9942d109c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>btnClose</value>
      <webElementGuid>371d8c61-e02f-4583-8d1a-ab117046beb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btnClose</value>
      <webElementGuid>e8284fca-ba82-4663-8293-43907218f093</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-type</name>
      <type>Main</type>
      <value>xl.dijit-bootstrap.Button</value>
      <webElementGuid>21c2ff4d-d77e-4811-92f6-7baf7d00345c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dtabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>1cae72b5-a952-402b-901b-7c0ffd1f4f21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5f3af4b4-1b45-43bb-a79e-8902763371d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>bfe42e77-127b-41a4-8461-ef900afa9438</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_4</value>
      <webElementGuid>ac3538f9-dca0-47f3-8312-41a7bdc6cda9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_4</value>
      <webElementGuid>3dbf27e4-aa7c-4219-9a05-4f584015e9e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3de66d23-ab16-45cd-98cb-1dcae8886c96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Close</value>
      <webElementGuid>5ee9829c-22d9-47c4-a9bf-4b127c36985d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close</value>
      <webElementGuid>a2bfeab9-2c85-43a5-b869-7b6c22fcd3ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_4&quot;)</value>
      <webElementGuid>fd943d74-935d-46da-85ce-acdd02326584</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>3d2beed0-39a6-431d-98fc-96ece969d78a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_4']</value>
      <webElementGuid>48390532-2399-4f64-b1c6-38622414da20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[3]/div[2]/button</value>
      <webElementGuid>f4b8a401-e44c-4b19-902b-0241ca25b9d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[5]/following::button[1]</value>
      <webElementGuid>09bf2dd2-122a-4e52-a890-50c6bb4a22ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help center'])[1]/following::button[1]</value>
      <webElementGuid>033d3948-ab67-4213-92cd-a76d00aeb0c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/preceding::button[1]</value>
      <webElementGuid>f68636a2-403e-4209-9761-a79b0aadd803</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='correct,'])[1]/preceding::button[2]</value>
      <webElementGuid>ff79dc09-b677-4d0d-802c-0bee2abcebb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Close']/parent::*</value>
      <webElementGuid>e8a18032-bd12-45ff-89be-40c045a23347</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>998c0db9-ed08-4610-b61a-8346eac72dde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_4' and (text() = 'Close' or . = 'Close')]</value>
      <webElementGuid>f58cfae1-be39-467d-9712-fa761d460997</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
