<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Points 0 of 1</name>
   <tag></tag>
   <elementGuidId>debe21f4-ec7d-4961-acb5-a29863ee11b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                        
                        Points: 0 of 1
                    ' or . = '
                        
                        Points: 0 of 1
                    ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a2b2da44-1a64-4685-879b-1b831f7af584</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>itemScoreGroup</value>
      <webElementGuid>8a4e7731-8863-4204-8e36-cb2a992b1893</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>itemScore</value>
      <webElementGuid>f404c2ab-009e-4815-8284-463f996d3ae6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Points: 0 of 1
                    </value>
      <webElementGuid>25780178-7e25-4663-89a5-d00304880809</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutRight&quot;]/div[2]/div[@class=&quot;itemScore&quot;]</value>
      <webElementGuid>ce0d24a9-7d5c-4c92-bbd4-93e398c9abdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>718eb0e8-5aaa-4533-9b1f-fe9b10a52035</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div[2]</value>
      <webElementGuid>5134445e-7216-4704-b720-cfb4b921b283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/following::div[1]</value>
      <webElementGuid>60343ea5-1694-4723-8053-6a595708b1eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 5,'])[1]/following::div[6]</value>
      <webElementGuid>836af35e-3322-40a8-af10-57ea286b4c45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[1]</value>
      <webElementGuid>7dd0a2e2-ec9e-45fb-b54b-248e63436ea7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div[2]/div[2]</value>
      <webElementGuid>8b4f0813-e550-446d-8673-3b54627d317c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        Points: 0 of 1
                    ' or . = '
                        
                        Points: 0 of 1
                    ')]</value>
      <webElementGuid>045a625c-c192-4848-9926-2ec45f2f8ed7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
