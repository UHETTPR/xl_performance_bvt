<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Points 1 of 1</name>
   <tag></tag>
   <elementGuidId>62dbf977-3a2f-4234-8d52-f1b5d6370a57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                        
                        Points: 1 of 1
                    ' or . = '
                        
                        Points: 1 of 1
                    ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>39e3c3f8-20c4-46ba-8970-81a8598480b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>itemScoreGroup</value>
      <webElementGuid>1cae525d-5e0d-4fa9-8d96-200e6725f5b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>itemScore</value>
      <webElementGuid>54ef6f6c-69c0-4a28-b93f-b8127524a9a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Points: 1 of 1
                    </value>
      <webElementGuid>ea71a0f1-f2f9-4797-81c9-cdb9cc3a0cf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutRight&quot;]/div[2]/div[@class=&quot;itemScore&quot;]</value>
      <webElementGuid>63caade0-d161-4088-b5dc-1679f11dca81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>eab51fdb-18ed-41d4-a657-a4c4eacb784a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div[2]</value>
      <webElementGuid>64670151-5169-4d23-8811-441e3388ae5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/following::div[1]</value>
      <webElementGuid>0b0b7661-b0a8-4b01-adbc-62a156d6850c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 1,'])[1]/following::div[6]</value>
      <webElementGuid>96ed7a22-8e0d-423a-806e-c31ff6f7772a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[1]</value>
      <webElementGuid>42746a0f-29ef-4fdf-8bca-d7953165cb36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div[2]/div[2]</value>
      <webElementGuid>c99f9ab5-81e5-478f-8541-358fb86c601c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        Points: 1 of 1
                    ' or . = '
                        
                        Points: 1 of 1
                    ')]</value>
      <webElementGuid>ea50341a-5765-46c2-84f9-a0548331cb57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
