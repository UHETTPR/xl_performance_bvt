<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 1, 1.2.1</name>
   <tag></tag>
   <elementGuidId>c0805a9a-7568-40a9-82e7-682ba69fe7b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e2022915-3414-4c44-9f5a-42f78905efdf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>cdbb3e40-d451-46b2-9389-de28e85f2aa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>44e36f70-26ec-4070-ba7e-9ef3fcc190bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                </value>
      <webElementGuid>bdb1e4be-8ee1-491d-98e9-1c16ba062670</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>313b5862-46b1-40af-a7d1-29edb9600e64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>d68f714f-0f8e-4903-8c9d-8e0cfe9d9c25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div</value>
      <webElementGuid>6237d0df-c8c4-4141-a03c-287467283282</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework:'])[1]/following::div[4]</value>
      <webElementGuid>5347e4f4-3228-414c-986a-a6df1cafece7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>e87fa93f-2324-4e38-99f0-048f065aaf6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div</value>
      <webElementGuid>b8296c78-af59-4ff3-885d-f195ab96f878</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>38f5a2d0-f036-4ce3-af30-d0b020af2dd5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
