<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Question 5</name>
   <tag></tag>
   <elementGuidId>d334f53c-1a7d-4b65-9ee4-fe77df55c5a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[(text() = 'Question 5' or . = 'Question 5')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0259ca66-9188-4bd6-b5e9-caa0d6fbf0cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Question 5</value>
      <webElementGuid>9a5064e4-b6c6-457f-a21c-b4a2b69ce5bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit_HigherEdAssignmentReviewNav_0&quot;)/div[@class=&quot;assignmentreviewnavtablediv&quot;]/div[@class=&quot;assignmentreviewnavListContainer&quot;]/ul[1]/li[5]</value>
      <webElementGuid>dda10216-8a7a-4174-a1a3-9317ea0be86c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>d43cc117-5c0e-4f4f-af82-a61b7dd02157</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_dijit_HigherEdAssignmentReviewNav_0']/div[2]/div/ul/li[5]</value>
      <webElementGuid>610f7aef-0e45-4c46-9a31-4aa6e176ba48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question list'])[1]/following::li[5]</value>
      <webElementGuid>067d5e78-fb67-4a47-9450-6e8f74c8535e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='correct,'])[1]/following::li[5]</value>
      <webElementGuid>566ad006-c22a-4980-a661-a4bf51d16165</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question needs grading.'])[1]/preceding::li[13]</value>
      <webElementGuid>9d334920-94f9-406c-b543-2d0972b735f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grade:'])[1]/preceding::li[13]</value>
      <webElementGuid>7b71a1f8-b03f-4c7d-9a2f-8f6dd64d490a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]</value>
      <webElementGuid>54ad00d2-060b-40c8-961e-9d36874635e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'Question 5' or . = 'Question 5')]</value>
      <webElementGuid>a43acf69-d37d-40bf-a42e-824c08359b89</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
