<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username_username</name>
   <tag></tag>
   <elementGuidId>ef932dff-f469-48a5-b52c-c16d5f3f7c8c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#userName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='userName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>cc9f204e-619c-40f8-bf32-16887414af53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>50df105d-df04-440d-97d8-492576989263</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>3ac82e31-cbf1-46d0-9f11-54c90bd5b950</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userName</value>
      <webElementGuid>7ab13ae9-6296-48f3-a4fc-570737e8b785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>medium-width required-field error</value>
      <webElementGuid>c48d745a-e2e8-4131-9763-3e986d7c0387</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;userName&quot;)</value>
      <webElementGuid>98101fa0-af71-4162-bf01-58f28eb6c399</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='userName']</value>
      <webElementGuid>37c38994-ba72-45da-9597-357f1fbdf832</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='inputfrm']/div/input</value>
      <webElementGuid>d40e31af-eec4-41a6-bbbd-ca7548c89699</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>7a4cad64-20e8-48c4-a86b-e7103e2f58ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'username' and @id = 'userName']</value>
      <webElementGuid>da80aaa3-5d4e-4c5c-909a-cb3ddea6acde</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
