<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Sign In</name>
   <tag></tag>
   <elementGuidId>150fc4da-20de-41cb-a3ef-70989dd59d87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.button.button-big-icon.bg-color-match-medium.uppercase.mar-top-x1Half</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>15512f73-3ecc-4e1a-b1ea-2c1f8b1035a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>80c9ea14-9a01-4386-837f-9623f45fb45e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button-big-icon bg-color-match-medium uppercase mar-top-x1Half</value>
      <webElementGuid>bd59b197-eb7c-4564-9a18-3ff0182663e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Sign In
                        </value>
      <webElementGuid>287d7b3c-644f-4534-b9a8-c60c80efba86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inputfrm&quot;)/button[@class=&quot;button button-big-icon bg-color-match-medium uppercase mar-top-x1Half&quot;]</value>
      <webElementGuid>3a06252e-5028-4b5d-8009-3de8274c4dc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>256480d9-44e4-42f4-a50c-7c95238b420c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='inputfrm']/button</value>
      <webElementGuid>6b2dfad9-694e-4050-b5ba-24c23a47ce0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]</value>
      <webElementGuid>006fc67c-7e23-4df7-9559-d899ca02f177</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::button[1]</value>
      <webElementGuid>e9c0af89-51c4-4683-824f-4313dccbb069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your username or password?'])[1]/preceding::button[1]</value>
      <webElementGuid>579168b3-a160-4959-a01b-cb864afdac19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit our home page to register!'])[1]/preceding::button[1]</value>
      <webElementGuid>0676bbaf-c0d3-4d1a-af81-447c945ce127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>d428ee72-8070-4b2e-b5af-f1ae7703ff18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>46170816-4865-4bb1-866e-efae5383c857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = '
                            Sign In
                        ' or . = '
                            Sign In
                        ')]</value>
      <webElementGuid>ef231d3e-3e0d-4c98-a526-6135de42241e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
