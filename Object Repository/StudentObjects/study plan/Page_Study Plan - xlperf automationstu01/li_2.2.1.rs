<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_2.2.1</name>
   <tag></tag>
   <elementGuidId>713961b6-80d0-48a2-b2bb-8f534c6ab88c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.study-plan-details-list > li</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='studyPlanAllChapters']/div/div[2]/div/div/ul/li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>9e47daeb-becd-44c6-8b29-2aa015d00ceb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-repeat</name>
      <type>Main</type>
      <value>exercise in selectedObjective.exerciseList track by $index</value>
      <webElementGuid>c8ce088a-19e5-4d7e-856c-903bb046a9c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                        
                                        
                                            2.2.1
                                            
                                        
                                        
                                    
                                </value>
      <webElementGuid>5281c52f-d6ff-4a6c-ac2f-4c75fb0c40bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;studyPlanAllChapters&quot;)/div[1]/div[@class=&quot;exercise-container&quot;]/div[@class=&quot;study-plan-results&quot;]/div[1]/ul[@class=&quot;study-plan-details-list&quot;]/li[1]</value>
      <webElementGuid>7d4a89a2-69c4-4a67-9780-d69fe08e7c42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='studyPlanAllChapters']/div/div[2]/div/div/ul/li</value>
      <webElementGuid>714ad6b1-f7b3-4919-9fcf-6bff7692a0c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This question is from a mastered objective.'])[2]/following::li[1]</value>
      <webElementGuid>5d066a0e-2a87-445f-8971-492ae706fe03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This question needs more study'])[2]/following::li[1]</value>
      <webElementGuid>d5c74f03-b043-4d0a-98be-84b62a120307</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='take a sample test now'])[1]/preceding::li[11]</value>
      <webElementGuid>90442709-50a2-4a61-8927-e71809df046f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms of Use'])[1]/preceding::li[11]</value>
      <webElementGuid>385abc8f-581f-4e00-92f5-93cb255727fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li</value>
      <webElementGuid>726b45d7-9f8e-49a8-a3b3-0b93dfc0e48f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                                    
                                        
                                        
                                            2.2.1
                                            
                                        
                                        
                                    
                                ' or . = '
                                    
                                        
                                        
                                            2.2.1
                                            
                                        
                                        
                                    
                                ')]</value>
      <webElementGuid>ff9aeb23-dc55-4879-b14c-6e3f67c48a96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
