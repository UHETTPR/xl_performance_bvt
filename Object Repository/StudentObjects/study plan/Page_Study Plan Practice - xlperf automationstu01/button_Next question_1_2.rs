<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Next question_1_2</name>
   <tag></tag>
   <elementGuidId>af3942a8-4743-460f-b283-491a41af3736</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_dijit-bootstrap_Button') and (text() = 'Next question' or . = 'Next question')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_89</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>33544c10-4a5a-4696-afe7-2b45dcbc9322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>btnNextQuestion</value>
      <webElementGuid>95cfa4e0-4072-48d4-88cb-8a79bf76d94f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn-default btn btn-primary btnOnly</value>
      <webElementGuid>17d23bb6-3e81-4cdc-aac4-4a32bc6baffd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-type</name>
      <type>Main</type>
      <value>xl/dijit-bootstrap/Button</value>
      <webElementGuid>7c0f94b7-9d7b-4b4b-a591-8efe5cd6ac12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-props</name>
      <type>Main</type>
      <value>type:&quot;button&quot;</value>
      <webElementGuid>7dacdef6-e73f-41ab-911b-542606b55334</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-event</name>
      <type>Main</type>
      <value>onClick:nextQuestionClicked</value>
      <webElementGuid>9435e214-2932-4c16-bcff-43ac55cf3cda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>21</value>
      <webElementGuid>136cad0a-7a90-4a6c-9cff-fefc738747e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d5a037cc-ae4d-4b3d-b957-c17b1de5aeb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_89</value>
      <webElementGuid>7972d686-4738-4393-a026-66d67aa86f1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_89</value>
      <webElementGuid>e424b896-9292-4399-8e93-7b895bb1039c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next question</value>
      <webElementGuid>1419ba32-736d-437d-a5ae-fedf60ad9c8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_89&quot;)</value>
      <webElementGuid>77698067-452c-4686-a672-ea4271660aa9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>e4ab2ec3-1ce9-4b4f-8f76-e85a69048a1b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_89']</value>
      <webElementGuid>e86f5f3f-bf81-4f84-9ef5-1efeb4a8c8b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_FeedbackMessage_2_buttonBar']/button[4]</value>
      <webElementGuid>c3c27e0d-db65-4880-aab5-92eb7f961f39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SP'])[1]/following::button[1]</value>
      <webElementGuid>943533ec-dfce-42d5-a028-a0a7810c7e91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GS'])[1]/following::button[2]</value>
      <webElementGuid>9ef88e30-27b3-4b80-9f2a-b12b5230a257</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::button[1]</value>
      <webElementGuid>d1ca20b8-fcc0-4c49-9441-32a04df4e681</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[2]/preceding::button[2]</value>
      <webElementGuid>8af2c668-e599-4a9f-8239-58c383826cf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next question']/parent::*</value>
      <webElementGuid>437239b8-9bd8-41bb-bcab-f28568bcfac1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button[4]</value>
      <webElementGuid>5ff40edb-36d3-4fd5-be82-c32bcca4deda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_89' and (text() = 'Next question' or . = 'Next question')]</value>
      <webElementGuid>3a5c31e8-917d-455b-b1f8-1db82b210280</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
