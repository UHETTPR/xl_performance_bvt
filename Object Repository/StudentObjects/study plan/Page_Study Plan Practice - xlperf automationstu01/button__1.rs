<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1</name>
   <tag></tag>
   <elementGuidId>69878208-e2f0-427d-9278-d0c9b2a49552</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_player_PlayerMenu') and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_PlayerMenu_2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>61306ef5-9e6a-40ce-9c81-5f24c2cf19f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>icon, _buttonNode, focusNode</value>
      <webElementGuid>805e93a3-8cb1-4cb8-8a8a-af874f7db261</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlPlayerMenuIcon dijitDownArrowButton dijitHasDropDownOpen</value>
      <webElementGuid>e2fb7b9f-f0e1-491c-b3b9-81405b076425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>eb08daee-c11e-4773-ac0b-f44f7f88db72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>1cbb27e4-a1c4-4983-8b00-c9d2fd0f6716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>47def6de-da9b-48e8-94ab-1f4a51d0bf86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>ae4e3573-0075-4eb9-815f-de6132ee6c73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_PlayerMenu_2</value>
      <webElementGuid>9b7d5914-7637-4479-a9aa-81762a217893</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>026fb765-a56a-48aa-8c18-612462665cfd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>popupactive</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7d6f1b29-fc7b-430c-87a9-6ff626da7075</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>15084f22-e23c-4553-851a-8f9e4424125d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>dijit_Menu_7</value>
      <webElementGuid>f6835a4d-f6b0-4104-a0db-b422c54fdaa8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>68f2ef02-e748-4670-9388-971ea3d52c19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_PlayerMenu_2&quot;)</value>
      <webElementGuid>89c3d894-c71c-4be1-8454-63942dc87ec4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>5bf66177-d7eb-42d9-a2e3-ca04f05178e7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_player_PlayerMenu_2']</value>
      <webElementGuid>dcd28214-561a-4bf0-8045-76158f287752</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_1']/div/div[5]/div/button</value>
      <webElementGuid>187f7eec-524f-42ab-8a4b-835d9a25974a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[10]/following::button[2]</value>
      <webElementGuid>989c5229-e754-41ef-9051-302defe7536a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ask my instructor'])[1]/following::button[2]</value>
      <webElementGuid>dd6feaeb-bd4b-4f20-a3c6-17c8a8ac88c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/preceding::button[1]</value>
      <webElementGuid>2475f680-6848-4633-a558-a51b590caa29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear all'])[1]/preceding::button[1]</value>
      <webElementGuid>67d309d7-0b52-47f8-8479-b9906e96cebd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>42a14b5b-6b58-4627-aaa3-6f8c18129ba8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'xl_player_PlayerMenu_2' and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>ebca3614-d33e-47f5-9a46-f38c5c51969b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
