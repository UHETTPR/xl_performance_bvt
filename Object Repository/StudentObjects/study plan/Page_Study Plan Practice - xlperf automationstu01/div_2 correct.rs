<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_2 correct</name>
   <tag></tag>
   <elementGuidId>24192757-bdb3-4e9a-8473-11dccbbedca9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '2 correct' or . = '2 correct')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.overallScore</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ba82e011-b254-4402-ac31-da42a806a579</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>overallScore</value>
      <webElementGuid>d26abcf3-f779-4a5d-8cd5-a912dd7cfeef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>overallScore</value>
      <webElementGuid>96325f1a-c7f9-4ec9-b50c-5206f38820f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2 correct</value>
      <webElementGuid>0eead72e-786c-4e63-a76a-90d125dd76ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutRight&quot;]/div[2]/div[@class=&quot;overallScore&quot;]</value>
      <webElementGuid>b7c39d6f-9fb9-4dae-9b3a-f46af39d0735</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>abe18daa-1692-4341-b954-a367f5c06992</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div</value>
      <webElementGuid>30ea4043-6440-4330-96bf-c693e27c5462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Question 12,'])[1]/following::div[5]</value>
      <webElementGuid>ce0bf4ea-e1ab-4a9e-a268-4d9a0b023819</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points:'])[1]/preceding::div[1]</value>
      <webElementGuid>d49e79ee-0209-4c99-acce-94e06f91c9f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[2]</value>
      <webElementGuid>ab4e93c0-711a-41c5-a4c7-331bac54bfd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='2 correct']/parent::*</value>
      <webElementGuid>7e92bc4e-ac93-45a8-9316-34720f1d311d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div[2]/div</value>
      <webElementGuid>b5378fdf-bbe2-4a51-b911-99fc90c4bc4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '2 correct' or . = '2 correct')]</value>
      <webElementGuid>046d1df2-86bf-486f-bf67-44c297e2581f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
