<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fantastic                              _354ab9</name>
   <tag></tag>
   <elementGuidId>99a3e6f2-0ba7-4682-a1f1-3d71baf30c18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'xl_player_dialogs_FeedbackMessage')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_dialogs_FeedbackMessage_0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8bd1fd4f-3fd5-4e30-a859-59fb9ca019d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>feedbackDialog xlFocus correct dijitDialogFixed dijitDialog dijitDialogHover dijitHover dijitDialogFocused dijitDialogHoverFocused dijitHoverFocused dijitFocused</value>
      <webElementGuid>fd7355d6-4500-497d-8efb-6077103004ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>cf5eabb8-752e-4771-9041-81f171c6a068</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>dialogNode</value>
      <webElementGuid>16bace31-9795-4cfd-91d4-bf7cf85f5d79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0_title</value>
      <webElementGuid>2ce71d84-d328-45d0-bc94-013542cbd83e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0_message</value>
      <webElementGuid>8b17aab8-a9b6-44d4-97c9-588472e6e5af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>ab4da351-cca9-4b6f-98e0-fb1d75e933c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0</value>
      <webElementGuid>f06fe144-44cd-43e2-9b5f-b317129d6151</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0</value>
      <webElementGuid>11059010-5e14-4812-9a76-994948595fd7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
</value>
      <webElementGuid>74336f0e-7e7b-4061-92b1-cfb7cc23d5f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_dialogs_FeedbackMessage_0&quot;)</value>
      <webElementGuid>a8a695bf-88ed-4731-9919-b974f9c542c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>0dab16c3-0e67-44b3-bfff-a569de65592c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_FeedbackMessage_0']</value>
      <webElementGuid>ce1ae2c7-553a-4196-9be7-de1d869ad1b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[25]/following::div[1]</value>
      <webElementGuid>9a71a3d2-7b08-4e52-b7c0-bc6e777a1cbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download ipx'])[1]/following::div[1]</value>
      <webElementGuid>ca97dc1d-d3df-41cd-862c-7b937301ecfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]</value>
      <webElementGuid>70dc1745-29ef-494a-8de5-f6931d6c0617</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'xl_player_dialogs_FeedbackMessage_0' and (text() = '
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
' or . = '
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
')]</value>
      <webElementGuid>3f9d515b-51c3-4f5a-b166-9ecbd3c0cc12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
