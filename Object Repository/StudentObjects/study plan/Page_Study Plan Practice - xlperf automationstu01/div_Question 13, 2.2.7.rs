<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 13, 2.2.7</name>
   <tag></tag>
   <elementGuidId>31b2f151-3521-4019-9c90-6035ca58d0be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                    
                    
                        
                            Question 13, 2.2.7
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 13, 2.2.7
                        
                        
                    
                    
                ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6afb3d7e-7203-4fb5-8378-cc26ecc00a57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>4c4c8bc8-6cd3-4938-ab35-b5f96bf05136</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>a3d0086f-5b11-421f-8282-43b0fb111a96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            Question 13, 2.2.7
                        
                        
                    
                    
                </value>
      <webElementGuid>c3685771-b0a7-4432-968c-5e0c5a931525</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>2ce26a3b-a715-4f28-96a5-2dd7691fb68e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>e23d89d3-651f-42de-b2eb-3718685ac7f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div</value>
      <webElementGuid>6e454b1a-fec1-449a-a740-0151945210b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points:'])[1]/preceding::div[6]</value>
      <webElementGuid>8af92daf-a3a5-4908-8feb-882f0792436a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div</value>
      <webElementGuid>e5be0eca-f584-49ca-b455-ba383f8f043e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                            Question 13, 2.2.7
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 13, 2.2.7
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>8cf428ad-efda-4b45-b4ab-906d4b0ab111</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
