<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Skip step</name>
   <tag></tag>
   <elementGuidId>bbd7e290-fb69-469d-99ac-266ef15129b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem') and (text() = 'Skip step' or . = 'Skip step')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_52_text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>079b9bfc-bb3c-4682-ba7c-5644a651e44c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>9e4660ee-43d4-43bc-8a3f-f338b7fe608a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>f40ca2fa-33ab-4029-9fd3-e8f96ea3786b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>a1b1c01c-dba4-494d-b5e1-58e31db1a354</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_52_text</value>
      <webElementGuid>2a452a1d-18bf-43c1-a427-d5d6583ef88c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip step</value>
      <webElementGuid>14f8a5ca-474a-47ae-ace7-e77bb1c83b71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_52_text&quot;)</value>
      <webElementGuid>0dd4f0dd-3977-4a18-bec3-2d267d55f499</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>84945a04-fd6b-4b1e-95b2-e4767d18aecb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_52_text']</value>
      <webElementGuid>59da392d-4497-4ac4-947d-6ead0047fd85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_52']/td[2]</value>
      <webElementGuid>2fe11bef-eab1-4d48-bce2-1e0a4188e0c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hold step'])[1]/following::td[4]</value>
      <webElementGuid>5f81c81c-10f5-4d3c-8aa5-1326bc77a691</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/following::td[5]</value>
      <webElementGuid>ec889bc6-586b-4d85-b811-58f510593e4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[14]/preceding::td[2]</value>
      <webElementGuid>e356b1aa-34f5-4b80-9136-f38e99b1d1e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to end'])[1]/preceding::td[4]</value>
      <webElementGuid>9698d672-7076-418d-b805-ed46bcfae7d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip step']/parent::*</value>
      <webElementGuid>6ea258d4-54a3-43b6-ba4a-ec7130f99f15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[7]/td[2]</value>
      <webElementGuid>150c9cc4-a43d-485a-b606-99b368bae091</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_52_text' and (text() = 'Skip step' or . = 'Skip step')]</value>
      <webElementGuid>a97cfae2-03ad-47e8-aeb5-3f4f23737bbf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
