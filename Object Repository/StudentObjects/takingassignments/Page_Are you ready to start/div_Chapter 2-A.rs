<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Chapter 2-A</name>
   <tag></tag>
   <elementGuidId>150f3274-511c-414c-9c69-b9d204d0f8f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@title = 'Chapter 2-A' and (text() = 'Chapter 2-A' or . = 'Chapter 2-A')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[title=&quot;Chapter 2-A&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0aa3ab4f-96b1-40f5-8f1a-7546c693bce9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Chapter 2-A</value>
      <webElementGuid>b76c25d1-0846-4292-8c4a-4eb4f6e072d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Chapter 2-A</value>
      <webElementGuid>dd5aecb6-705b-4b35-b22b-5cb57a838187</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/section[@class=&quot;well ready-to-start&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-7&quot;]/table[@class=&quot;table-test-start&quot;]/tbody[1]/tr[1]/td[2]/div[1]</value>
      <webElementGuid>b9d912ae-ca47-4597-b3f2-e3b83fde4265</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/section/div/div/table/tbody/tr/td[2]/div</value>
      <webElementGuid>6575a33a-f6ba-4f0d-84ae-de606c113e66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/following::div[1]</value>
      <webElementGuid>ef0bc595-98cc-4a5f-8380-0c6ac85488d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Are you ready to start?'])[1]/following::div[3]</value>
      <webElementGuid>40258db0-5bf0-44fc-9040-b4cd752656b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Questions:'])[1]/preceding::div[1]</value>
      <webElementGuid>14d5d817-23ac-4e9e-9e15-9ff5d15bb8ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Attempts:'])[1]/preceding::div[1]</value>
      <webElementGuid>c742305c-1580-40ab-974e-afba64d88b86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chapter 2-A']/parent::*</value>
      <webElementGuid>2d9731d0-28ed-41b6-9e13-510bf5f795db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div</value>
      <webElementGuid>9e88f0df-61fc-4f4b-b649-a08e8145b582</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'Chapter 2-A' and (text() = 'Chapter 2-A' or . = 'Chapter 2-A')]</value>
      <webElementGuid>7a94c346-7b20-4d87-940b-52216e9101ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
