<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Are you ready to start</name>
   <tag></tag>
   <elementGuidId>939cb344-5daf-4058-a813-e7cd42ab422d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h1[@id = 'PageHeaderTitle' and (text() = '
            Are you ready to start?' or . = '
            Are you ready to start?')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>1602e046-0a18-4992-8fdc-0957547b2d42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>bea396fd-a4f5-4c70-a684-c7ad222c6cd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>visible</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>de1be2f8-34b9-41e5-a211-255721695412</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h1-font integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>16c25bcf-2d99-4d20-8000-d024b4cb69b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Are you ready to start?</value>
      <webElementGuid>10ce236a-f4cf-4558-b23d-a261571b8978</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>94d02186-68b5-4442-a4c9-1c1fbf0cc7ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h1[@id='PageHeaderTitle']</value>
      <webElementGuid>c18086d6-569b-495e-a9f8-0a52502260a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_HeaderStart_HeaderTitleH1Div']/h1</value>
      <webElementGuid>4a2b4a41-8cbe-41f8-8499-e20223a46edf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/following::h1[1]</value>
      <webElementGuid>dbbfac96-23e4-49cc-bcb9-596909d1ae2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MathXL Support'])[1]/following::h1[1]</value>
      <webElementGuid>2e4fcdaf-fa90-4043-8083-318708e19aa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h1[1]</value>
      <webElementGuid>af1593a5-bf9f-4bc6-8a41-2661d17e0f5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h1[1]</value>
      <webElementGuid>12f793d0-4113-42f3-9fd9-1390b97397a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>0c642b24-58a1-44bb-a0c7-5ab471b5107d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[@id = 'PageHeaderTitle' and (text() = '
            Are you ready to start?' or . = '
            Are you ready to start?')]</value>
      <webElementGuid>e7f00d32-6871-4d4e-aef1-a5f2138eefbc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
