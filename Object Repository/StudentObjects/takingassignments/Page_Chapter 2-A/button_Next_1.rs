<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Next_1</name>
   <tag></tag>
   <elementGuidId>03f59523-84f7-4a8c-9756-228912646301</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_44</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='xl_dijit-bootstrap_Button_44']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>edc73d2e-655e-4b02-b0df-d848fb4729a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8317e230-1dce-42df-bc07-1942f2f47b2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default navButton btn-primary</value>
      <webElementGuid>330b7f5b-c09f-4cad-a7f8-b012ab23ba51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>ee026f2a-22e9-4f67-bb8a-7ed9a7ca020a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_44</value>
      <webElementGuid>4875137a-10f9-44eb-bbfc-f6c089f9ca4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_44</value>
      <webElementGuid>0894cdaf-21ec-40e9-902f-e957c578d03d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>4b159867-af64-4364-bd91-66c5de81777c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>ecb60cd0-ae16-4c5e-b964-244a1de05960</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_44&quot;)</value>
      <webElementGuid>6c6e10ed-148a-4c9c-bdd6-8d63355ce52a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>014b039c-ff26-41cc-bc73-74755be6ae10</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_44']</value>
      <webElementGuid>cb91c40f-d887-4c7f-974b-777cacf4a5ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_1']/div[2]/div[2]/div[2]/button[2]</value>
      <webElementGuid>64679cab-33b5-48f6-964c-3a1f1017342c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skill builder'])[1]/following::button[2]</value>
      <webElementGuid>3a52e917-758b-42e4-b006-4bc91fcfa042</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/following::button[3]</value>
      <webElementGuid>7595bd8f-22bf-4337-89e2-9666d729bab3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'xl_dijit-bootstrap_Button_44', '&quot;', ')')])[1]/preceding::button[1]</value>
      <webElementGuid>169bbf9f-847f-4665-b347-d3d7eba83d92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please wait...'])[1]/preceding::button[1]</value>
      <webElementGuid>f5996cf9-83a3-4fc3-8a0d-f71f2968538a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>77448d06-0533-4324-a26d-f616ee5ab75f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/button[2]</value>
      <webElementGuid>7af845a3-7996-4621-bc59-ce59b034c688</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_44' and @title = 'Next' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>ecedc99c-6f91-44ed-bc4b-56ee889fdff1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
