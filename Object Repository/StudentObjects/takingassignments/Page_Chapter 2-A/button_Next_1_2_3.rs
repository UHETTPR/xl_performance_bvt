<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Next_1_2_3</name>
   <tag></tag>
   <elementGuidId>d1b231ef-160e-401a-be40-c4c57c124af4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_dijit-bootstrap_Button') and @title = 'Next' and (text() = 'Next' or . = 'Next')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_79</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b4e2f941-a203-4699-898a-14be492d6928</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a6ee1bfc-1e10-47a3-80b4-951edfb0a5b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default navButton btn-primary</value>
      <webElementGuid>6f65c994-9548-4805-9e1e-d0cb810cf268</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>8441e4a8-2d93-4fb9-856b-e6e45491dc88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_79</value>
      <webElementGuid>feb9bf87-05c8-4677-82bb-c42ed3a0bb2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_79</value>
      <webElementGuid>3406e635-9c95-4e46-b544-90b2a221943a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>eae6e0a8-8563-41e1-b8d3-562fea803ac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>7152791b-e568-46ad-9373-3accddef53e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_79&quot;)</value>
      <webElementGuid>4a2d9c85-6a86-4d13-8252-2c9d45ebcb41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>ee5df4c5-c750-4dc9-8f74-048d095fc02c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_79']</value>
      <webElementGuid>5ee4a13a-8b2c-4a69-a4c0-7f336173cf41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_3']/div[2]/div[2]/div[2]/button[2]</value>
      <webElementGuid>c1092f2d-d309-481d-bfec-4c9405030aca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skill builder'])[1]/following::button[2]</value>
      <webElementGuid>93af0447-6d49-490e-91c4-ac640d58ade3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/following::button[3]</value>
      <webElementGuid>3f254c24-9f95-4455-a5d6-1b62880bd95b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'xl_dijit-bootstrap_Button_79', '&quot;', ')')])[1]/preceding::button[1]</value>
      <webElementGuid>a815c755-ae90-4bd2-9995-8d2547d53588</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please wait...'])[1]/preceding::button[1]</value>
      <webElementGuid>fd9cd24b-421f-4d9a-b093-495c0d96c2b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>120e1ba4-294b-4852-ab75-4e29aa292761</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/button[2]</value>
      <webElementGuid>51003c9e-1a51-4835-886b-f954570ff3dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_79' and @title = 'Next' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>06a9cabc-57bf-441a-91a3-f3faa94daa91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
