<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1_2_3_4</name>
   <tag></tag>
   <elementGuidId>3146dce4-6c3c-4c64-ab46-26f9894a76f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_player_PlayerMenu') and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_PlayerMenu_5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>30dc1702-e613-4b37-9836-c6cf9e9f5dbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>icon, _buttonNode, focusNode</value>
      <webElementGuid>d9218f26-fa8e-48ef-96b2-ad0531b5f4ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlPlayerMenuIcon dijitDownArrowButton dijitHasDropDownOpen</value>
      <webElementGuid>2a7d74cd-b993-41a1-a1c4-08498adb49d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>bacee8c5-0f48-43ca-ba4a-203125e9ebc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2d6cee3d-42c2-4663-8261-053ff7d58519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>da3e2b80-00b7-4052-911a-f923d2beb460</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>d3133035-154c-4aab-a30d-0fb45f81477a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_PlayerMenu_5</value>
      <webElementGuid>280b3da9-71da-4b28-a9d6-1b7d8c3b2299</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>5f330b59-a76c-46ac-9d98-80ccf67b67ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>popupactive</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>75a00854-37f2-4adb-ab5e-8a539776ce63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ef2a5a33-5dde-4731-8da3-5c89dfd4e618</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>dijit_Menu_16</value>
      <webElementGuid>13200b5b-2985-4ae0-bafd-ef439eddbc06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>38163f87-7f03-4719-aa95-42a1b1609d2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_PlayerMenu_5&quot;)</value>
      <webElementGuid>fa887f78-d7f2-47e4-bb31-d6bb564ec40c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>e8159572-ef8d-4d12-ad72-fce6b023d9b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_player_PlayerMenu_5']</value>
      <webElementGuid>1b28870f-f030-4a7e-8a95-a4f34f00bcee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_4']/div/div[5]/div/button</value>
      <webElementGuid>2d08db59-0e90-4e7b-8a16-e8a894132106</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Get more help'])[1]/following::button[2]</value>
      <webElementGuid>4473408f-f11c-466b-a213-32ab207b03ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='REMAINING:'])[1]/following::button[3]</value>
      <webElementGuid>be6887fd-250c-48bf-8ae6-61d430a84ff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/preceding::button[1]</value>
      <webElementGuid>5dc8d5c2-b100-4547-bcf7-249b4399c717</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skill builder'])[1]/preceding::button[1]</value>
      <webElementGuid>e6a2ad3f-2970-46b8-ab26-90f298edf724</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>1f2456b7-2dc1-44b6-931c-60aad1b88f93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'xl_player_PlayerMenu_5' and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>a6c4faf4-1c22-405f-aa31-572549768450</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
