<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_pop-up content starts                  _e79979</name>
   <tag></tag>
   <elementGuidId>8c34b31a-0866-4d6f-bb21-2fdabad23d81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'xl_player_dialogs_ConfirmationDialog')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_dialogs_ConfirmationDialog_0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bcc84516-1cf2-4e64-afa0-d049ba45b289</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlDialog xlConfirmationDialog dijitDialogFixed dijitDialog dijitDialogHover dijitHover dijitDialogFocused dijitDialogHoverFocused dijitHoverFocused dijitFocused</value>
      <webElementGuid>eaf88f4f-b3a4-4c2e-8b89-332493d99933</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alertdialog</value>
      <webElementGuid>602cb5dc-c1aa-49dd-b842-18badc35f0b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>dialogNode</value>
      <webElementGuid>13ac1ae1-a617-447c-8e51-1c680adce250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-modal</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>dab360a5-8929-4623-b562-02326834e4c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>be0d0558-b194-4f3e-a512-8b14fb7fc104</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>xl_player_dialogs_ConfirmationDialog_0_titleNode</value>
      <webElementGuid>bd6b1bf9-b918-4607-86c8-0c3c36f8fb69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_dialogs_ConfirmationDialog_0</value>
      <webElementGuid>1dc42f61-906a-4369-9305-0b96715ae590</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_player_dialogs_ConfirmationDialog_0</value>
      <webElementGuid>4445df38-e226-487b-8c12-886c5b9bee9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>      
     
        pop-up content starts      
        
            Confirmation
            
                
            
            
                
            
        
        

        
        
            YOU HAVE NOT COMPLETED YOUR ASSIGNMENT!You still have 15 unanswered questions. 

Are you sure you want to submit your assignment?
        
        pop-up content ends
        
            
                
            
            
                
            CancelSubmit test
        
    
</value>
      <webElementGuid>71e4c56e-424c-437c-adc9-3a9f9774b39f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_dialogs_ConfirmationDialog_0&quot;)</value>
      <webElementGuid>0e002c93-7933-4823-8d34-3e46fcf0b178</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40</value>
      <webElementGuid>501e0968-5fa4-44fb-85cf-7b2f29ece287</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_ConfirmationDialog_0']</value>
      <webElementGuid>b2a03578-635e-4fca-9c76-5a60cc43a3e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'xl_player_dialogs_ConfirmationDialog_0', '&quot;', ')')])[1]/following::div[1]</value>
      <webElementGuid>5dbb2d94-3aa7-4810-934b-c701794fb610</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please wait...'])[1]/following::div[3]</value>
      <webElementGuid>5f6d4d74-8a22-42bd-a190-96f877e7115e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]</value>
      <webElementGuid>041bd933-5711-4ad3-acb9-30fb0b63728c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'xl_player_dialogs_ConfirmationDialog_0' and (text() = '      
     
        pop-up content starts      
        
            Confirmation
            
                
            
            
                
            
        
        

        
        
            YOU HAVE NOT COMPLETED YOUR ASSIGNMENT!You still have 15 unanswered questions. 

Are you sure you want to submit your assignment?
        
        pop-up content ends
        
            
                
            
            
                
            CancelSubmit test
        
    
' or . = '      
     
        pop-up content starts      
        
            Confirmation
            
                
            
            
                
            
        
        

        
        
            YOU HAVE NOT COMPLETED YOUR ASSIGNMENT!You still have 15 unanswered questions. 

Are you sure you want to submit your assignment?
        
        pop-up content ends
        
            
                
            
            
                
            CancelSubmit test
        
    
')]</value>
      <webElementGuid>397fcfcb-131c-44e1-8742-95d56521afb4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
