<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Homework and Tests</name>
   <tag></tag>
   <elementGuidId>1d809f34-f5c3-4b9e-95a0-39a1fb1a05a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Homework and Tests')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.menu-item-selected > a.navA</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>591cb3f3-3311-4192-84b1-b062d2f80b6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1</value>
      <webElementGuid>bdcf93f6-9a4d-43e7-8410-9824c43a1145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>9a104e58-b8f6-47c7-9bb2-4f374b0e56e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navA</value>
      <webElementGuid>adc905d2-42b6-405d-8f7c-ef14eedf4b74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>storeNavData(&quot;10&quot;)</value>
      <webElementGuid>c487a3d6-6f77-487c-b13a-4310162ea5af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>cfe090c0-e872-4570-832c-04edb6146f8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Homework and Tests</value>
      <webElementGuid>00400d7b-d161-496f-9dec-acc07402fd43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainLeftNav&quot;)/div[@class=&quot;menu-item-selected&quot;]/a[@class=&quot;navA&quot;]</value>
      <webElementGuid>92bf7033-0c2d-4970-80d5-f885f20f2d0c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='storeNavData(&quot;10&quot;)']</value>
      <webElementGuid>3ba36121-50ac-405d-b29f-2ec122ed3e0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainLeftNav']/div[4]/a</value>
      <webElementGuid>8a01baf6-875f-4f71-b216-61ca10a13acf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Homework and Tests')]</value>
      <webElementGuid>66c529b5-d3ed-431c-9b78-547595578bad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Calendar'])[1]/following::a[1]</value>
      <webElementGuid>28923c2a-14c0-4bdb-9ac0-5c75388ee857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course Home'])[1]/following::a[2]</value>
      <webElementGuid>27e8f2ed-5831-4094-8897-00d9c1773f84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Results'])[1]/preceding::a[1]</value>
      <webElementGuid>4484dcf2-d0fc-46b3-9891-88e944556e91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Study Plan'])[1]/preceding::a[2]</value>
      <webElementGuid>c088e1f0-2181-4f6b-9786-dea0f2bc009d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Homework and Tests']/parent::*</value>
      <webElementGuid>dbe96309-bd24-46f6-a558-179670a05e40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1')]</value>
      <webElementGuid>bfd7aff5-b743-4f61-b807-9cb570d4b5fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a</value>
      <webElementGuid>6f462ff6-b549-4173-8cfe-0bfefd3455b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/Student/DoAssignments.aspx?type=-1&amp;chapterId=-1' and @title = 'Homework and Tests' and (text() = 'Homework and Tests' or . = 'Homework and Tests')]</value>
      <webElementGuid>a56bdaab-edb8-4f2e-a70f-a5d2d10c3ace</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
