<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Excellent                              _7fe3c1</name>
   <tag></tag>
   <elementGuidId>9cd1e32d-7ab9-44ea-b669-f72cf8171231</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'xl_player_dialogs_FeedbackMessage')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_dialogs_FeedbackMessage_1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b3ffa0a7-b404-4128-a63b-c59f41083a09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>feedbackDialog xlFocus correct dijitDialogFixed dijitDialog dijitDialogHover dijitHover dijitDialogFocused dijitDialogHoverFocused dijitHoverFocused dijitFocused</value>
      <webElementGuid>c8f04d28-c1e3-4c83-a063-8678bb085c13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>5591a70c-38e3-4793-80d0-b861a2c334a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>dialogNode</value>
      <webElementGuid>6228c382-0d99-4379-b58d-ac4e2e1da39b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_1_title</value>
      <webElementGuid>e54cda38-1c78-47f1-9baa-f7274ee74438</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_1_message</value>
      <webElementGuid>3392b7b3-9e97-4959-8d53-5f09fb328fd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>893d4d87-ba6b-4327-ac7f-664e197a18a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_1</value>
      <webElementGuid>c715a620-d8b2-4b49-9bf6-49fd3901d0b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_1</value>
      <webElementGuid>6a193b40-7830-43db-88f8-29729db64c00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
        Excellent!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
</value>
      <webElementGuid>127aae75-fc6d-4517-803d-ad42a08cbdbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_dialogs_FeedbackMessage_1&quot;)</value>
      <webElementGuid>8fb844b1-430d-459d-b9d1-ff2c3df3bc0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>d63872f8-64c9-46bb-897b-02199bd399cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_FeedbackMessage_1']</value>
      <webElementGuid>cbd94388-9b10-4930-883f-70f27218494e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[24]/following::div[1]</value>
      <webElementGuid>fa23177c-2357-4d73-9a4a-9f920e9c9872</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download ipx'])[1]/following::div[1]</value>
      <webElementGuid>457fdc78-3721-4b4c-9f91-6161a1f94260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]</value>
      <webElementGuid>99932745-aabb-4457-bb88-1be748b59c51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'xl_player_dialogs_FeedbackMessage_1' and (text() = '
    
        
            
        
    
    
        
        Excellent!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
' or . = '
    
        
            
        
    
    
        
        Excellent!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
')]</value>
      <webElementGuid>eddf64a6-5378-4993-bb3a-4b27ca0ce35f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
