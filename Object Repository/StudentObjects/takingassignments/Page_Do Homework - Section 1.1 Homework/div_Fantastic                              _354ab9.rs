<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fantastic                              _354ab9</name>
   <tag></tag>
   <elementGuidId>8a2b8a26-c22b-48ce-bc30-b401e7343e61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[starts-with(@id,'xl_player_dialogs_FeedbackMessage')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#xl_player_dialogs_FeedbackMessage_0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>feda0b99-29ce-4267-bc93-c0f635bbc8d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>feedbackDialog xlFocus correct dijitDialogFixed dijitDialog dijitDialogHover dijitHover dijitDialogFocused dijitDialogHoverFocused dijitHoverFocused dijitFocused</value>
      <webElementGuid>a9a4870c-3e15-4f36-85af-d5aae2576c31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
      <webElementGuid>61536a0b-c501-45e5-8c92-19220b957b0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>dialogNode</value>
      <webElementGuid>39dc06e4-ceb7-435c-a956-8c50f4703986</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0_title</value>
      <webElementGuid>7a4375d6-07c8-4b04-be89-618d72653273</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0_message</value>
      <webElementGuid>83e16b98-a873-418e-911e-a2bea5e81654</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>5e666939-dd59-4302-a6a7-1c6436009489</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0</value>
      <webElementGuid>90559c20-223b-41c1-bbde-b18e6519462d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_player_dialogs_FeedbackMessage_0</value>
      <webElementGuid>6990c511-1398-4503-8379-fdc0657f4ef4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
</value>
      <webElementGuid>041ca74e-97de-4053-af4f-983fc62174c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_dialogs_FeedbackMessage_0&quot;)</value>
      <webElementGuid>3dbd0cf7-181a-41c5-a245-7c455b275428</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>2acbae68-3b0c-457a-876d-54a1804151ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_FeedbackMessage_0']</value>
      <webElementGuid>29f43256-8018-4da6-ad4e-319934e51c48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[25]/following::div[1]</value>
      <webElementGuid>9317652a-b938-41f9-b6a6-80a9be0a2577</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download ipx'])[1]/following::div[1]</value>
      <webElementGuid>e7db77ac-7525-423a-b272-df36f8174256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]</value>
      <webElementGuid>224c0f9d-1458-4feb-9833-1ed752693202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'xl_player_dialogs_FeedbackMessage_0' and (text() = '
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
' or . = '
    
        
            
        
    
    
        
        Fantastic!
    
    
        
            
        
    
    
        
            Similar Exercise
        
        
            GS
        
        
            SP
        
        Next question
        OK
        
            OK
        
    
')]</value>
      <webElementGuid>6f157127-740f-406b-90c0-9a82912003c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
