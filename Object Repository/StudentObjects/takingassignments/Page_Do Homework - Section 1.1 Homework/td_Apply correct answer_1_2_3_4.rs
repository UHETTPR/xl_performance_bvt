<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Apply correct answer_1_2_3_4</name>
   <tag></tag>
   <elementGuidId>ecac3594-a36b-42ef-ad7d-3e1cad497857</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem') and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_124_text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>6d24916b-9241-456c-8159-faf4dbc4d4ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>32bd3c4e-fd01-43c2-9411-80e9d703df70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>c34ea5d8-0658-418b-9008-2fa8540fa91e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>fc8ecb7e-204f-4965-b77a-38d4b726bfc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_124_text</value>
      <webElementGuid>f95b8184-d4cb-4f14-9645-757f1630f8c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply correct answer</value>
      <webElementGuid>aa7881d2-ad93-4501-babd-8b171c6a7e79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_124_text&quot;)</value>
      <webElementGuid>c7174f26-214d-46a3-ba55-d083b1b8f8ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>ab777e03-e8af-424f-8498-d7afaada1a1d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_124_text']</value>
      <webElementGuid>a5ac3b54-b773-4ab1-9baf-a332f8106015</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_124']/td[2]</value>
      <webElementGuid>b2660287-dc57-4392-80de-128a74780241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show alt text'])[1]/following::td[4]</value>
      <webElementGuid>0be3b61c-1699-4789-be1b-6834b13a35f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[1]/following::td[5]</value>
      <webElementGuid>b9ca77d9-4567-49b6-9fe5-cdaa54d3be2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[12]/preceding::td[2]</value>
      <webElementGuid>480443b9-f10e-4d70-bee9-9a6fbb5721e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/preceding::td[3]</value>
      <webElementGuid>c33168b8-8f04-43e4-a835-e750a41bdadf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply correct answer']/parent::*</value>
      <webElementGuid>4c5fe2a2-624d-46f7-bc88-6da41028169c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/table/tbody/tr[5]/td[2]</value>
      <webElementGuid>384edcb5-103a-4062-9b40-8e7de7e9a58f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_124_text' and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      <webElementGuid>19212702-2c8d-4c30-bf75-b1a29acd362b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
