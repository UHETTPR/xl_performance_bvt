<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Chapter 2-A</name>
   <tag></tag>
   <elementGuidId>3156ec6f-e27a-409e-b47e-d0c9753d88c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Chapter 2-A')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>tr.alt > th.assignmentNameColumn > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3907d126-27a8-403f-b446-74af3b1b9b5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doTest(77066894, false);</value>
      <webElementGuid>e59ec4ec-9048-4d93-bad7-6c3a0977c624</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Chapter 2-A</value>
      <webElementGuid>773a2e00-df6b-4872-b744-5a59841dbd02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[@class=&quot;alt&quot;]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>4df18f5b-1be1-449f-ab45-af1ffa0b9fb5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[2]/th/a</value>
      <webElementGuid>20eaef7b-dbca-434f-ac82-87d5b0e85548</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Chapter 2-A')]</value>
      <webElementGuid>0ad84a15-1f71-4146-8a0a-1f34e2a8297d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/following::a[1]</value>
      <webElementGuid>d9f3b7fc-e850-46cf-bfdc-d2bb67dbadfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[3]/preceding::a[1]</value>
      <webElementGuid>85add341-03f0-41a6-9fbe-b4d18d108f2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chapter 2-A']/parent::*</value>
      <webElementGuid>f1a5cf7c-66eb-4903-b8b9-486559e63853</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doTest(77066894, false);')]</value>
      <webElementGuid>c84d17a0-70fb-4c61-af0a-46cf1ac3a453</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/th/a</value>
      <webElementGuid>9579c5b6-29dc-4e4e-85a6-c181e945cc3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doTest(77066894, false);' and (text() = 'Chapter 2-A' or . = 'Chapter 2-A')]</value>
      <webElementGuid>b92b945e-df0c-4196-8aac-e774d421ea86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
