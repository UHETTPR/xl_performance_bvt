<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Quizzes  Tests</name>
   <tag></tag>
   <elementGuidId>9fd62086-b670-4844-aecd-e28319ea224c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='ul_All Assignments']/li[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8a3d6da9-e2f6-4292-a67d-e146ae0b4b1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-menu-id</name>
      <type>Main</type>
      <value>All_Assignments</value>
      <webElementGuid>8cedfd91-5c8f-4e68-809e-0dfffcdea907</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')</value>
      <webElementGuid>310ab87e-133a-4a44-a87a-92e580d1b722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quizzes &amp; Tests</value>
      <webElementGuid>29f6dbf6-d499-4c20-89e2-d71dc8b17806</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ul_All Assignments&quot;)/li[3]/a[1]</value>
      <webElementGuid>1a28ee25-5d06-4b82-beb5-f0d75445a293</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ul_All Assignments']/li[3]/a</value>
      <webElementGuid>853288a6-f665-43cd-a9db-61f6632dab58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Quizzes &amp; Tests')]</value>
      <webElementGuid>7ac0ed17-ce64-4a0a-ac8c-9770a493abae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/following::a[1]</value>
      <webElementGuid>9a1bbe2c-7532-4bff-8c7d-db53eb554013</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/following::a[2]</value>
      <webElementGuid>e5987b09-14bd-4791-921f-f6c59ebdaa7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[1]/preceding::a[1]</value>
      <webElementGuid>9d086219-a121-40c4-9cfe-21c4ad0c3569</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[2]/preceding::a[1]</value>
      <webElementGuid>424b843c-2590-4672-838e-c1d2fa0e37dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Quizzes &amp; Tests']/parent::*</value>
      <webElementGuid>d1eef908-992f-47dc-b858-272e5b5ba5ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')&quot;)]</value>
      <webElementGuid>02d2fa32-3363-4dec-bd61-f459ca9bf8c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/ul/li[3]/a</value>
      <webElementGuid>0eae905d-a824-4e61-a2e9-43ba98dd94b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Quizzes &amp; Tests' or . = 'Quizzes &amp; Tests')]</value>
      <webElementGuid>3ff2baa2-60b8-44a7-b22d-3d5cf102c283</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
