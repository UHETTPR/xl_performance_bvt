<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 1.1 Homework</name>
   <tag></tag>
   <elementGuidId>8b6b45bd-4442-47bf-900a-54d66c8c356e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 1.1 Homework')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>69091003-0653-4972-b79c-3222512ba9ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(205399931, false, true);</value>
      <webElementGuid>48e592a3-6e2b-44e8-92a7-3617510feffd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 1.1 Homework</value>
      <webElementGuid>d5333f77-02e2-45b3-aead-b6731c5ba36f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[3]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>2bac9c24-f907-4473-a557-70290f3bf577</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[3]/th/a</value>
      <webElementGuid>6080779b-36ad-482b-9ae2-38a3dcb70c20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 1.1 Homework')]</value>
      <webElementGuid>3edbee2e-1d24-4fc8-9eff-3ef1b8c6a0e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[5]/following::a[1]</value>
      <webElementGuid>9237b4a2-e9d8-4277-8e64-89d419b511a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HWSkillBuilder_base'])[1]/following::a[1]</value>
      <webElementGuid>d1551ae9-d650-45f3-b899-103ff2f0e7d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[6]/preceding::a[1]</value>
      <webElementGuid>5c31dd20-f1ae-4d0b-82b2-81bbb5941300</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section 1.4 Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>248ae5b0-09fa-422b-aa74-85770463cce4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 1.1 Homework']/parent::*</value>
      <webElementGuid>4e3e69b5-4e42-490f-87d3-c7b4be5829c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(205399931, false, true);')]</value>
      <webElementGuid>659fecda-1cd9-493d-9ace-7c52764685db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/th/a</value>
      <webElementGuid>2c4aa477-fab4-487d-8aff-9523e2a77221</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(205399931, false, true);' and (text() = 'Section 1.1 Homework' or . = 'Section 1.1 Homework')]</value>
      <webElementGuid>289c9846-344c-45a2-83ca-931cd1910bdc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
