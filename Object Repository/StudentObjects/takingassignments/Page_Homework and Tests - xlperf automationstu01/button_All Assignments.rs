<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_All Assignments</name>
   <tag></tag>
   <elementGuidId>89ceb97c-5e9d-447a-8b02-88ab712d95c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#All_Assignments</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='All_Assignments']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6108c451-79c9-4bfe-9678-f0081fa1d633</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>All_Assignments</value>
      <webElementGuid>2165a6a2-20d9-4773-8658-8e9e86692bd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn btn-default dropdown-toggle dropdown-toggle btn-xl-toggle</value>
      <webElementGuid>85cb12ac-3300-4a27-bd1b-e876066dfbdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9e3d5175-1f9d-4a86-a04b-1b467ae31780</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>e10cd743-2b12-4a90-99f1-acf689b9dabb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-active-css</name>
      <type>Main</type>
      <value>btn-default</value>
      <webElementGuid>9673d0c0-04e3-4ab9-bc34-465fe9b86626</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5e5a253f-0280-477d-98b9-bb1d01333795</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>380d6166-a0d5-460e-9a98-a5d971500b03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>ul_All Assignments</value>
      <webElementGuid>bc57cd45-ee4e-402e-8198-9d94e53bd347</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> All Assignments </value>
      <webElementGuid>512d74a5-c728-4f48-b945-9515db7fa482</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;All_Assignments&quot;)</value>
      <webElementGuid>5e1fc0ea-b695-4b5d-97d1-2a7166b92a06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='All_Assignments']</value>
      <webElementGuid>b85bf532-b616-4707-b97f-eda94713fe00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FilterButtons']/div/div/button</value>
      <webElementGuid>38d5eeb6-4742-4af0-af43-058aacf4305a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Icons &amp; Conventions'])[1]/following::button[1]</value>
      <webElementGuid>1b9463d5-97c8-4c64-9691-13bd5f713131</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Learn about Homework and Tests'])[1]/following::button[1]</value>
      <webElementGuid>1d4543dd-8693-4154-9b11-517d10229544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/preceding::button[1]</value>
      <webElementGuid>0945df0a-53ef-4e33-82fa-f888c8e4c71c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>33a8f1e7-6744-48d9-b4ea-7617e82d9cc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'All_Assignments' and @type = 'button' and (text() = ' All Assignments ' or . = ' All Assignments ')]</value>
      <webElementGuid>1a9e6c09-cb8f-4a4c-974e-6c4a14f60e9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
