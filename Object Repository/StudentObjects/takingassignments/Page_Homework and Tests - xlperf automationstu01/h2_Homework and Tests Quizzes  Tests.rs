<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Homework and Tests Quizzes  Tests</name>
   <tag></tag>
   <elementGuidId>e68deb1d-379c-442e-9210-3b01adbf97af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Homework and Tests: Quizzes &amp; Tests' or . = '
            Homework and Tests: Quizzes &amp; Tests')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#PageHeaderTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>fdb7b3a9-6e92-4a62-9774-c80c09b48b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>PageHeaderTitle</value>
      <webElementGuid>ff251cf3-bed2-47b2-9e82-6de793218a3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>integrationheaderstyle PageHeaderTitle header-component-float-left header-font-size</value>
      <webElementGuid>12aa0e4b-1eb1-4f48-98a0-0d53bcfe6a81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Homework and Tests: Quizzes &amp; Tests</value>
      <webElementGuid>ec31591f-5545-4fa9-92e4-b51db5968334</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;PageHeaderTitle&quot;)</value>
      <webElementGuid>efd7f4ee-1c85-4bb0-8e37-6b3cabeb2fc4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h2[@id='PageHeaderTitle']</value>
      <webElementGuid>f4b1d838-cb25-4309-8b79-68518a28f2ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ctl00_ctl00_InsideForm_MasterContent_PageHeader1_HeaderTitleH2Div']/h2</value>
      <webElementGuid>a7c935af-43cc-46ce-a947-e58ed95a5495</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL Load Testbook'])[1]/following::h2[1]</value>
      <webElementGuid>dc991a8c-d7cc-4626-b04d-df2cbe226c9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='xlperf automationins01'])[1]/following::h2[1]</value>
      <webElementGuid>28892f65-407e-4876-802d-da1158cd27a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print this page'])[1]/preceding::h2[1]</value>
      <webElementGuid>037c67e6-d412-4b8e-83e3-f3ac7fefbb7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[3]/preceding::h2[1]</value>
      <webElementGuid>bff0867e-089a-442c-b5e3-c9c0fe66ad43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Homework and Tests: Quizzes &amp; Tests']/parent::*</value>
      <webElementGuid>1bf18ed9-d3ae-447a-bba5-fb8ed4f798d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>683dada6-c20c-4ec2-ad7a-8b619b3d8c48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[@id = 'PageHeaderTitle' and (text() = '
            Homework and Tests: Quizzes &amp; Tests' or . = '
            Homework and Tests: Quizzes &amp; Tests')]</value>
      <webElementGuid>fa55d172-2c93-4108-b9d6-226d77098200</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
