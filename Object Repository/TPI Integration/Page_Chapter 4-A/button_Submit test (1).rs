<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit test (1)</name>
   <tag></tag>
   <elementGuidId>d9c5b2cd-3c29-463e-9da3-2840a570938e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_dijit-bootstrap_Button') and (text() = 'Submit test' or . = 'Submit test')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c583e101-bfe4-428d-96da-6d7151459685</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f9354b13-296e-420d-adcc-f1c0008ed059</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>6e0e37a6-0fd8-45bb-9c58-95e3e825995d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>af7bdae2-3c5e-4855-b8c6-52d440719e2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>0eecfdaa-3614-4a54-aee8-1cb57c39fd83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_7</value>
      <webElementGuid>9f16fd3f-8d8c-4c38-a3dd-ca97a11f04ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit test</value>
      <webElementGuid>93df21e8-a45c-4dac-9679-6d7ae3396d64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_7&quot;)</value>
      <webElementGuid>31e40377-24df-4fa9-ab60-86af08dbd74b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Chapter 4-A/iframe_Opens in a new window_ctl00_ctl00_In_5f7b40</value>
      <webElementGuid>681e2cdf-b93c-4e1f-81aa-6e55965ce6c2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_7']</value>
      <webElementGuid>c74f8790-9963-4f8a-8078-274be8062057</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_ConfirmationDialog_0']/div/div[4]/div[2]/button[2]</value>
      <webElementGuid>5c8f816f-76e7-4239-bac3-8cbdb827acc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>d1697d0b-f56f-4952-ae1b-9f9b3070ddbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='pop-up content ends'])[1]/following::button[2]</value>
      <webElementGuid>a84c294f-32d6-4914-8ca5-112e032a6c34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Submit test']/parent::*</value>
      <webElementGuid>a1f19452-a5d3-4bff-8c36-55a9c27b550c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button[2]</value>
      <webElementGuid>3afb5295-466d-4e2b-8082-c74d459e9d50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_7' and (text() = 'Submit test' or . = 'Submit test')]</value>
      <webElementGuid>99ff3f48-43b1-4ac6-a91b-6202260c2296</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
