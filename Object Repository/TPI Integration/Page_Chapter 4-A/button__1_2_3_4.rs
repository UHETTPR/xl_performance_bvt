<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1_2_3_4</name>
   <tag></tag>
   <elementGuidId>9fa022c2-15a7-4977-8d4c-4fcb3db78666</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_player_PlayerMenu_5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_player_PlayerMenu') and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2b8d6542-f0b2-4c90-8392-c2213ce8b415</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>icon, _buttonNode, focusNode</value>
      <webElementGuid>099e9420-4ec0-4593-a53c-9f2db5ee8556</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlPlayerMenuIcon dijitDownArrowButton dijitHasDropDownOpen</value>
      <webElementGuid>052c84cd-2230-491f-81bb-cdec9ec37ecd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>f071bf0f-7ea6-4df7-b7c4-9624169c9640</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d70db93c-a52c-4d13-a5b6-6f38a894b21e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>f8c4ab2f-e693-4dd4-87a9-bf5f932cce6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>34a830c5-e3c9-479f-aed0-7aabf87163f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_PlayerMenu_5</value>
      <webElementGuid>ba3238c9-403c-4787-9d17-7863aa771d5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>245fd9c6-98be-4ad3-9d9e-c3d81266295f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>popupactive</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a03f2427-0b72-4b07-bc48-b0df1743d923</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>721006bc-7ccc-4e76-b00f-e8edcad2917f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>dijit_Menu_16</value>
      <webElementGuid>e7e6be0f-a92f-4884-85e8-62ebe512e0f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>df522340-2857-4c3c-bf60-d0751fbdbd0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_PlayerMenu_5&quot;)</value>
      <webElementGuid>cffb6cfb-6af4-44b6-8982-155676733ae9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Chapter 4-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>c333fc68-3ba1-4325-82d6-e2d8ca9836c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_player_PlayerMenu_5']</value>
      <webElementGuid>47bb77f5-85dd-4226-a73d-bb99fce4a66e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_4']/div/div[5]/div/button</value>
      <webElementGuid>abcb3071-60de-475d-8e7e-7bda079d34da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Get more help'])[1]/following::button[2]</value>
      <webElementGuid>acc1d1a7-a2fa-4c56-afa3-ba63d5a8c493</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='REMAINING:'])[1]/following::button[3]</value>
      <webElementGuid>7256ba88-26d7-4f2b-9e0a-6e7c5d5a2594</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/preceding::button[1]</value>
      <webElementGuid>a9c13aad-efc1-420b-8fa0-84ac4c439bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skill builder'])[1]/preceding::button[1]</value>
      <webElementGuid>e70ae9b5-e0d5-4976-828a-01ac3ef24097</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>6b1a194c-ac3f-49c7-87e3-c602a7901fcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'xl_player_PlayerMenu_5' and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>84dafadf-0732-4ca9-ba65-f3a235af7db9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
