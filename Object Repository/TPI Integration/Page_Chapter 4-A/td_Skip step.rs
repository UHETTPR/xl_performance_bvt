<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Skip step</name>
   <tag></tag>
   <elementGuidId>1bf77dec-109b-4123-ba2a-b5ab714346b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_60_text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem') and (text() = 'Skip step' or . = 'Skip step')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>58e29fc4-09e0-4a68-bdd1-c64adc01dd1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>1b0fa98f-6c9d-4bf7-b202-18f02cc4e3d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>d7b3d0e1-d2b0-4bcc-be22-4a26acd5fc07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>ad70562c-b75a-480d-9f76-8d711eb0c733</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_60_text</value>
      <webElementGuid>07eca265-156a-421d-805e-9fea6dad57e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip step</value>
      <webElementGuid>d5e11d8b-07c2-4c8d-9100-13386451ca86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_60_text&quot;)</value>
      <webElementGuid>967fbd1f-16e5-4419-82d5-46db92952f28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Chapter 4-A/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>7f5e1d08-927b-40ef-be70-d6eb402ca615</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_60_text']</value>
      <webElementGuid>32ee464d-df3e-4584-b7df-fb537636c0c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_60']/td[2]</value>
      <webElementGuid>94faa6c4-6e7f-4a11-9678-6a0d2f7187c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hold step'])[1]/following::td[4]</value>
      <webElementGuid>1049e432-41ce-45ad-bf14-ca2bd21062ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/following::td[5]</value>
      <webElementGuid>188ea7d2-0493-476c-a3b2-fbcf38a2900a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[8]/preceding::td[2]</value>
      <webElementGuid>c1fe2a8b-bea1-4dc9-aceb-e4b4a6b09d79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to end'])[1]/preceding::td[4]</value>
      <webElementGuid>32b14664-7afb-422e-b5c2-989cd9307a4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip step']/parent::*</value>
      <webElementGuid>6a02a818-f50e-44f5-8fde-b207b5f87b86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[7]/td[2]</value>
      <webElementGuid>56240ab9-9bb0-44f6-9214-6c1cc43cff92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_60_text' and (text() = 'Skip step' or . = 'Skip step')]</value>
      <webElementGuid>35a48f40-c132-42ed-988c-2699f3f251e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
