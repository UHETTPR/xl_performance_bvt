<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 1, 1.2.1</name>
   <tag></tag>
   <elementGuidId>d15bfc57-716a-4bbd-9e8f-0f6bc8f83800</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanel</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7e9bfd64-0eed-4bde-80f7-798601a541c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>a66e5c97-eca3-44c5-a59d-4cf64e6e3d92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>nextPrevPanel</value>
      <webElementGuid>5b1a1ae4-2fa9-4c5f-bf5e-9ae5c1685408</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                </value>
      <webElementGuid>86e1b20f-d68d-4f79-aed3-5dfe3debb757</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]</value>
      <webElementGuid>56ff120b-9331-488d-b8ad-243a86910b23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>c2ebdac5-14f3-4ff2-8982-d941d8a46cd1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div</value>
      <webElementGuid>1fe7b848-38a9-4419-9995-b834828199ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework:'])[1]/following::div[4]</value>
      <webElementGuid>de040e0f-595a-4991-9e70-ca3d4cf1add1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HW Score:'])[1]/preceding::div[5]</value>
      <webElementGuid>24ae1a21-9c57-49fb-ae22-fd5508af0271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div</value>
      <webElementGuid>38597975-a502-40e1-870e-0066ee55f83e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ' or . = '
                    
                    
                        
                            Question 1, 1.2.1
                        
                        
                    
                    
                ')]</value>
      <webElementGuid>86d6bdd6-479c-4fb4-99ac-08bc472c08ae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
