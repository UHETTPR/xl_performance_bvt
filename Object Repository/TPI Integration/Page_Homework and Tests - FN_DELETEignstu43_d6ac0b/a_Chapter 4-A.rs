<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Chapter 4-A</name>
   <tag></tag>
   <elementGuidId>f86241ca-fcc3-4371-890a-116611933228</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Chapter 4-A')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>42aea49b-46cd-4d6d-b443-8da65aa460a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doTest(76615995, false);</value>
      <webElementGuid>a61b3e42-7f08-4f85-97f0-0f2ccbbb95eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Chapter 4-A</value>
      <webElementGuid>82f54915-d26d-4073-98cf-88d3f5d97a1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[3]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>140662e7-362e-4ee3-a0ca-ec5b9855dc6c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[3]/th/a</value>
      <webElementGuid>cd933820-2b91-41a9-a478-f349e601a005</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Chapter 4-A')]</value>
      <webElementGuid>52b6e579-3685-4fe9-9332-b5e36bf82c3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[3]/following::a[1]</value>
      <webElementGuid>bf8cee2b-593b-43a9-90da-e1809bd11e01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[4]/preceding::a[1]</value>
      <webElementGuid>fcae7d4e-3158-4dd4-af1b-bc014bfd4220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chapter 4-A']/parent::*</value>
      <webElementGuid>384e31d1-41c7-48fc-9f3f-cb73d6daea40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doTest(76615995, false);')]</value>
      <webElementGuid>e5aeace4-43f2-4b6c-be2b-db30fce7e531</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/th/a</value>
      <webElementGuid>2c7db9d3-41ee-4bd3-9879-5512b52ec426</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doTest(76615995, false);' and (text() = 'Chapter 4-A' or . = 'Chapter 4-A')]</value>
      <webElementGuid>bc25afae-cbbb-4109-9294-5d38f8db1ff2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
