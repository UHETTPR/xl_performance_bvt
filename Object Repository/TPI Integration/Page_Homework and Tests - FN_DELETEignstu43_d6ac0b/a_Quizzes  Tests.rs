<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Quizzes  Tests</name>
   <tag></tag>
   <elementGuidId>38814cc1-7c7c-4a74-982a-79bbb8b233e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Quizzes &amp; Tests')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>dfd5a993-33ae-4484-8ac9-080992663d2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-menu-id</name>
      <type>Main</type>
      <value>All_Assignments</value>
      <webElementGuid>f5505798-dd8f-4e4d-ba36-b45e2bb537fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')</value>
      <webElementGuid>eddf1efb-b8a6-45fa-910f-dbf164e404ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quizzes &amp; Tests</value>
      <webElementGuid>bcfbab0a-c600-4fe6-9f53-9b409967f439</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ul_All Assignments&quot;)/li[3]/a[1]</value>
      <webElementGuid>9fccb9fa-612c-46e4-a045-7e306f76f523</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='ul_All Assignments']/li[3]/a</value>
      <webElementGuid>9de8c3f7-016c-4e14-8fb2-12ddfbe84250</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Quizzes &amp; Tests')]</value>
      <webElementGuid>3eee346c-4e8f-4bcc-becb-1098cb1b1f49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[1]/following::a[1]</value>
      <webElementGuid>99c4f07c-7a61-4130-b5b4-53869f01c3d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Assignments'])[2]/following::a[2]</value>
      <webElementGuid>f6d44910-bb9a-4a4b-a5c6-1c5bdeab6322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[1]/preceding::a[1]</value>
      <webElementGuid>a2180d7d-993b-4635-95a5-7527da9bbd49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[2]/preceding::a[1]</value>
      <webElementGuid>178bb297-95f3-43ec-baa4-f0fa94f34367</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Quizzes &amp; Tests']/parent::*</value>
      <webElementGuid>66860f3e-99f0-4ce2-94d1-ade10d789e81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, &quot;javascript:__doPostBack('ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09','')&quot;)]</value>
      <webElementGuid>d117ce4f-ef55-44b0-b9b8-888d8dfb20c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div/div/ul/li[3]/a</value>
      <webElementGuid>d82fd51f-b503-4fc1-b5d4-62761cf80547</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = concat(&quot;javascript:__doPostBack(&quot; , &quot;'&quot; , &quot;ctl00$ctl00$InsideForm$MasterContent$filterControl$FilterButtons$ctl09&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;)&quot;) and (text() = 'Quizzes &amp; Tests' or . = 'Quizzes &amp; Tests')]</value>
      <webElementGuid>77e6f125-1cc4-4b40-8e27-53063b7d9dde</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
