<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Section 2.5 Homework</name>
   <tag></tag>
   <elementGuidId>ff680f93-c977-4b46-bd15-9151bd66b5af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Section 2.5 Homework')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d7b394d4-b33e-41c0-86e0-3584859fe46a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>javascript:doHomework(204456812, false, true);</value>
      <webElementGuid>e4af6287-d8da-49b0-b449-61bf1d923da8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Section 2.5 Homework</value>
      <webElementGuid>6488f112-01e6-4213-9676-64e46bf7728a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_gridAssignments&quot;)/tbody[1]/tr[@class=&quot;alt&quot;]/th[@class=&quot;assignmentNameColumn&quot;]/a[1]</value>
      <webElementGuid>2baad086-244a-4bea-a3fa-9180546271c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctl00_InsideForm_MasterContent_gridAssignments']/tbody/tr[4]/th/a</value>
      <webElementGuid>ca1ede53-c018-49b3-9a91-36a08f56945e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Section 2.5 Homework')]</value>
      <webElementGuid>e9e748b2-9f89-4ff9-ac87-4626df1486f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[5]/following::a[1]</value>
      <webElementGuid>63f74d87-2e56-4593-8bcc-1ffafb88a359</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section 2.1 Homework'])[1]/following::a[1]</value>
      <webElementGuid>3899df01-6105-4129-874b-e7557f5c979d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Homework'])[6]/preceding::a[1]</value>
      <webElementGuid>587b9563-7c14-4533-9bc9-3441abc97a30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section 3.1 Homework'])[1]/preceding::a[1]</value>
      <webElementGuid>eafd82ab-8352-4467-82b9-495602e56855</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Section 2.5 Homework']/parent::*</value>
      <webElementGuid>a81bf423-0448-46d5-9f98-4d981d24c97a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'javascript:doHomework(204456812, false, true);')]</value>
      <webElementGuid>b161d89e-f0b6-41b8-9b5d-e83aa4dd1583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/th/a</value>
      <webElementGuid>f594b27f-3837-4f70-a771-af56d37bd3e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'javascript:doHomework(204456812, false, true);' and (text() = 'Section 2.5 Homework' or . = 'Section 2.5 Homework')]</value>
      <webElementGuid>a7569209-beb4-4164-8151-b00f81659e53</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
