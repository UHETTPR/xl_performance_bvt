<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_Test Chapter 4-A</name>
   <tag></tag>
   <elementGuidId>8b8bead7-2f3d-47f4-a242-311ef3fea302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[(text() = 'Test Chapter 4-A' or . = 'Test Chapter 4-A')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>578303e4-f147-421b-ae26-e9c1235b8f6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>leftcol fullwidth </value>
      <webElementGuid>300bf13e-50d9-4442-abb3-af133cc8144c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scope</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>419702aa-5450-4782-a8f3-488d6f636bc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4cbfe386-486f-46fb-a2dd-79ccf5fa7439</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test Chapter 4-A</value>
      <webElementGuid>708be88b-38d4-4555-90d7-5c065a9b7e9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;row76615995&quot;)/th[@class=&quot;leftcol fullwidth&quot;]</value>
      <webElementGuid>378c3bfe-51e0-4ca1-914e-6a8dda64ce8c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='row76615995']/th</value>
      <webElementGuid>d2f85f3f-5bdc-432c-a4d5-ff7f9b910d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Worked'])[1]/following::th[1]</value>
      <webElementGuid>2781f6ff-9b73-46b5-8d24-8fc232279c2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Started'])[1]/following::th[2]</value>
      <webElementGuid>8d52e080-377d-40a1-b9aa-b524b5ef3321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Chapter 4-A']/parent::*</value>
      <webElementGuid>d42974c7-8b87-459c-83db-7e2b057faf41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/table/tbody/tr/th</value>
      <webElementGuid>bfb6fb25-d336-44ba-bd33-7f8b20fa7835</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = 'Test Chapter 4-A' or . = 'Test Chapter 4-A')]</value>
      <webElementGuid>641f52ea-cf5e-4f45-8a46-b53833b2953e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
