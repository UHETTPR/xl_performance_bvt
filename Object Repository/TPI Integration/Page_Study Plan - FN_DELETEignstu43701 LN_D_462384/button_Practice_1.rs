<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Practice_1</name>
   <tag></tag>
   <elementGuidId>2e721540-d47c-415c-bbb0-cf978fc5ff7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id = 'h2' and @title = 'Practice Vocabulary and Readiness Check' and (text() = ' Practice ' or . = ' Practice ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>109aef18-a44d-42e2-bf0b-9d00959f05e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>a12b393a-1c79-40fe-b805-ca0d184f555a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default practice</value>
      <webElementGuid>4b472fb2-14da-40e7-a9df-5e88d31d58e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>h2 h15069898757015136000</value>
      <webElementGuid>1c58387d-d693-40de-8aba-190f11575f23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Practice Vocabulary and Readiness Check</value>
      <webElementGuid>5a099c3a-9daf-4ecf-9365-6929bb595c16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Practice </value>
      <webElementGuid>e9c2d693-0442-4e22-9e2d-b9e837367437</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;studyPlanRecommendations&quot;)/div[1]/app-section-item[1]/section[@class=&quot;well objectives&quot;]/div[@class=&quot;collapse in&quot;]/div[@class=&quot;inner&quot;]/ul[@class=&quot;sections-list&quot;]/li[2]/ul[@class=&quot;objective-list&quot;]/li[1]/app-media-container[1]/div[@class=&quot;entry-container&quot;]/div[@class=&quot;actions&quot;]/button[@id=&quot;h2&quot;]</value>
      <webElementGuid>d5fb656e-75d0-403e-988e-8b5e9aab5ffa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@id='h2'])[2]</value>
      <webElementGuid>b2f913d8-2c87-49f8-bbc5-195866c47956</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='studyPlanRecommendations']/div/app-section-item/section/div/div/ul/li[2]/ul/li/app-media-container/div/div/button</value>
      <webElementGuid>e958e6b4-e06f-4f55-9952-393c4afe4226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommended Media:'])[3]/following::button[1]</value>
      <webElementGuid>b904d651-5314-496d-91a6-1c4ec99ded3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommended Media:'])[2]/following::button[1]</value>
      <webElementGuid>fda07727-1340-42a3-94e6-7fb56d4c0596</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quiz Me'])[2]/preceding::button[1]</value>
      <webElementGuid>4b33e1e6-9a77-4723-ad1b-d91e112bfe44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommended Media:'])[4]/preceding::button[2]</value>
      <webElementGuid>38e3dfaf-bb9d-4428-a58e-64d7fc9026f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li/app-media-container/div/div/button</value>
      <webElementGuid>752a82ac-84c0-47d3-82b0-062f6673f74e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'h2' and @title = 'Practice Vocabulary and Readiness Check' and (text() = ' Practice ' or . = ' Practice ')]</value>
      <webElementGuid>cf04f34b-4a8d-4d39-9c63-41a0f2c1e171</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
