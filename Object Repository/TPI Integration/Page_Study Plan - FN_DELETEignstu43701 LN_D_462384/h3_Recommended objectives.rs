<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Recommended objectives</name>
   <tag></tag>
   <elementGuidId>20e8732a-0f40-4c59-998c-5748431ada4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.additonal-ojbectives</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[(text() = ' Recommended objectives ' or . = ' Recommended objectives ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>79d8a4fa-a17f-447d-9a8d-328d6236f5cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>additonal-ojbectives</value>
      <webElementGuid>8ffb4c97-197c-4506-8898-ce33d4858d4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Recommended objectives </value>
      <webElementGuid>9302da0c-e5f9-4a15-8c20-32bf4082f9cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;studyPlanRecommendations&quot;)/div[1]/app-section-item[1]/section[@class=&quot;well objectives&quot;]/header[1]/h3[@class=&quot;additonal-ojbectives&quot;]</value>
      <webElementGuid>403a7d79-2fe7-4c00-91ba-ce9f6b72fea5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='studyPlanRecommendations']/div/app-section-item/section/header/h3</value>
      <webElementGuid>d3a63e42-a367-4cda-a7fb-d4f939125dc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Chapters'])[1]/following::h3[1]</value>
      <webElementGuid>a67071ff-0a1d-438e-9505-9931065a0a2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Progress'])[1]/following::h3[1]</value>
      <webElementGuid>42bfacd0-e6f7-4a9a-b35d-344dc8473098</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommended Media:'])[1]/preceding::h3[1]</value>
      <webElementGuid>0de26b89-ea1a-47d4-8da7-e20d4f6ec85c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Practice'])[1]/preceding::h3[1]</value>
      <webElementGuid>96eada30-72c4-40bb-91b4-e1f1be620b92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Recommended objectives']/parent::*</value>
      <webElementGuid>a98dc894-90e4-4d0a-a783-7d88d9e7859b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>5d35ed68-5112-45a4-a1b1-aebf7c1f3054</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = ' Recommended objectives ' or . = ' Recommended objectives ')]</value>
      <webElementGuid>d1435e4f-58b0-4655-8517-fc38c54474d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
