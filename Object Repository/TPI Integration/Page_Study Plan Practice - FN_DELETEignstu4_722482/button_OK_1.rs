<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_OK_1</name>
   <tag></tag>
   <elementGuidId>eca9180d-ffec-4d0c-b876-a7d9341a0c6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_dijit-bootstrap_Button_62</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='xl_dijit-bootstrap_Button_62']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>02ce8c76-cb00-4049-aa90-3d31c66c5471</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>btnOk</value>
      <webElementGuid>f44e4689-0479-4f12-828f-9edcf40aa06c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default btnOnly</value>
      <webElementGuid>f7e1f963-e780-4307-928a-d20f2787134d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-type</name>
      <type>Main</type>
      <value>xl/dijit-bootstrap/Button</value>
      <webElementGuid>46f1fbb9-d1fa-45be-a7e7-1d5e1576ae7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-props</name>
      <type>Main</type>
      <value>type:&quot;button&quot;</value>
      <webElementGuid>93a6a94d-91b6-42ed-8e41-ab94fb380989</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-event</name>
      <type>Main</type>
      <value>onClick:okClicked</value>
      <webElementGuid>fbc0803e-c3ac-44af-9d83-6b070f9dea12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>bf635583-1425-4ec3-ab81-28236eb59540</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e94e50e0-d6c7-42c8-8b99-eca92aa97389</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_62</value>
      <webElementGuid>6fad24b2-3160-4d4f-85d1-250b1553850d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>widgetid</name>
      <type>Main</type>
      <value>xl_dijit-bootstrap_Button_62</value>
      <webElementGuid>c6442bd7-6019-4ce2-9b01-e186f664b3d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>7d81e3ac-c6c6-44ec-bf3d-b97d565b0706</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_dijit-bootstrap_Button_62&quot;)</value>
      <webElementGuid>aaa7399a-fe7a-4fcb-8109-7a3935182e14</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>209e7a5d-8212-4c12-8084-86ff0e1dee6b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_dijit-bootstrap_Button_62']</value>
      <webElementGuid>767ff9f9-d80e-4cf2-b49f-05ab348e44d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_dialogs_FeedbackMessage_1_buttonBar']/button[5]</value>
      <webElementGuid>bde3e303-960b-4cc4-b8e0-9d6458a7a86a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Question'])[1]/following::button[1]</value>
      <webElementGuid>5998ea87-ed74-40aa-ba2e-2e02daf20353</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SP'])[1]/following::button[2]</value>
      <webElementGuid>971eda95-5aa3-4d04-8bb2-efb4e7ddab92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[2]/preceding::button[1]</value>
      <webElementGuid>b0194374-92b1-4b02-a917-474e19fb120f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OK']/parent::*</value>
      <webElementGuid>5d80f043-e22d-4906-a35c-d14ec1d44d4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button[5]</value>
      <webElementGuid>d18d558c-0877-43f4-ad89-d2e2c79b67c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'xl_dijit-bootstrap_Button_62' and (text() = 'OK' or . = 'OK')]</value>
      <webElementGuid>5e7073c7-6d45-4d09-8617-e848d08697d5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
