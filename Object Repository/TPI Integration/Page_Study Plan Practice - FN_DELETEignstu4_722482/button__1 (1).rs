<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button__1 (1)</name>
   <tag></tag>
   <elementGuidId>22f1ea92-14be-47b7-85c2-4486a5bef81b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#xl_player_PlayerMenu_2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[starts-with(@id, 'xl_player_PlayerMenu') and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1eb2a6af-a526-4117-944f-9644fd3d855e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>icon, _buttonNode, focusNode</value>
      <webElementGuid>049bf705-5e1b-4624-b950-002dc91e2abd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>xlPlayerMenuIcon dijitDownArrowButton dijitHasDropDownOpen</value>
      <webElementGuid>66ce46f7-98ef-4f46-9e95-ddf21b98625c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>759d188a-d0b8-43c5-91f7-1c3d14f7a099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>41d3db81-9e59-4dc6-ba63-ac5de28dbb99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
      <webElementGuid>8cb9412a-1078-4df2-acd6-6545a1729646</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>70c6d0fc-54cd-4360-b2ca-88c5c90f0e0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>xl_player_PlayerMenu_2</value>
      <webElementGuid>c9daba45-a741-41a5-9549-b515cfdd081e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>QA Tools</value>
      <webElementGuid>8aa295bf-0345-46cc-84ad-02a42248776d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>popupactive</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>23a18228-b8ef-4e45-b23c-b86163041ca7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>be09e510-bf6f-48ce-a7ac-386cd3e0803e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-owns</name>
      <type>Main</type>
      <value>dijit_Menu_7</value>
      <webElementGuid>2c5c4db0-3b2d-4061-864e-1f69ee84faa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> </value>
      <webElementGuid>163d9428-2d98-4dec-9ef5-20811a093c35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;xl_player_PlayerMenu_2&quot;)</value>
      <webElementGuid>42b6f4ee-b4f6-4353-abb1-fdf8b51baa1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/iframe_Return to Homework_activityFrame (2)</value>
      <webElementGuid>95f050e8-66ec-4cd7-acd3-5da7c7b72b56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='xl_player_PlayerMenu_2']</value>
      <webElementGuid>fabf763f-5768-47da-bd32-e3a6a3d589df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='xl_player_HigherEdPlayerControlPanel_1']/div/div[5]/div/button</value>
      <webElementGuid>76a780ed-2f6a-43af-90cc-2194d80dc8d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[8]/following::button[2]</value>
      <webElementGuid>1768751b-f6d3-47ba-af71-b724b04fccc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ask my instructor'])[2]/following::button[2]</value>
      <webElementGuid>1c177e8c-f62f-4329-862c-1b202ff1e96f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show completed'])[1]/preceding::button[1]</value>
      <webElementGuid>59b11106-8ced-4a82-b9c3-27e75bf52042</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clear all'])[1]/preceding::button[1]</value>
      <webElementGuid>d97f2a63-6279-4a61-96c4-31e2ef8ced5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/button</value>
      <webElementGuid>3a648453-910c-4fa4-adbf-de5ef45a7a86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'xl_player_PlayerMenu_2' and @title = 'QA Tools' and (text() = ' ' or . = ' ')]</value>
      <webElementGuid>b953b396-f69e-4a2e-9e74-e54d485bd694</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
