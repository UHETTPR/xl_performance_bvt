<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_0 correct</name>
   <tag></tag>
   <elementGuidId>8578674d-77ae-4948-807e-e226c414d4ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.overallScore</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fd1d5a6a-8fd2-405c-b6f8-3ab54d561727</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>overallScore</value>
      <webElementGuid>6c3b9aab-d9c4-45f6-8f4c-235c0f8725d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>overallScore</value>
      <webElementGuid>374d32e8-5ba9-4814-b255-df5d46ae57cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>0 correct</value>
      <webElementGuid>72657998-7e0c-4c61-832a-445527325c9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutRight&quot;]/div[2]/div[@class=&quot;overallScore&quot;]</value>
      <webElementGuid>923570a7-f3dc-43ca-9953-9b497b3f7b6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>0bcb66a6-61ec-41ec-9488-ebccb64017c2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[3]/div[2]/div</value>
      <webElementGuid>bf71b976-5b36-40cc-bb60-ad998d6e96f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Part 1 of 6'])[1]/following::div[4]</value>
      <webElementGuid>e4b13887-7606-4f9f-a6c5-842f751806f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='O.1.1'])[1]/following::div[5]</value>
      <webElementGuid>86338553-5f8d-43a1-abb1-1282ad752323</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Points:'])[1]/preceding::div[1]</value>
      <webElementGuid>5fb6e243-392a-4d1a-8dc2-d9179bed0618</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[2]</value>
      <webElementGuid>bb29a1da-a4d3-40c1-afff-057440514696</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='0 correct']/parent::*</value>
      <webElementGuid>0b3ccaee-0588-4e71-b8f4-205d18180314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div[2]/div</value>
      <webElementGuid>6d1e0568-3785-4a44-91e0-89ce67a2316e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '0 correct' or . = '0 correct')]</value>
      <webElementGuid>f182a750-b3eb-4b1d-a096-737bc3329287</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
