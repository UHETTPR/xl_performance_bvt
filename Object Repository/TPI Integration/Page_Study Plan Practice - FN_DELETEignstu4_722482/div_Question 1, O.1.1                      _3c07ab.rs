<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Question 1, O.1.1                      _3c07ab</name>
   <tag></tag>
   <elementGuidId>cd4f2f40-4690-4d9c-9388-e71640cf5dc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.nextPrevPanelText</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3b97a974-78e0-44e9-93f4-4cf1a8db4b02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nextPrevPanelText</value>
      <webElementGuid>0a7590c2-c2b9-48a9-9ec8-24cd2d72d033</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            Question 1, O.1.1
                        
                        Part 1 of 6
                    </value>
      <webElementGuid>99aae970-76a7-460f-9797-d9d8f265ca82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_layout_ContentPane_5&quot;)/div[@class=&quot;layoutContainer&quot;]/div[@class=&quot;layoutCenter&quot;]/div[@class=&quot;nextPrevPanel&quot;]/div[@class=&quot;nextPrevPanelText&quot;]</value>
      <webElementGuid>70fd07aa-18c8-4452-b0c0-b32d97e4baa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/iframe_Return to Homework_activityFrame</value>
      <webElementGuid>b423f189-123c-4892-a398-131345201bbe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='dijit_layout_ContentPane_5']/div/div[2]/div/div</value>
      <webElementGuid>2dfb19b8-7e7e-4429-aa04-68c8f1f73f77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='O.1'])[1]/following::div[5]</value>
      <webElementGuid>86889d09-2af9-47cd-9d49-1d21e12634ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[2]/div/div</value>
      <webElementGuid>ca5957bf-a8d1-456c-859f-b08d8f624e4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            Question 1, O.1.1
                        
                        Part 1 of 6
                    ' or . = '
                        
                            Question 1, O.1.1
                        
                        Part 1 of 6
                    ')]</value>
      <webElementGuid>8f7fed56-dad0-403c-b171-1a34a73de460</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
