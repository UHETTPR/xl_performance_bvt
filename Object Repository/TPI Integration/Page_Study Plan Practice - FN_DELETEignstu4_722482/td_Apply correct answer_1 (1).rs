<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Apply correct answer_1 (1)</name>
   <tag></tag>
   <elementGuidId>58e2e21a-7324-468a-b17a-9fb91e1b829f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dijit_MenuItem_44_text</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[starts-with(@id, 'dijit_MenuItem') and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>855ba092-2009-4429-8c0d-758d782030d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dijitReset dijitMenuItemLabel</value>
      <webElementGuid>9bc33068-ef43-49c2-934b-5954fbecad9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>fdbee5fe-67a3-4d9c-8239-482ddb159b99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dojo-attach-point</name>
      <type>Main</type>
      <value>containerNode,textDirNode</value>
      <webElementGuid>096cb566-138a-459e-9d00-3da8aa0c6ebe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>dijit_MenuItem_44_text</value>
      <webElementGuid>526d05eb-de05-445a-b294-cbb8afe9b02f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply correct answer</value>
      <webElementGuid>b8cda8fb-b65a-40e0-b8a3-0d3b357bf843</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dijit_MenuItem_44_text&quot;)</value>
      <webElementGuid>c9df4bdc-678e-45c2-9b7d-244b12d35fa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/iframe_Return to Homework_activityFrame (2)</value>
      <webElementGuid>82195c94-5d9e-4fbf-bbf6-804cf751b365</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='dijit_MenuItem_44_text']</value>
      <webElementGuid>ab63a1bf-3027-4f7c-a5ca-dce130420ec0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='dijit_MenuItem_44']/td[2]</value>
      <webElementGuid>b0aae7bc-eb22-4d65-bb64-9c9b8c494ecd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show alt text'])[1]/following::td[4]</value>
      <webElementGuid>de0c72f7-e6f4-4ba3-9400-986c727fe6f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[1]/following::td[5]</value>
      <webElementGuid>8e880cac-c510-4fd5-94ad-504939a79080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[11]/preceding::td[2]</value>
      <webElementGuid>9ff5ba86-ad48-4942-86b9-1e3823989034</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✓'])[2]/preceding::td[3]</value>
      <webElementGuid>7469df38-6a2b-48a4-b90e-d0f9b61bcf40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply correct answer']/parent::*</value>
      <webElementGuid>05e4d869-4b44-460d-aafa-1ff427aa0445</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/table/tbody/tr[5]/td[2]</value>
      <webElementGuid>a29d70ee-6f2f-4434-9b47-cd6c68770dea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'dijit_MenuItem_44_text' and (text() = 'Apply correct answer' or . = 'Apply correct answer')]</value>
      <webElementGuid>9ac365e6-883b-47a8-834b-b92c31e02049</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
