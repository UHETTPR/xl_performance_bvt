<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AuthSystem        SMSsmscertsms4certTPI_9bf095</name>
   <tag></tag>
   <elementGuidId>ea46a4f0-c4ba-4f23-8b4c-04a73a1f17f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#pnlManage</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='pnlManage']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d649f19b-3aea-4ece-ba0c-e2cd0cad0723</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pnlManage</value>
      <webElementGuid>42b7c461-75b6-4ac8-8c92-ddeabc56e56e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		TPI-Test
		eCollege-sms
		sakai-sms
		synapse-sms
		venture1144-sms
		moodleblti-sms
		wgulrps-sms
		blackboardht-sms
		d2l-sms
		canvas-sms
		apollo-sms
		angel-sms
		brainhoney-sms
		sakaiblti-sms
		learnsomething-sms
		acelearning-sms
		coursesmart-sms
		coursera-sms
		loudcloud-sms
		eCollege-ppe
		canvas-ppe
		easybridge-ppe
		blackboardht-ppe
		moodleblti-ppe
		d2l-smsppe

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student's SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS
		Mastering Environmental Science with LMS
		Mastering Physics with LMS
		Mastering Anatomy and Physiology with LMS
		Mastering Astronomy with LMS
		Mastering Biology with LMS
		Mastering Genetics with LMS
		Mastering Geography &amp; Meterology with LMS
		Mastering Geology &amp; Oceanography with LMS
		Mastering Health &amp; Nutrition with LMS
		Mastering Microbiology with LMS
		Mastering Engineering &amp; Computer Science with LMS
		&quot;Mastering Chemistry with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
</value>
      <webElementGuid>3e57b49a-aa92-4c9a-97b3-cb9ed9ebc980</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;pnlManage&quot;)</value>
      <webElementGuid>defe7af6-195f-4929-ac53-d0ee0648e6a4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='pnlManage']</value>
      <webElementGuid>dfe09db0-72a5-471d-b94a-7bbfa6f1ebf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]</value>
      <webElementGuid>08431130-90f4-425f-ba63-2d7e195c9418</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='(use tpicertprofalec for the professor account)']/parent::*</value>
      <webElementGuid>bf1c0700-da51-4eb6-984a-431bbb398cb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>408f3382-e57b-4dc3-9bda-844d8cecf993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'pnlManage' and (text() = concat(&quot;
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		TPI-Test
		eCollege-sms
		sakai-sms
		synapse-sms
		venture1144-sms
		moodleblti-sms
		wgulrps-sms
		blackboardht-sms
		d2l-sms
		canvas-sms
		apollo-sms
		angel-sms
		brainhoney-sms
		sakaiblti-sms
		learnsomething-sms
		acelearning-sms
		coursesmart-sms
		coursera-sms
		loudcloud-sms
		eCollege-ppe
		canvas-ppe
		easybridge-ppe
		blackboardht-ppe
		moodleblti-ppe
		d2l-smsppe

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS
		Mastering Environmental Science with LMS
		Mastering Physics with LMS
		Mastering Anatomy and Physiology with LMS
		Mastering Astronomy with LMS
		Mastering Biology with LMS
		Mastering Genetics with LMS
		Mastering Geography &amp; Meterology with LMS
		Mastering Geology &amp; Oceanography with LMS
		Mastering Health &amp; Nutrition with LMS
		Mastering Microbiology with LMS
		Mastering Engineering &amp; Computer Science with LMS
		&quot;Mastering Chemistry with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
&quot;) or . = concat(&quot;
	
        AuthSystem
        
		SMS
		smscert
		sms4cert
		TPI-Test
		eCollege-sms
		sakai-sms
		synapse-sms
		venture1144-sms
		moodleblti-sms
		wgulrps-sms
		blackboardht-sms
		d2l-sms
		canvas-sms
		apollo-sms
		angel-sms
		brainhoney-sms
		sakaiblti-sms
		learnsomething-sms
		acelearning-sms
		coursesmart-sms
		coursera-sms
		loudcloud-sms
		eCollege-ppe
		canvas-ppe
		easybridge-ppe
		blackboardht-ppe
		moodleblti-ppe
		d2l-smsppe

	
        Institution
        
		-None Selected-

	
        
        
        BBCourseID
        
        BBUserID
        
            (use tpicertprofalec for the professor account)
        

        Note: Due to SMS not being used if you test eText launch you will receive &quot;Either you have entered an incorrect username/password, or you do not have a subscription to this site.&quot;.  This is expected.
        Note: For Easybridge the external user id is sent as &quot;ext_user_id&quot; instead of &quot;custom_ext_user_id&quot; since TPI is sending the student&quot; , &quot;'&quot; , &quot;s SIS id as &quot;ext_user_id&quot;
        

        
		
            UDSON Examples:
            urn:udson:pearson.com/sms/Test:user/8537535
            urn:udson:pearson.com/sms/Test:course/viraji08203
        
	

        
        
        
		
            
                View:
                Link
                
                ToolLaunch (EText)
            
        
	
        
        
                
                    CC LinkInfo
                    Discipline
                    
		Math
		Econ
		Finance
		Accounting
		A&amp;B? - Not Ready
		MyMathTest
		MyReadinessTest
		Math Spanish--PELA
		Math Portuguese--PELA
		MG Math
		MyFoundationsLab
		MyMathLab Global
		My PoliSciLab
		MyReadingLab
		MyWritingLab
		MyFoundationsLab for Math
		MySkillsLab
		MyCJLab with LMS
		MyCulinaryLab with LMS
		MyEngineeringLab
		MyMathLab Deutsche Version
		MyBCommLab with LMS
		MyBizLab with LMS
		MyManagementLab with LMS
		MyMarketingLab with LMS
		MyMISLab with LMS
		MyWritingLab Global
		MyMathLab Italiano
		MyMathLab Prufungstraining
		MyMathLab version francaise
		MonLab | mathematique
		MyBRADYLab with LMS
		MyHealthProfessionsLab with LMS
		MyMedicalTerminologyLab with LMS
		Universal MyLabs with LMS
		MyStudentSuccessLab with LMS
		MyLiteratureLab with LMS
		TSI//MyFoundationsLab with LMS
		MyACCU//MFL with LMS
		Mastering Environmental Science with LMS
		Mastering Physics with LMS
		Mastering Anatomy and Physiology with LMS
		Mastering Astronomy with LMS
		Mastering Biology with LMS
		Mastering Genetics with LMS
		Mastering Geography &amp; Meterology with LMS
		Mastering Geology &amp; Oceanography with LMS
		Mastering Health &amp; Nutrition with LMS
		Mastering Microbiology with LMS
		Mastering Engineering &amp; Computer Science with LMS
		&quot;Mastering Chemistry with LMS

	
                    Target        
                    
		invalidtarget
		classgrades
		assignmentmanager
		announcementmanager
		coursesettings
		mmlannouncements
		coursehome
		doassignments
		gradebook
		studyplan
		multimedia
		calendar
		exercises
		grapher
		browsercheck
		ebook
		studyplanmanager
		fullannouncements
		assignment
		managehomework
		managetests
		manageresults
		taketest
		assignedtests
		assignedhomework
		classrosterupdate
		instructorhelp
		learningpath
		lpchapter
		learningpathmanager
		homepagesettings
		tutor
		alerts
		uopdiagnostic
		dashboard
		bridgepage
		specificassignment
		learning_catalytics
		companionstudyplan
		thirdpartygradedinstructordo

	

                    Bridge Page
                    
		
		Reading
		Writing
		Math

	

                    Page Link Mode        
                    
		
		frame
		popup

	

                    Content
                     (foundations only - content area string name)
                    ChapterID
                    
                    SectionID
                    
                    ExerciseID
                    
                    ChapterNumber
                     (multimedia library only)
                    SectionNumber
                     (multimedia library only)
                    Media
                     (multimedia library only, comma separated list of content type names)

                    UnitID
                    

                    ExternalUserID
                     (SMS kiosk, Easybridge and grade repository integrations only) 
                    ExternalPartnerID
                     (SMS kiosk and grade repository integrations only)
                    ExternalInstitutionID
                     (SMS kiosk and grade repository integrations only)
                    ExternalCourseID
                     (SMS kiosk and grade repository integrations only)

                    Mode        
                    
		
		chapter
		unit
		experiments

	
                    Category
                    
		None
		testquiz
		test
		homework

	
                    Category (classgrades)
                    
		None
		homework
		quizzes
		tests
		other

	

                    Dataview (classgrades)
                    
		None
		allassignments
		bystudent
		studyplan
		bychapter

	
                    ProductID
                    
		-- Select --
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS
		3 - MyMathLab
		4 - MyEconLab
		12 - MyFinanceLab
		21 - MyAccountingLab
		34 - MathXL School with LMS
		37 - MyOMLab
		39 - MyMathTest with LMS
		41 - MyReadinessTest with LMS
		42 - MyMathLab Español with LMS
		45 - MyMathLab Brasil with LMS
		46 - MG Math
		48 - MyFoundationsLab with LMS
		50 - ResultsPlus Booster with LMS
		51 - MyMathLab Global with LMS
		52 - MyMathLab Italiano with LMS
		54 - MyReadingLab with LMS
		56 - MyWritingLab with LMS
		58 - MyFoundationsLab for Math with LMS
		60 - MySkillsLab with LMS
		62 - MyFdLab ACCUPLACER with LMS
		64 - MyNursingLab with LMS
		66 - MyMathLab Deutsche Version with LMS
		67 - Engineering with LMS
		72 - MyMathLab Prüfungstraining with LMS
		73 - MyMathLab version française with LMS
		74 - MonLab | mathématique with LMS
		76 - MyWritingLab Global with LMS
		78 - BComm with LMS
		80 - Biz with LMS
		82 - Management with LMS
		84 - Marketing with LMS
		86 - MIS with LMS
		88 - BRADY with LMS
		90 - HealthProfessions with LMS
		92 - MedicalTerminology with LMS
		94 - Culinary with LMS
		96 - Criminal Justice with LMS
		98 - MyStudentSuccessLab with LMS
		100 - MyLiteratureLab with LMS
		102 - TSI//MyFoundationsLab with LMS
		104 - MyACCU//MFL with LMS
		106 - Universal MyLabs with LMS
		107 - Mastering Environmental Science with LMS
		108 - Mastering Physics with LMS
		109 - Mastering Anatomy and Physiology with LMS
		110 - Mastering Astronomy with LMS
		111 - Mastering Biology with LMS
		112 - Mastering Genetics with LMS
		113 - Mastering Geography &amp; Meterology with LMS
		114 - Mastering Geology &amp; Oceanography with LMS
		115 - Mastering Health &amp; Nutrition with LMS
		117 - Mastering Microbiology with LMS
		121 - Mastering Engineering &amp; Computer Science with LMS
		122 - Mastering Chemistry with LMS
		140 - IT with LMS

	
                    Show Banner
                    
		no
		yes

	(yes to show header banner on student pages regardless of pagelink value; no for default behavior)
                    View
                    
		
		needsgrading
		Test
		Homework

	(view:needsgrading option for alerts - blank for default no view, Test/Homework view for assignmentmanager)
                    Page
                     (ebook use only)
                    Page Start 
                     (ebook use only)
                    Page End
                     (ebook use only)
                    Page View
                    
		
		All
		lp

	 (ebook use only)
                    
                   Order
                    (Only for uopdiagnostic target - relative assignment order id)
                    History Id
                     (only for specific assignment and companionstudyplan target)
                   
                
                   Custom Role Ext
                     
		
		Admin

	 (Only for dashboard target)
                
                    Show Left Navigation 
		no
		yes

	
                
                   
                   
                   
            
            
            
        
&quot;))]</value>
      <webElementGuid>354a8616-7700-457e-89de-b1255ae7621a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
