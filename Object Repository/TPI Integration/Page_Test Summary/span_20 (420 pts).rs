<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_20 (420 pts)</name>
   <tag></tag>
   <elementGuidId>dca6e812-f30e-4e75-a434-ed90bdd46a90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctl00_InsideForm_MasterContent_LblScore</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = '20% (4/20 pts)' or . = '20% (4/20 pts)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a7b42c7f-0fd4-4b0c-bc19-98cd7a9e9e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctl00_InsideForm_MasterContent_LblScore</value>
      <webElementGuid>ca4e7506-37d2-464e-ae5d-dc8acafc48b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>20% (4/20 pts)</value>
      <webElementGuid>eb632143-ba08-40f2-9dba-421349d34e1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctl00_InsideForm_MasterContent_LblScore&quot;)</value>
      <webElementGuid>6b5f44e4-7e88-41db-9c38-8b4d2b7a2569</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ctl00_ctl00_InsideForm_MasterContent_LblScore']</value>
      <webElementGuid>3646f5d3-054d-4970-bb34-28174ed11bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='ctl00_ctl00_InsideForm_MasterContent_trScore']/td[2]/b/span</value>
      <webElementGuid>6310a014-6f1f-466f-9ef3-c1cd991ac4b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Score:'])[1]/following::span[1]</value>
      <webElementGuid>c382e393-0e03-40c1-a8e0-2e9e897ac7d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Time Spent:'])[1]/following::span[2]</value>
      <webElementGuid>2d12bd44-a021-41a0-b440-124b8ed8ac94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='will'])[1]/preceding::span[1]</value>
      <webElementGuid>88b8ce61-ce06-47f5-8269-236b5f2a0926</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='20% (4/20 pts)']/parent::*</value>
      <webElementGuid>f7e80191-3dd9-4be3-96ed-0e62e3b6fde7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//b/span</value>
      <webElementGuid>b9edc677-c4a6-4fde-9788-ff89783b9b01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ctl00_ctl00_InsideForm_MasterContent_LblScore' and (text() = '20% (4/20 pts)' or . = '20% (4/20 pts)')]</value>
      <webElementGuid>ae29e436-7eb0-45fe-b36a-9b6b9c3d60c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
