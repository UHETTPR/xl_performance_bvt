import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Course Manager - ppe_ins2 Prabhashi/a_Assignment Manager'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Assignment'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Homework'))

WebUI.setText(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_Homework Name_ctl00ctl00InsideFormMas_dbdf37'), 
    'SampleHomework')

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.selectOptionByValue(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/select_1. The Real Number System2. Linear E_06cce4'), 
    '2', true)

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 2.1.13_chk990080.2.1.15.5.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 2.1.15_chk990080.2.1.17.6.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 2.1.17_chk990080.2.1.19.7.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 2.1.19_chk990080.2.1.21.8.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Add'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Save  Assign'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Assignment'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Quiz'))

WebUI.setText(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_Homework Name_ctl00ctl00InsideFormMas_dbdf37'), 
    'SampleQuiz')

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.1.47_chk990080.1.1.51.19.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.1.45_chk990080.1.1.47.17.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.1.51_chk990080.1.1.53.20.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Add'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Save  Assign'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Assignment'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Assignment Manager - ppe_ins2 Prabhashi/a_Create Test'))

WebUI.setText(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_Homework Name_ctl00ctl00InsideFormMas_dbdf37'), 
    'SampleTest')

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.25_chk990080.1.3.29.67.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.29_chk990080.1.3.31.68.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.31_chk990080.1.3.33.69.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.35_chk990080.1.3.37.71.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Add'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.17_chk990080.1.3.19.64.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.19_chk990080.1.3.23.65.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/input_(P) 1.3.23_chk990080.1.3.25.66.0'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Pool'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Next'))

WebUI.click(findTestObject('InstructorObjects/AssignmentCreation/Page_Create Homework and Tests - ppe_ins2 P_72ec81/a_Save  Assign'))

