import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//
//WebUI.navigateToUrl('https://mylabsxl-ppe.pearson.com/login_mxlppe.htm?bypass=yes')
//
//WebUI.setText(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/input_Username_username'), 
//    'xlperfautomationins01')
//
//WebUI.setEncryptedText(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/input_Password_password'), 
//    'p4y+y39Ir5Oy1MY8jPt0uQ==')
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/button_Sign In'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Launch - xlperf automationins01/a_Enter MathXL'))
WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Instructor Home - xlperf automationins01/a_Course Manager'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Course Manager - xlperf automationins01/a_PerfBVT_XL_AutomationCourse'))
} else {
    WebUI.click(findTestObject('InstructorObjects/clear results/Page_Course Manager - xlperf automationins01/a_PerfBVT_XL_AutomationCourse Janus'))
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Instructor Home - xlperf automationins01/a_Gradebook'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/span_PerfBVT_XL_AutomationCourse 1         _327a67'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/h2_Gradebook'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/a_More Tools'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/a_Delete Results'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/input_Other_SHAll'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/a_Delete Selected Results'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/div_Delete Selected Assignment Results     _60df66'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/h4_Delete Selected Assignment Results'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/button_Permanently Delete Results'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/p_Your updates have been saved. Some studen_f25684'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/a_OK'))

