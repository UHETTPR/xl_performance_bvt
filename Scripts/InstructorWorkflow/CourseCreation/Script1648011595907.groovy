import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Gradebook - Prabhashi ppe_ins1/a_Course Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Manager - Prabhashi ppe_ins1/h2_Course Manager'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_Course Manager - Prabhashi ppe_ins1/a_Create or copy a course'))

WebUI.selectOptionByValue(findTestObject('InstructorObjects/CourseCreation/Page_New Course/select_-- Choose --StandardCoordinatorMember'), 
    'Standard', true)

WebUI.setText(findTestObject('InstructorObjects/CourseCreation/Page_New Course/input_Course name_ctl00ctl00InsideFormMaste_92e586'), 
    'SampleTestCourse')

WebUI.selectOptionByValue(findTestObject('InstructorObjects/CourseCreation/Page_New Course/select_-- Choose Book --   ADP Algebra II O_fd48ff'), 
    '990080', true)

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_New Course/a_Next'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_New Course/a_Next'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_New Course/a_Next'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_New Course/a_Next'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_New Course/a_Save'))

WebUI.sleep(5000)

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_Course Settings Summary - Prabhashi ppe_ins1/h2_Course Settings Summary'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_Course Settings Summary - Prabhashi ppe_ins1/a_XL1U-V1R3-30L7-8Y85'))

WebUI.click(findTestObject('InstructorObjects/CourseCreation/Page_Course Settings Summary - Prabhashi ppe_ins1/input_Browser Check_ctl00ctl00InsideFormMas_e7786d'))

