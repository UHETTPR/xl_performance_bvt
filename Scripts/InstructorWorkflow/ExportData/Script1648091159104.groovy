import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Instructor Home - xlperf automationins01/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Gradebook - xlperf automationins01/a_Export Data'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/h2_Export Data'), 
    0)

WebUI.selectOptionByValue(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/select_--Choose--HomeworkQuizQuiz MeTestSam_942f0f'), 
    '2', true)

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/a_Add'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/span_Select Assignment Tags'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/input_Select Assignment Tags_23398'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/input_AssignmentTag1_23399'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/input_AssignmentTag2_23400'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/button_OK'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/div_AssignmentTag1'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/div_AssignmentTag2'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/div_AssignmentTag3'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/a_Export Data'))

WebUI.switchToWindowTitle('Export')

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Export/a_Detailed_Homework_Results.csv'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.switchToWindowTitle('Quick Export - xlperf automationins01')
} else {
    WebUI.switchToWindowTitle('Quick Export - xlperf automationins02')
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Quick Export - xlperf automationins01/a_Back to Gradebook'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Gradebook - xlperf automationins01/a_Student Overview'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Overview by Student - xlperf automationins01/td_automationstu01, xlperf'), 
        0)
} else {
    WebUI.verifyElementPresent(findTestObject('InstructorObjects/exports/Page_Overview by Student - xlperf automationins01/td_automationstu02, xlperf'), 
        0)
}

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Overview by Student - xlperf automationins01/div_Class RosterOverall ScoreHomeworkQuizze_229e9a'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Overview by Student - xlperf automationins01/a_Study Plan'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/exports/Page_Study Plan - xlperf automationins01/td_automationstu01, xlperf'), 
        0)
} else {
    WebUI.verifyElementPresent(findTestObject('InstructorObjects/exports/Page_Study Plan - xlperf automationins01/td_automationstu02, xlperf'), 
        0)
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Study Plan - xlperf automationins01/input_View_ctl00ctl00InsideFormMasterConten_e0fa7a'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/exports/Page_Study Plan - xlperf automationins01/a_Performance by Chapter'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter/a_Ch. 1. Review of Real Numbers'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter/a_Section 1.2 Symbols and Sets of Numbers'))

WebUI.switchToWindowTitle('Performance By Chapter - Specific Section')

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/h2_Gradebook'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/th_Section 1.2 Symbols and Sets of Numbers'), 
    0)

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu01, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationins01, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu04, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu05, xlperf'), 
        0)
} else {
    WebUI.verifyElementPresent(findTestObject('InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu02, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationins02, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu07, xlperf'), 
        0)

    WebUI.verifyElementPresent(findTestObject('InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/td_automationstu10, xlperf'), 
        0)
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter - Specific Section/a_OK'))

WebUI.switchToWindowTitle('Performance By Chapter')

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter/td_29.4'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter/td_25'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/ExportData/Page_Performance By Chapter/td_0'), 
    0)

