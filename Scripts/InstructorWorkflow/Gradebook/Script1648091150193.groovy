import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Instructor Home - xlperf automationins01/a_Gradebook'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_Assignments'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_Homework'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/h2_Gradebook'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/i_--_xlicon-ia'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/h2_Item Analysis'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/p_Homework                     Section 1.1 _970011'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/a_Back to Gradebook'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_Assignments'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_Tests'))

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/i_--_xlicon-ia_1'))

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/h2_Item Analysis'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/p_Test                     Chapter 2-A'), 
    0)

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Item Analysis - xlperf automationins01/a_Back to Gradebook'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_automationstu01, xlperf'))
} else {
    WebUI.click(findTestObject('InstructorObjects/gb/Page_Gradebook - xlperf automationins01/a_automationstu02, xlperf'))
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Results - xlperf automationins01/button_-- Select --'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Results - xlperf automationins01/a_Change Score'))

WebUI.switchToWindowTitle('Submit Score')

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Submit Score/h2_Submit Score'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Submit Score/td_Test                     Chapter 2-A'), 
    0)

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Submit Score/td_Student automationstu01, xlperf'), 
        0)
} else {
    WebUI.verifyElementPresent(findTestObject('InstructorObjects/gb/Page_Submit Score/td_Student automationstu02, xlperf'), 
        0)
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Submit Score/a_Submit Score'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.switchToWindowTitle('Results - xlperf automationins01')
} else {
    WebUI.switchToWindowTitle('Results - xlperf automationins02')
}

WebUI.sleep(8000)

WebUI.click(findTestObject('InstructorObjects/gb/Page_Results - xlperf automationins01/button_-- Select -- (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Results - xlperf automationins01/a_Settings Per Student'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.switchToWindowTitle('Individual Student Settings - xlperf automationins01')
} else {
    WebUI.switchToWindowTitle('Individual Student Settings - xlperf automationins02')
}

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Individual Student Settings - xlperf a_9fb648/h2_Individual Student Settings'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Individual Student Settings - xlperf a_9fb648/div_Homework Section 1.1 Homework'), 
    0)

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/gb/Page_Individual Student Settings - xlperf a_9fb648/div_Student automationstu01, xlperf'), 
        0)
} else {
    WebUI.verifyElementPresent(findTestObject('InstructorObjects/gb/Page_Individual Student Settings - xlperf a_9fb648/div_Student automationstu02, xlperf'), 
        0)
}

WebUI.click(findTestObject('Object Repository/InstructorObjects/gb/Page_Individual Student Settings - xlperf a_9fb648/a_Save Settings'))

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.switchToWindowTitle('Results - xlperf automationins01')
} else {
    WebUI.switchToWindowTitle('Results - xlperf automationins02')
}

