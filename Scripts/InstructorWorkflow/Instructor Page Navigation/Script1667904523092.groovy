import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Instructor Home - Prabhashi ppe_ins1/a_Course Home'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Home - Prabhashi ppe_ins1/a_Entire Course To Date'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Home - Prabhashi ppe_ins1/a_Current'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Calendar - Prabhashi ppe_ins1/a_Homework and Tests'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Homework and Tests - Prabhashi ppe_ins1/h2_Homework and Tests'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Homework and Tests - Prabhashi ppe_ins1/a_Results'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Results - Prabhashi ppe_ins1/h2_Results'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Results - Prabhashi ppe_ins1/a_Study Plan'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Study Plan - Prabhashi ppe_ins1/h2_Study Plan'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Study Plan - Prabhashi ppe_ins1/a_Instructor Home'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Instructor Home - Prabhashi ppe_ins1/a_View Student Home'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Home - Prabhashi ppe_ins1/a_View Instructor Home'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Instructor Home - Prabhashi ppe_ins1/a_Course Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Manager - Prabhashi ppe_ins1/a_Home Page Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Home Page Manager - Prabhashi ppe_ins1/h2_Home Page Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Home Page Manager - Prabhashi ppe_ins1/a_Assignment Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Assignment Manager - Prabhashi ppe_ins1/h2_Assignment Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Assignment Manager - Prabhashi ppe_ins1/a_Study Plan Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Study Plan Manager - Prabhashi ppe_ins1/h2_Study Plan Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Study Plan Manager - Prabhashi ppe_ins1/a_Gradebook'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Gradebook - Prabhashi ppe_ins1/h2_Gradebook'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Gradebook - Prabhashi ppe_ins1/a_Course Manager'))

WebUI.click(findTestObject('InstructorObjects/PageNav/Page_Course Manager - Prabhashi ppe_ins1/h2_Course Manager'))

