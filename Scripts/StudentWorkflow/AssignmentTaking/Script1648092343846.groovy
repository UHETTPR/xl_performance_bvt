import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Course Home - xlperf automationstu01/a_Homework and Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/h2_Homework and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/button_All Assignments'))

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/a_Homework'))

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/a_Section 1.1 Homework'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/h2_Do Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/span_Section 1.1 Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/span_0 (0 points out of 17)'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/a_Start'))

WebUI.sleep(12000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_PerfBVT_XL_AutomationCourse'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/span_Homework Section 1.1 Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_HW Score 0, 0 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Apply correct answer'))

WebUI.sleep(3000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_Fantastic                              _354ab9'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Next question'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_HW Score 5.88, 1 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button__1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Apply correct answer_1'))

WebUI.sleep(3000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_Excellent                              _7fe3c1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Next question_1'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_HW Score 11.76, 2 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button__1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Apply correct answer_1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Next question_1_2'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button__1_2_3'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Apply correct answer_1_2_3'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Next question_1_2_3'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button__1_2_3_4'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Skip step'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Question 5,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_HW Score 23.53, 4 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button__1_2_3_4_5'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/td_Apply correct answer_1_2_3_4'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Next question_1_2_3_4'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/div_HW Score 29.41, 5 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/button_Save'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/span_29.41 (5 points out of 17)'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/li_Correct 5'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/li_Incorrect 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Do Homework - Section 1.1 Homework/a_Homework and Tests'))

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/button_All Assignments'))

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/a_Quizzes  Tests'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/h2_Homework and Tests Quizzes  Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Homework and Tests - xlperf automationstu01/a_Chapter 2-A'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Are you ready to start/h1_Are you ready to start'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Are you ready to start/div_Chapter 2-A'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Are you ready to start/a_Start Test'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/div_PerfBVT_XL_AutomationCourse'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/span_Test Chapter 2-A'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/div_This test 20 point(s) possible'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Apply correct answer'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Apply correct answer_1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Skip step'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1_2'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1_2_3'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Skip step_1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1_2_3'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1_2_3_4'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Apply correct answer_1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1_2_3_4'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1_2_3_4_5'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Apply correct answer_1_2_3'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1_2_3_4_5'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button__1_2_3_4_5_6'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/td_Apply correct answer_1_2_3_4'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Next_1_2_3_4_5_6'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/button_Submit test'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Chapter 2-A/div_pop-up content starts                  _e79979'), 
    0)

WebUI.click(findTestObject('StudentObjects/takingassignments/Page_Chapter 2-A/button_Submit test (1)'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Test Summary/h2_Test Summary'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Test Summary/span_Chapter 2-A'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Test Summary/span_25 (520 pts)'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Test Summary/a_Go to Results'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Results - xlperf automationstu01/h2_Results'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Results - xlperf automationstu01/td_25'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/takingassignments/Page_Results - xlperf automationstu01/td_29.41'), 
    0)

