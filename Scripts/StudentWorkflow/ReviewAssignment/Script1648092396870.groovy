import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Course Home - xlperf automationstu01/a_Results'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Results - xlperf automationstu01/button_Past 2 Weeks'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Results - xlperf automationstu01/a_Entire course to date'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Results - xlperf automationstu01/a_Review Chapter 2-A'))

WebUI.sleep(12000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/span_Review  Test Chapter 2-A'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/div_Test Score 25, 5 of 20 points'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/span_Points 1 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/div_Question 2, 1.2.3'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/span_Points 1 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/div_Question 3, 1.2.5'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/span_Points 0 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/div_Points 1 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Chapter 2-A/button_Close'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Results - xlperf automationstu01/a_Review Section 1.1 Homework'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework - Section 1.1 Homework/h2_Review Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework - Section 1.1 Homework/div_Warning            You are reviewing yo_14d351'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework - Section 1.1 Homework/a_Question1'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework - Section 1.1 Homework/button_Continue to Review Homework'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_Question 1, 1.2.1'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_Points 1 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/li_Question 5'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_Points 0 of 1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/button_Question 1,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_Question 6, 1.2.11'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_Points 1 of 1'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/div_HW Score 29.41, 5 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Review Homework/button_Close'))

WebUI.click(findTestObject('Object Repository/StudentObjects/assignment reviewing/Page_Results - xlperf automationstu01/h2_Results'))

