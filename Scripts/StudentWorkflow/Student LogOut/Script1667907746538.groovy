import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

if (GlobalVariable.executionProfile == 'BAU') {
    WebUI.click(findTestObject('Object Repository/StudentObjects/LogOut/Page_Course Home - xlperf automationstu01/a_xlperf automationstu01'))
} else {
    WebUI.click(findTestObject('StudentObjects/LogOut/Page_Course Home - xlperf automationstu01/a_xlperf automationstu02'))
}

WebUI.click(findTestObject('Object Repository/StudentObjects/LogOut/Page_Course Home - xlperf automationstu01/a_Log Out'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/LogOut/Page_Pearson Sign In/img_Hello. Sign in and let the learning beg_244ca2'), 
    0)

