import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Course Home - xlperf automationstu01/a_Study Plan'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/a_All Chapters'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/i_Practice This_xlicon-disclosure-open'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/i_Practice This_xlicon-disclosure-close'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/i_Concept Extensions_xlicon-disclosure-open'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/a_2.2 The Addition and Multiplication Prope_16003c'))

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/a_2.2.1'))

WebUI.sleep(12000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/span_2.2The Addition and Multiplication Pro_4a0287'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/div_Question 10, 2.2.1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/td_Apply correct answer'))

WebUI.sleep(3000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/div_Fantastic                              _354ab9'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_Next question'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/div_Question 11, 2.2.3'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button__1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/td_Apply correct answer_1'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_Next question_1'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button__1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/td_Skip step'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_Question 12,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(6000)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/div_Question 13, 2.2.7'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button__1_2_3'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/td_Apply correct answer_1_2'))

WebUI.sleep(3000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_Next question_1_2'))

WebUI.sleep(6000)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan Practice - xlperf automationstu01/button_Close'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/h2_Study Plan All Chapters'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/a_Define linear equations and use the addit_f98dff'))

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/img_This question is from a mastered object_583249'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/img_This question is from a mastered object_583249_1'), 
    0)

WebUI.click(findTestObject('Object Repository/StudentObjects/study plan/Page_Study Plan - xlperf automationstu01/a_OK'))

