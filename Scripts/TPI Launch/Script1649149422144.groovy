import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
//
//WebUI.navigateToUrl('https://mylabsxl-ppe.pearson.com/login_mxlppe.htm?bypass=yes')
//
//WebUI.setText(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/input_Username_username'), 
//    'lrxlngmlmcoordedu291')
//
//WebUI.setEncryptedText(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/input_Password_password'), 
//    'ZX80H+kCOh9j2JjSYAbxWMDTFql2Dei0')
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Sign in  MathXL  Pearson/button_Sign In'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Launch - xlperf automationins01/a_Enter MathXL'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Instructor Home - xlperf automationins01/a_Course Manager'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Course Manager - xlperf automationins01/a_PerfBVT_XL_AutomationCourse'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Instructor Home - xlperf automationins01/a_Gradebook'))
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/h2_Gradebook'), 
//    0)
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/a_More Tools'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Gradebook - xlperf automationins01/a_Delete Results'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/input_Other_SHAll'))
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/a_Delete Selected Results'))
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/div_Delete Selected Assignment Results     _60df66'), 
//    0)
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/h4_Delete Selected Assignment Results'), 
//    0)
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/button_Permanently Delete Results'))
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/p_Your updates have been saved. Some studen_f25684'), 
//    0)
//
//WebUI.click(findTestObject('Object Repository/InstructorObjects/clear results/Page_Delete Results - xlperf automationins01/a_OK'))
//
//WebUI.closeBrowser()
WebUI.openBrowser('')

WebUI.navigateToUrl('https://mylabsxl-ppe.pearson.com/test/testtpilink.aspx')

WebUI.setEncryptedText(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_Secret Code_TextBoxSecret'), 
    'HXoGuAl+HfQ=')

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_Secret Code_ButtonLogin'))

WebUI.selectOptionByValue(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/select_SMSsmscertsms4certTPI-TesteCollege-s_c3a44b'), 
    '5', true, FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_BBCourseID_TextCourseID'), 
    0)

WebUI.setText(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_BBCourseID_TextCourseID'), 
    'mclrxlngMemMLM18edu2913')

WebUI.setText(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_BBUserID_TextUserID'), '802643701')

WebUI.selectOptionByValue(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/select_invalidtargetclassgradesassignmentma_db1b43'), 
    'doassignments', true)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_ProductID_ButtonRedirect'))

WebUI.switchToWindowTitle('Homework and Tests - FN_DELETEignstu43701 LN_DELETEignstu43701')

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/h2_Homework and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/button_All Assignments'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/a_Quizzes  Tests'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/a_Chapter 4-A'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Are you ready to start/a_Start Test'))

WebUI.sleep(8000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/div_mclrxlngMemMLM18edu2913'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/span_Test Chapter 4-A'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/span_Question 1 of 20'), 0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Apply correct answer'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Next'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button__1'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Apply correct answer_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Next_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button__1_2'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Apply correct answer_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Next_1_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button__1_2_3'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Skip step'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Next_1_2_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button__1_2_3_4'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Skip step_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Next_1_2_3_4'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button__1_2_3_4_5'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/td_Apply correct answer_1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Chapter 4-A/button_Submit test'))

WebUI.sleep(2000)

WebUI.click(findTestObject('TPI Integration/Page_Chapter 4-A/button_Submit test (1)'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Test Summary/h2_Test Summary'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Test Summary/span_20 (420 pts)'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Test Summary/span_Chapter 4-A'), 0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Test Summary/a_Go to Results'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Results - FN_DELETEignstu43701 LN_DELE_ed7db4/th_Test Chapter 4-A'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Results - FN_DELETEignstu43701 LN_DELE_ed7db4/td_20'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Results - FN_DELETEignstu43701 LN_DELE_ed7db4/td_420'), 
    0)

WebUI.closeWindowTitle('Results - FN_DELETEignstu43701 LN_DELETEignstu43701')

WebUI.switchToWindowTitle('TPI Link Test Page')

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_ProductID_ButtonRedirect'))

WebUI.switchToWindowTitle('Homework and Tests - FN_DELETEignstu43701 LN_DELETEignstu43701')

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/h2_Homework and Tests'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Homework and Tests - FN_DELETEignstu43_d6ac0b/a_Section 2.5 Homework'))

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/h2_Do Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/span_Section 2.5 Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/span_0 (0 points out of 17)'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/a_Start'))

WebUI.sleep(8000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/div_mclrxlngMemMLM18edu2913'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/span_Homework Section 2.5 Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/div_Question 1, 1.2.1'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/div_HW Score 0, 0 of 17 points'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer_1'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1_2'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Skip step'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Question 3,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1_2_3'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer_1_2'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question_1_2'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1_2_3_4'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer_1_2_3'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question_1_2_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1_2_3_4_5'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer_1_2_3_4'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question_1_2_3_4'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button__1_2_3_4_5_6'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/td_Apply correct answer_1_2_3_4_5'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Next question_1_2_3_4_5'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/button_Save'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/h2_Do Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/span_Section 2.5 Homework'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Do Homework - Section 2.5 Homework/span_35.29 (6 points out of 17)'), 
    0)

WebUI.closeWindowTitle('Do Homework - Section 2.5 Homework')

WebUI.switchToWindowTitle('TPI Link Test Page')

WebUI.selectOptionByValue(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/select_invalidtargetclassgradesassignmentma_db1b43'), 
    'studyplan', true)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_TPI Link Test Page/input_ProductID_ButtonRedirect'))

WebUI.switchToWindowTitle('Study Plan - FN_DELETEignstu43701 LN_DELETEignstu43701')

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Study Plan - FN_DELETEignstu43701 LN_D_462384/h3_Recommended objectives'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan - FN_DELETEignstu43701 LN_D_462384/button_Practice_1'))

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/div_mclrxlngMemMLM18edu2913'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/span_1.2Symbols and Sets of Numbers'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_ (1)'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/td_Apply correct answer (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_Next question'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button__1 (1)'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/td_Skip step (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_Question 2,_xl_dijit-bootstrap_Button_3'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button__1_2'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/td_Apply correct answer_1 (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_Next question_1'))

WebUI.sleep(4000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button__1_2_3'))

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/td_Apply correct answer_1_2 (1)'))

WebUI.sleep(2000)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_OK (1)'))

WebUI.sleep(4000)

WebUI.verifyElementPresent(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/div_3 correct'), 
    0)

WebUI.click(findTestObject('Object Repository/TPI Integration/Page_Study Plan Practice - FN_DELETEignstu4_722482/button_Close (2)'))

WebUI.sleep(4000)

WebUI.closeWindowTitle('Study Plan - FN_DELETEignstu43701 LN_DELETEignstu43701')

WebUI.closeBrowser()

